<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VisiMisiProdi extends Model
{
    protected $table = 'visi_misi_prodi';
    protected $fillable = ['visi', 'misi','id_prodi'];

    public function prodi()
    {
        return $this->belongsTo('App\ProgramStudi', 'id_prodi');
    }
}
