<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class KompetensiProdi extends Model
{
    protected $table = 'kompetensi';
    protected $fillable = ['kompetensi', 'id_prodi'];

    public function prodi()
    {
        return $this->belongsTo('App\ProgramStudi', 'id_prodi');
    }
}
