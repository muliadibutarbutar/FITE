<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class KategoriUnduhan extends Model
{
    protected $table = 'kategori_unduhan';
    protected $fillable = ['nama'];
}
