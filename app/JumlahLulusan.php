<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JumlahLulusan extends Model
{
    protected $table = 'jumlah_lulusan';
    protected $fillable = ['tahun_lulus','jumlah','id_prodi'];

    public function prodi()
    {
        return $this->belongsTo('App\ProgramStudi', 'id_prodi');
    }
}
