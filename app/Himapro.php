<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Himapro extends Model
{
    protected $table = 'himapro';
    protected $fillable = ['nama','aktifitas','deskripsi','gambar'];
}
