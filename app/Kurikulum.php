<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kurikulum extends Model
{
    protected $table = 'kurikulum';
    protected $fillable = ['id_prodi','id_tahun','pdf'];

    public function prodi()
    {
        return $this->belongsTo('App\ProgramStudi', 'id_prodi');
    }

    public function tahun()
    {
        return $this->belongsTo('App\Tahun', 'id_tahun');
    }
}
