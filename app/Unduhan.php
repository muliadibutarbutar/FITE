<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Unduhan extends Model
{
    protected $table = 'unduhan';
    protected $fillable = ['nama','pdf','id_kategori'];

    public function kategori()
    {
        return $this->belongsTo('App\KategoriUnduhan', 'id_kategori');
    }
}
