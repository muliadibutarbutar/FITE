<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pengabdian extends Model
{
    protected $table = 'pengabdian';
    protected $fillable = ['nama','gambar','deskripsi','tanggal'];
}
