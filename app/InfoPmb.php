<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InfoPmb extends Model
{
    protected $table = 'info_pmb';
    protected $fillable = ['jadwal1','jadwal2','jenis_tes','id_usm'];

    public function kategori()
    {
        return $this->belongsTo('App\KategoriPendaftaran', 'id_usm');
    }
}
