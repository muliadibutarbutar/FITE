<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EtikaProdi extends Model
{
    protected $table = 'etika_prodi';
    protected $fillable = ['etika_prodi', 'id_prodi'];

    public function prodi()
    {
        return $this->belongsTo('App\ProgramStudi', 'id_prodi');
    }
}
