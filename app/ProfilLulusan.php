<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProfilLulusan extends Model
{
    protected $table = 'profil_lulusan';
    protected $fillable = ['profil_lulusan','jenis_pekerjaan','deskripsi','id_prodi'];

    public function prodi()
    {
        return $this->belongsTo('App\ProgramStudi', 'id_prodi');
    }
}
