<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class KategoriPendaftaran extends Model
{
    protected $table = 'kategori_usm';
    protected $fillable = ['nama','link'];
}
