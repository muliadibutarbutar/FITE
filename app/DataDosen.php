<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DataDosen extends Model
{
    protected $table = 'data_dosen';
    protected $fillable = ['nama','gambar','riwayatpendidikan','link','id_prodi','id_kategori'];

    public function prodi()
    {
        return $this->belongsTo('App\ProgramStudi', 'id_prodi');
    }

    public function jabatan()
    {
        return $this->belongsTo('App\KategoriDosen', 'id_kategori');
    }
}
