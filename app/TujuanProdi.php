<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TujuanProdi extends Model
{
    protected $table = 'tujuan_prodi';
    protected $fillable = ['tujuan','id_prodi'];

    public function prodi()
    {
        return $this->belongsTo('App\ProgramStudi', 'id_prodi');
    }
}
