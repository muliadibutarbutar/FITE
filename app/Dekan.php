<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Dekan extends Model
{
    protected $table = 'dekan';
    protected $fillable = ['nama','foto','deskripsi'];
}
