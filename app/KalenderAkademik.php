<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class KalenderAkademik extends Model
{
    protected $table = 'kalender_akademik';
    protected $fillable = ['nama','kalender'];
}
