<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BeritaFakultas extends Model
{
    protected $table = 'berita_fakultas';
    protected $fillable = ['nama', 'gambar','deskripsi', 'id_user'];

    public function user()
    {
        return $this->belongsTo('App\User', 'id_user');
    }
}
