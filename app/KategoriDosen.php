<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class KategoriDosen extends Model
{
    protected $table = 'kategori_dosen';
    protected $fillable = ['nama'];
}
