<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class KalenderKemahasiswaan extends Model
{
    protected $table = 'kalender_kemahasiswaan';
    protected $fillable = ['nama','pdf'];
}
