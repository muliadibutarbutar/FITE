<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LokasiTes extends Model
{
    protected $table = 'lokasi_tes';
    protected $fillable = ['nama','lokasi'];
}
