<?php

namespace App\Http\Controllers;
use App\JumlahLulusan;
use App\ProgramStudi;
use Illuminate\Http\Request;

class JumlahLulusanController extends Controller
{
    public function index(){
        $jl = JumlahLulusan::all();
        return view('admin.jumlahlulusan.index', compact('jl'));
    }

    public function create(){
        $ka = ProgramStudi::all();
        return view('admin.jumlahlulusan.create', compact('ka'));
    }

    public function add(Request $request){
        $jl = JumlahLulusan::create([
            'tahun_lulus' => $request->tahun_lulus,
            'jumlah' => $request->jumlah,
            'id_prodi' => $request->id_prodi
        ]);
        return redirect('/alumni/jumlahlulusan');
    }

    public function edit($id){
        $bf = JumlahLulusan::find($id);
        $ka = ProgramStudi::all();
        return view('admin.jumlahlulusan.edit', compact('bf','ka'));
    }

    public function update(Request $request, $id){
        $jl = JumlahLulusan::find($id);
        $jl->update($request->all());
        return redirect('/alumni/jumlahlulusan');
    }

    public function hapus($id){
        \App\JumlahLulusan::find($id)->delete();
        return redirect('/alumni/jumlahlulusan');
    }
}
