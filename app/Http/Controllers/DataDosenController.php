<?php

namespace App\Http\Controllers;
use App\DataDosen;
use App\ProgramStudi;
use App\KategoriDosen;

use Illuminate\Http\Request;

class DataDosenController extends Controller
{
    public function index(){
        $bf = DataDosen::all();
        return view('admin.datadosen.index', compact('bf'));
    }

    public function create(){
        $ps = ProgramStudi::all();
        $ka = KategoriDosen::all();
        return view('admin.datadosen.create', compact('ps','ka'));
    }

    public function add(Request $request){
        $bf = DataDosen::create($request->all());
        if($request->hasFile('gambar')){
            $request->file('gambar')->move('images/',$request->file('gambar')->getClientOriginalName());
            $bf->gambar = $request->file('gambar')->getClientOriginalName();
            $bf->save();
        }
        return redirect('/data-dosen/data-dosen');
    }

    public function show($id){
        $bf = DataDosen::find($id);
        return view('admin.datadosen.show', compact('bf'));
    }

    public function edit($id){
        $bf = DataDosen::find($id);
        $ka = KategoriDosen::all();
        return view('admin.datadosen.edit', compact('bf','ka'));
    }

    public function update(Request $request, $id){
        $bf = DataDosen::find($id);
        $bf->update($request->all());
        if($request->hasFile('gambar')){
            $request->file('gambar')->move('images/',$request->file('gambar')->getClientOriginalName());
            $bf->gambar = $request->file('gambar')->getClientOriginalName();
            $bf->save();
        }
        return redirect('/data-dosen/data-dosen');
    }

    public function hapus($id){
        \App\DataDosen::find($id)->delete();
        return redirect('/data-dosen/data-dosen');
    }
}
