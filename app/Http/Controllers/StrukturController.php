<?php

namespace App\Http\Controllers;
use App\Struktur;

use Illuminate\Http\Request;

class StrukturController extends Controller
{
    public function index(){
        $fs = Struktur::all();
        return view('admin.struktur.index', compact('fs'));
    }

    public function create(){
        return view('admin.struktur.create');
    }

    public function add(Request $request){
        $fs = Struktur::create($request->all());
        if($request->hasFile('gambar')){
            $request->file('gambar')->move('images/',$request->file('gambar')->getClientOriginalName());
            $fs->gambar = $request->file('gambar')->getClientOriginalName();
            $fs->save();
        }
        return redirect('/admin-tentang/struktur');
    }

    public function edit($id){
        $fs = Struktur::find($id);
        return view('admin.struktur.edit', compact('fs'));
    }

    public function update(Request $request, $id){
        $fs = Struktur::find($id);
        $fs->update($request->all());
        if($request->hasFile('gambar')){
            $request->file('gambar')->move('images/',$request->file('gambar')->getClientOriginalName());
            $fs->gambar = $request->file('gambar')->getClientOriginalName();
            $fs->save();
        }
        return redirect('/admin-tentang/struktur');
    }

    public function hapus($id){
        \App\Struktur::find($id)->delete();
        return redirect('/admin-tentang/struktur');
    }
}
