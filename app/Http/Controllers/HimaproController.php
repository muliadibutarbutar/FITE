<?php

namespace App\Http\Controllers;
use App\Himapro;
use Illuminate\Http\Request;

class HimaproController extends Controller
{
    public function index(){
        $bf = Himapro::all();
        return view('admin.himapro.index', compact('bf'));
    }

    public function index2(){
        $himapro = Himapro::all();
        return view('user.himapro.index', compact('himapro'));
    }

    public function create(){
        return view('admin.himapro.create');
    }

    public function add(Request $request){
        $bf = Himapro::create($request->all());
        if($request->hasFile('gambar')){
            $request->file('gambar')->move('images/',$request->file('gambar')->getClientOriginalName());
            $bf->gambar = $request->file('gambar')->getClientOriginalName();
            $bf->save();
        }
        return redirect('/kemahasiswaan/himapro');
    }

    public function show($id){
        $bf = Himapro::find($id);
        return view('admin.himapro.show', compact('bf'));
    }

    public function edit($id){
        $bf = Himapro::find($id);
        return view('admin.himapro.edit', compact('bf'));
    }

    public function update(Request $request, $id){
        $bf = Himapro::find($id);
        $bf->update($request->all());
        if($request->hasFile('gambar')){
            $request->file('gambar')->move('images/',$request->file('gambar')->getClientOriginalName());
            $bf->gambar = $request->file('gambar')->getClientOriginalName();
            $bf->save();
        }
        return redirect('/kemahasiswaan/himapro');
    }

    public function hapus($id){
        \App\Himapro::find($id)->delete();
        return redirect('/kemahasiswaan/himapro');
    }
}
