<?php

namespace App\Http\Controllers;
use App\LokasiTes;

use Illuminate\Http\Request;

class LokasiTesController extends Controller
{
    public function index(){
        $k = LokasiTes::all();
        return view('admin.lokasi_usm.index', compact('k'));
    }

    public function create(){
        return view('admin.lokasi_usm.create');
    }

    public function add(Request $request){
        $k = LokasiTes::create($request->all());
        return redirect('/infopmb/lokasi');
    }

    public function edit($id){
        $k = LokasiTes::find($id);
        return view('admin.lokasi_usm.edit', compact('k'));
    }

    public function update(Request $request, $id){
        $k = LokasiTes::find($id);
        $k->update($request->all());
        return redirect('/infopmb/lokasi');
    }

    public function hapus($id){
        \App\LokasiTes::find($id)->delete();
        return redirect('/infopmb/lokasi');
    }
}
