<?php

namespace App\Http\Controllers;
use App\Kurikulum;
use App\Tahun;
use App\ProgramStudi;

use Illuminate\Http\Request;

class KurikulumController extends Controller
{
    public function index(){
        $k = Kurikulum::all();
        return view('admin.kurikulum.index', compact('k'));
    }

    public function index2(){
        $ps = ProgramStudi::all();
        $ta = Tahun::all();
        return view('user.kurikulum.index', compact('ps','ta'));
    }

    public function create(){
        $ka = ProgramStudi::all();
        $ta = Tahun::all();
        return view('admin.kurikulum.create', compact('ka','ta'));
    }

    public function show2($id){
        $kurikulum = Kurikulum::whereIdProdi($id)->get();
        return view('user.kurikulum.show', compact('kurikulum'));
    }

    public function add(Request $request){
        $bf = Kurikulum::create($request->all());
        if($request->hasFile('pdf')){
            $request->file('pdf')->move('images/',$request->file('pdf')->getClientOriginalName());
            $bf->pdf = $request->file('pdf')->getClientOriginalName();
            $bf->save();
        }
        return redirect('/akademik/kurikulum');
    }

    public function edit($id){
        $k = Kurikulum::find($id);
        $ka = ProgramStudi::all();
        $ta = Tahun::all();
        return view('admin.kurikulum.edit', compact('k','ka','ta'));
    }

    public function update(Request $request, $id){
        $bf = Kurikulum::find($id);
        $bf->update($request->all());
        if($request->hasFile('pdf')){
            $request->file('pdf')->move('images/',$request->file('pdf')->getClientOriginalName());
            $bf->pdf = $request->file('pdf')->getClientOriginalName();
            $bf->save();
        }
        return redirect('/akademik/kurikulum');
    }

    public function hapus($id){
        \App\Kurikulum::find($id)->delete();
        return redirect('/akademik/kurikulum');
    }
}
