<?php

namespace App\Http\Controllers;
use App\Gambar;

use Illuminate\Http\Request;

class GambarController extends Controller
{
    public function index(){
        $fs = Gambar::all();
        return view('admin.gambar.index', compact('fs'));
    }

    public function create(){
        return view('admin.gambar.create');
    }

    public function add(Request $request){
        $fs = Gambar::create($request->all());
        if($request->hasFile('gambar')){
            $request->file('gambar')->move('images/',$request->file('gambar')->getClientOriginalName());
            $fs->gambar = $request->file('gambar')->getClientOriginalName();
            $fs->save();
        }
        return redirect('/gambar');
    }

    public function edit($id){
        $fs = Gambar::find($id);
        return view('admin.gambar.edit', compact('fs'));
    }

    public function update(Request $request, $id){
        $fs = Gambar::find($id);
        $fs->update($request->all());
        if($request->hasFile('gambar')){
            $request->file('gambar')->move('images/',$request->file('gambar')->getClientOriginalName());
            $fs->gambar = $request->file('gambar')->getClientOriginalName();
            $fs->save();
        }
        return redirect('/gambar');
    }

    public function hapus($id){
        \App\Gambar::find($id)->delete();
        return redirect('/gambar');
    }
}
