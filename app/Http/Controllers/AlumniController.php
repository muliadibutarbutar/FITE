<?php

namespace App\Http\Controllers;
use App\Alumni;
use App\ProgramStudi;
use App\JumlahLulusan;

use Illuminate\Http\Request;

class AlumniController extends Controller
{
    public function index(){
        $bf = Alumni::all();
        return view('admin.alumni.index', compact('bf'));
    }

    public function index2(){
        $al = Alumni::all();
        $ps = ProgramStudi::all();
        $jl = JumlahLulusan::all();
        return view('user.alumni.index', compact('al','ps','jl'));
    }

    public function create(){
        $ka = ProgramStudi::all();
        return view('admin.alumni.create', compact('ka'));
    }

    public function add(Request $request){
        $bf = Alumni::create($request->all());
        if($request->hasFile('testimoni')){
            $request->file('testimoni')->move('images/',$request->file('testimoni')->getClientOriginalName());
            $bf->testimoni = $request->file('testimoni')->getClientOriginalName();
            $bf->save();
        }
        return redirect('/alumni/alumni');
    }

    public function show($id){
        $bf = Alumni::find($id);
        return view('admin.alumni.show', compact('bf'));
    }

    public function show2($id){
        $al = Alumni::whereIdProdi($id)->get();
        return view('user.alumni.show', compact('al'));
    }

    public function edit($id){
        $ka = ProgramStudi::all();
        $bf = Alumni::find($id);
        return view('admin.alumni.edit', compact('bf','ka'));
    }

    public function update(Request $request, $id){
        $bf = Alumni::find($id);
        $bf->update($request->all());
        if($request->hasFile('gambar')){
            $request->file('gambar')->move('images/',$request->file('gambar')->getClientOriginalName());
            $bf->gambar = $request->file('gambar')->getClientOriginalName();
            $bf->save();
        }
        return redirect('/alumni/alumni');
    }

    public function hapus($id){
        \App\Alumni::find($id)->delete();
        return redirect('/alumni/alumni');
    }
}
