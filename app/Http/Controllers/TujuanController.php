<?php

namespace App\Http\Controllers;
use App\Tujuan;

use Illuminate\Http\Request;

class TujuanController extends Controller
{
    public function index(){
        $k = Tujuan::all();
        return view('admin.tujuan.index', compact('k'));
    }

    public function create(){
        return view('admin.tujuan.create');
    }

    public function add(Request $request){
        $k = Tujuan::create($request->all());
        return redirect('/admin-tentang/tujuan');
    }

    public function edit($id){
        $k = Tujuan::find($id);
        return view('admin.tujuan.edit', compact('k'));
    }

    public function update(Request $request, $id){
        $k = Tujuan::find($id);
        $k->tujuan = $request->tujuan;
        $k->save();
        return redirect('/admin-tentang/tujuan');
    }

    public function hapus($id){
        \App\Tujuan::find($id)->delete();
        return redirect('/admin-tentang/tujuan');
    }
}
