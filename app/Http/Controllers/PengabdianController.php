<?php

namespace App\Http\Controllers;
use App\Pengabdian;

use Illuminate\Http\Request;

class PengabdianController extends Controller
{
    public function index(){
        $fs = Pengabdian::all();
        return view('admin.pengabdian.index', compact('fs'));
    }

    public function index2(){
        $fs = Pengabdian::all();
        return view('user.pengabdian.index', compact('fs'));
    }

    public function create(){
        return view('admin.pengabdian.create');
    }

    public function add(Request $request){
        $fs = Pengabdian::create($request->all());
        if($request->hasFile('gambar')){
            $request->file('gambar')->move('images/',$request->file('gambar')->getClientOriginalName());
            $fs->gambar = $request->file('gambar')->getClientOriginalName();
            $fs->save();
        }
        return redirect('/tridharma/pengabdian');
    }

    public function edit($id){
        $fs = Pengabdian::find($id);
        return view('admin.pengabdian.edit', compact('fs'));
    }

    public function update(Request $request, $id){
        $fs = Pengabdian::find($id);
        $fs->update($request->all());
        if($request->hasFile('gambar')){
            $request->file('gambar')->move('images/',$request->file('gambar')->getClientOriginalName());
            $fs->gambar = $request->file('gambar')->getClientOriginalName();
            $fs->save();
        }
        return redirect('/tridharma/pengabdian');
    }

    public function hapus($id){
        \App\Pengabdian::find($id)->delete();
        return redirect('/tridharma/pengabdian');
    }
}
