<?php

namespace App\Http\Controllers;
use App\VisiMisi;

use Illuminate\Http\Request;

class VisiMisiController extends Controller
{
    public function index(){
        $vs = VisiMisi::take(1)->first();
        return view('admin.visi_misi.index', compact('vs'));
    }

    public function create(){
        return view('admin.visi_misi.create');
    }

    public function add(Request $request){
        $visimisi = VisiMisi::create([
            'misi' => $request->misi,
            'visi' => $request->visi
        ]);

        return redirect('/admin-tentang/visimisifakultas');
    }

    public function edit($id){
        $vs = VisiMisi::find($id);
        return view('admin.visi_misi.edit', compact('vs'));
    }

    public function update(Request $request, $id){
        $vs = VisiMisi::find($id);
        $vs->visi = $request->visi;
        $vs->misi = $request->misi;
        $vs->save();

        return redirect('/admin-tentang/visimisifakultas');
    }
}
