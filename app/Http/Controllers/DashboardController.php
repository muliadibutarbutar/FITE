<?php

namespace App\Http\Controllers;
use App\Fasilitas;
use App\BeritaFakultas;
use App\Dekan;
use App\Struktur;
use App\Kerjasama;
use App\Alumni;
use App\Slider;

use Illuminate\Http\Request;

class DashboardController extends Controller
{
    // public function index() {
    //     return view('dashboard');
    // }

    public function index(){
        $fasilitas = Fasilitas::limit(3)->get();
        $berita = BeritaFakultas::latest()->take(3)->get();
        $dekan = Dekan::take(1)->first();
        $kerjasama = Kerjasama::all();
        $alumni = Alumni::latest()->take(3)->get();
        $sliderLast = Slider::latest()->take(1)->get();
        $slider = Slider::all();
        $kerjasamaFiveLatest = Kerjasama::latest()->take(5)->get();
        $kerjasamaRandomFiveToOne = Kerjasama::all()->random(5);
        $kerjasamaRandomFiveToTwo = Kerjasama::all()->random(5);
        return view('user.dashboard.index', compact('fasilitas','berita','dekan','kerjasama','alumni','slider','sliderLast','kerjasamaFiveLatest','kerjasamaRandomFiveToOne','kerjasamaRandomFiveToTwo'));
    }
}
