<?php
namespace App\Http\Controllers;
use App\BeritaFakultas;

use Illuminate\Http\Request;

class BeritaFakultasController extends Controller
{
    public function index(){
        $bf = BeritaFakultas::all();
        return view('admin.beritafakultas.index', compact('bf'));
    }

    public function index2(){
        $berita = BeritaFakultas::all();
        return view('user.berita.index', compact('berita'));
    }

    public function create(){
        return view('admin.beritafakultas.create');
    }

    public function add(Request $request){
        $bf = BeritaFakultas::create($request->all());
        if($request->hasFile('gambar')){
            $request->file('gambar')->move('images/',$request->file('gambar')->getClientOriginalName());
            $bf->gambar = $request->file('gambar')->getClientOriginalName();
            $bf->save();
        }
        return redirect('/beritafakultas');
    }

    public function show($id){
        $bf = BeritaFakultas::find($id);
        return view('admin.beritafakultas.show', compact('bf'));
    }

    public function show2($id){
        $f = BeritaFakultas::find($id);
        return view('user.berita.show', compact('f'));
    }

    public function edit($id){
        $bf = BeritaFakultas::find($id);
        return view('admin.beritafakultas.edit', compact('bf'));
    }

    public function update(Request $request, $id){
        $bf = BeritaFakultas::find($id);
        $bf->update($request->all());
        if($request->hasFile('gambar')){
            $request->file('gambar')->move('images/',$request->file('gambar')->getClientOriginalName());
            $bf->gambar = $request->file('gambar')->getClientOriginalName();
            $bf->save();
        }
        return redirect('/beritafakultas');
    }

    public function hapus($id){
        \App\BeritaFakultas::find($id)->delete();
        return redirect('/beritafakultas');
    }
}
