<?php

namespace App\Http\Controllers;
use App\Unduhan;
use Auth;
use App\KategoriUnduhan;

use Illuminate\Http\Request;

class UnduhanController extends Controller
{
    public function index(){
        $un = Unduhan::all();
        return view('admin.unduhan_down.index', compact('un'));
    }

    public function index2(){
        $un = Unduhan::all();
        return view('user.unduhan.index', compact('un'));
    }

    public function create(){
        $ka = KategoriUnduhan::all();
        return view('admin.unduhan_down.create', compact('ka'));
    }

    public function add(Request $request){
        $ka = Unduhan::create($request->all());
        if($request->hasFile('pdf')){
            $request->file('pdf')->move('images/',$request->file('pdf')->getClientOriginalName());
            $ka->pdf = $request->file('pdf')->getClientOriginalName();
            $ka->save();
        }
        return redirect('/unduhan/unduhan');
    }

    public function edit($id){
        $ka = KategoriUnduhan::all();
        $un = Unduhan::find($id);
        return view('admin.unduhan_down.edit', compact('un','ka'));
    }

    public function update(Request $request, $id){
        $un = Unduhan::find($id);
        $un->update($request->all());
        if($request->hasFile('pdf')){
            $request->file('pdf')->move('images/',$request->file('pdf')->getClientOriginalName());
            $un->pdf = $request->file('pdf')->getClientOriginalName();
            $un->save();
        }
        return redirect('/unduhan/unduhan');
    }

    public function hapus($id){
        \App\Unduhan::find($id)->delete();
        return redirect('/unduhan/unduhan');
    }
}
