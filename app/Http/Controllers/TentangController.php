<?php

namespace App\Http\Controllers;
use App\Tentang;
use App\Struktur;
use App\VisiMisi;
use App\Tujuan;

use Illuminate\Http\Request;

class TentangController extends Controller
{
    public function index(){
        $t = Tentang::take(1)->first();
        return view('admin.tentang.index', compact('t'));
    }

    public function index2(){
        $t = Tentang::take(1)->first();
        $st = Struktur::take(1)->first();
        $vs = VisiMisi::take(1)->first();
        $tj = Tujuan::take(1)->first();
        return view('user.tentang.index', compact('t','st','vs','tj'));
    }

    public function create(){
        return view('admin.tentang.create');
    }

    public function add(Request $request){
        $r = Tentang::create([
            'tentang' => $request->tentang
        ]);
        return redirect('/admin-tentang/tentang');
    }

    public function edit($id){
        $t = Tentang::find($id);
        return view('admin.tentang.edit', compact('t'));
    }

    public function update(Request $request, $id){
        $t = Tentang::find($id);
        $t->tentang = $request->tentang;
        $t->save();

        return redirect('/admin-tentang/tentang');
    }
}
