<?php

namespace App\Http\Controllers;
use App\Beasiswa;

use Illuminate\Http\Request;

class BeasiswaController extends Controller
{
    public function index(){
        $fs = Beasiswa::all();
        return view('admin.beasiswa.index', compact('fs'));
    }

    public function index2(){
        $beasiswa = Beasiswa::all();
        return view('user.beasiswa.index', compact('beasiswa'));
    }

    public function create(){
        return view('admin.beasiswa.create');
    }

    public function add(Request $request){
        $fs = Beasiswa::create($request->all());
        if($request->hasFile('logo')){
            $request->file('logo')->move('images/',$request->file('logo')->getClientOriginalName());
            $fs->logo = $request->file('logo')->getClientOriginalName();
            $fs->save();
        }
        return redirect('/akademik/beasiswa');
    }

    public function edit($id){
        $fs = Beasiswa::find($id);
        return view('admin.beasiswa.edit', compact('fs'));
    }

    public function update(Request $request, $id){
        $fs = Beasiswa::find($id);
        $fs->update($request->all());
        if($request->hasFile('logo')){
            $request->file('logo')->move('images/',$request->file('logo')->getClientOriginalName());
            $fs->logo = $request->file('logo')->getClientOriginalName();
            $fs->save();
        }
        return redirect('/akademik/beasiswa');
    }

    public function hapus($id){
        \App\Beasiswa::find($id)->delete();
        return redirect('/akademik/beasiswa');
    }
}
