<?php

namespace App\Http\Controllers;
use App\Semester;

use Illuminate\Http\Request;

class SemesterController extends Controller
{
    public function index(){
        $k = Semester::all();
        return view('admin.semester.index', compact('k'));
    }

    public function create(){
        return view('admin.semester.create');
    }

    public function add(Request $request){
        $k = Semester::create($request->all());
        return redirect('/master-data/semester');
    }

    public function edit($id){
        $k = Semester::find($id);
        return view('admin.semester.edit', compact('k'));
    }

    public function update(Request $request, $id){
        $k = Semester::find($id);
        $k->update($request->all());
        return redirect('/master-data/semester');
    }

    public function hapus($id){
        \App\Semester::find($id)->delete();
        return redirect('/master-data/semester');
    }
}
