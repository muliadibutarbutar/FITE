<?php

namespace App\Http\Controllers;
use App\Penelitian;

use Illuminate\Http\Request;

class PenelitianController extends Controller
{
    public function index(){
        $fs = Penelitian::all();
        return view('admin.penelitian.index', compact('fs'));
    }

    public function index2(){
        $fs = Penelitian::all();
        return view('user.penelitian.index', compact('fs'));
    }

    public function create(){
        return view('admin.penelitian.create');
    }

    public function add(Request $request){
        $fs = Penelitian::create($request->all());
        if($request->hasFile('gambar')){
            $request->file('gambar')->move('images/',$request->file('gambar')->getClientOriginalName());
            $fs->gambar = $request->file('gambar')->getClientOriginalName();
            $fs->save();
        }
        return redirect('/tridharma/penelitian');
    }

    public function edit($id){
        $fs = Penelitian::find($id);
        return view('admin.penelitian.edit', compact('fs'));
    }

    public function update(Request $request, $id){
        $fs = Penelitian::find($id);
        $fs->update($request->all());
        if($request->hasFile('gambar')){
            $request->file('gambar')->move('images/',$request->file('gambar')->getClientOriginalName());
            $fs->gambar = $request->file('gambar')->getClientOriginalName();
            $fs->save();
        }
        return redirect('/tridharma/penelitian');
    }

    public function hapus($id){
        \App\Penelitian::find($id)->delete();
        return redirect('/tridharma/penelitian');
    }
}
