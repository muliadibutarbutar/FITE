<?php

namespace App\Http\Controllers;
use App\Pendidikan;

use Illuminate\Http\Request;

class PendidikanController extends Controller
{
    public function index(){
        $fs = Pendidikan::all();
        return view('admin.pendidikan.index', compact('fs'));
    }

    public function index2(){
        $fs = Pendidikan::all();
        return view('user.pendidikan.index', compact('fs'));
    }

    public function create(){
        return view('admin.pendidikan.create');
    }

    public function add(Request $request){
        $fs = Pendidikan::create($request->all());
        if($request->hasFile('gambar')){
            $request->file('gambar')->move('images/',$request->file('gambar')->getClientOriginalName());
            $fs->gambar = $request->file('gambar')->getClientOriginalName();
            $fs->save();
        }
        return redirect('/tridharma/pendidikan');
    }

    public function edit($id){
        $fs = Pendidikan::find($id);
        return view('admin.pendidikan.edit', compact('fs'));
    }

    public function update(Request $request, $id){
        $fs = Pendidikan::find($id);
        $fs->update($request->all());
        if($request->hasFile('gambar')){
            $request->file('gambar')->move('images/',$request->file('gambar')->getClientOriginalName());
            $fs->gambar = $request->file('gambar')->getClientOriginalName();
            $fs->save();
        }
        return redirect('/tridharma/pendidikan');
    }

    public function hapus($id){
        \App\Pendidikan::find($id)->delete();
        return redirect('/tridharma/pendidikan');
    }
}
