<?php

namespace App\Http\Controllers;
use App\Dekan;

use Illuminate\Http\Request;

class DekanController extends Controller
{
    public function index(){
        $dekan = Dekan::all();
        return view('admin.dekan.index', compact('dekan'));
    }

    public function create(){
        return view('admin.dekan.create');
    }

    public function add(Request $request){
        $fs = Dekan::create($request->all());
        if($request->hasFile('foto')){
            $request->file('foto')->move('images/',$request->file('foto')->getClientOriginalName());
            $fs->foto = $request->file('foto')->getClientOriginalName();
            $fs->save();
        }
        return redirect('admin-tentang/dekan');
    }

    public function edit($id){
        $fs = Dekan::find($id);
        return view('admin.dekan.edit', compact('fs'));
    }

    public function update(Request $request, $id){
        $fs = Dekan::find($id);
        $fs->update($request->all());
        if($request->hasFile('foto')){
            $request->file('foto')->move('images/',$request->file('foto')->getClientOriginalName());
            $fs->foto = $request->file('foto')->getClientOriginalName();
            $fs->save();
        }
        return redirect('/admin-tentang/dekan');
    }

    public function hapus($id){
        \App\Dekan::find($id)->delete();
        return redirect('/admin-tentang/dekan');
    }
}
