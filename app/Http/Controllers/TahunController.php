<?php

namespace App\Http\Controllers;
use App\Tahun;

use Illuminate\Http\Request;

class TahunController extends Controller
{
    public function index(){
        $k = Tahun::all();
        return view('admin.tahun.index', compact('k'));
    }

    public function create(){
        return view('admin.tahun.create');
    }

    public function add(Request $request){
        $k = Tahun::create($request->all());
        return redirect('/master-data/tahun');
    }

    public function edit($id){
        $k = Tahun::find($id);
        return view('admin.tahun.edit', compact('k'));
    }

    public function update(Request $request, $id){
        $k = Tahun::find($id);
        $k->update($request->all());
        return redirect('/master-data/tahun');
    }

    public function hapus($id){
        \App\Tahun::find($id)->delete();
        return redirect('/master-data/tahun');
    }
}
