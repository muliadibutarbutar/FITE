<?php

namespace App\Http\Controllers;
use App\ProgramStudi;
use App\VisiMisiProdi;

use Illuminate\Http\Request;

class VisiMisiProdiController extends Controller
{
    public function create($id){
        $ps = ProgramStudi::find($id);
        return view('admin.program_studi.visi_misi_prodi.create',compact('ps'));
    }

    public function add(Request $request){
        $id = $request->id_prodi;

        $ps = VisiMisiProdi::create([
            'visi' => $request->visi,
            'misi' => $request->misi,
            'id_prodi' => $request->id_prodi
        ]);
        return redirect('/programstudi/programstudi');
    }

    public function edit($id){
        $ps = VisiMisiProdi::where('id_prodi', $id)->first();
        return view('admin.program_studi.visi_misi_prodi.edit', compact('ps','id'));
    }

    public function update(Request $request, $id){
        $vs = VisiMisiProdi::find($id);
        $vs->visi = $request->visi;
        $vs->misi = $request->misi;
        $vs->save();
        return redirect('/programstudi/programstudi/show/'.$id);
    }
}
