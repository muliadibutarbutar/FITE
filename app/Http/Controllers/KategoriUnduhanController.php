<?php

namespace App\Http\Controllers;
use App\KategoriUnduhan;

use Illuminate\Http\Request;

class KategoriUnduhanController extends Controller
{
    public function index(){
        $k = KategoriUnduhan::all();
        return view('admin.kategori_unduhan.index', compact('k'));
    }

    public function create(){
        return view('admin.kategori_unduhan.create');
    }

    public function add(Request $request){
        $k = KategoriUnduhan::create($request->all());
        return redirect('/unduhan/kategoriunduhan');
    }

    public function edit($id){
        $k = KategoriUnduhan::find($id);
        return view('admin.kategori_unduhan.edit', compact('k'));
    }

    public function update(Request $request, $id){
        $k = KategoriUnduhan::find($id);
        $k->update($request->all());
        return redirect('/unduhan/kategoriunduhan');
    }

    public function hapus($id){
        \App\KategoriUnduhan::find($id)->delete();
        return redirect('/unduhan/kategoriunduhan');
    }
}
