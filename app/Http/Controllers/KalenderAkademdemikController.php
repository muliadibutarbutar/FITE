<?php

namespace App\Http\Controllers;
use App\KalenderAkademik;
use App\KalenderKemahasiswaan;

use Illuminate\Http\Request;

class KalenderAkademdemikController extends Controller
{
    public function index(){
        $un = KalenderAkademik::all();
        return view('admin.kalender.index', compact('un'));
    }

    public function index2(){
        $ka = KalenderAkademik::all();
        $ut = KalenderKemahasiswaan::all();
        return view('user.kalender.index', compact('ka','ut'));
    }

    public function create(){
        return view('admin.kalender.create');
    }

    public function add(Request $request){
        $ka = KalenderAkademik::create($request->all());
        if($request->hasFile('kalender')){
            $request->file('kalender')->move('images/',$request->file('kalender')->getClientOriginalName());
            $ka->kalender = $request->file('kalender')->getClientOriginalName();
            $ka->save();
        }
        return redirect('/akademik/kalender');
    }

    public function edit($id){
        $un = KalenderAkademik::find($id);
        return view('admin.kalender.edit', compact('un'));
    }

    public function update(Request $request, $id){
        $un = KalenderAkademik::find($id);
        $un->update($request->all());
        if($request->hasFile('kalender')){
            $request->file('kalender')->move('images/',$request->file('kalender')->getClientOriginalName());
            $un->kalender = $request->file('kalender')->getClientOriginalName();
            $un->save();
        }
        return redirect('/akademik/kalender');
    }

    public function hapus($id){
        \App\KalenderAkademik::find($id)->delete();
        return redirect('/akademik/kalender');
    }
}
