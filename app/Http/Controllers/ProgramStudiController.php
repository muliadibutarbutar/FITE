<?php

namespace App\Http\Controllers;
use App\ProgramStudi;
use App\VisiMisiProdi;
use App\TujuanProdi;
use App\KompetensiProdi;
use App\EtikaProdi;
use App\ProfilLulusan;
use App\Kurikulum;
use App\DataDosen;

use Illuminate\Http\Request;

class ProgramStudiController extends Controller
{
    public function index(){
        $ps = ProgramStudi::all();
        return view('admin.program_studi.index', compact('ps'));
    }

    public function index2($id){
        $ps = ProgramStudi::find($id);
        $vs = VisiMisiProdi::where('id_prodi', $id)->first();
        $tj = TujuanProdi::where('id_prodi', $id)->first();
        $kp = KompetensiProdi::where('id_prodi', $id)->first();
        $kurikulum = Kurikulum::whereIdProdi($id)->get();
        $dosen = DataDosen::whereIdProdi($id)->get();
        $etika = EtikaProdi::whereIdProdi($id)->first();
        return view('user.programstudi.index', compact('etika','ps','vs','tj','kp','kurikulum','dosen'));
    }

    public function create(){
        return view('admin.program_studi.create');
    }

    public function add(Request $request){
        $ps = ProgramStudi::create([
            'kode_prodi' => $request->kode_prodi,
            'nama_prodi' => $request->nama_prodi,
            'deskripsi' => $request->deskripsi
        ]);
        return redirect('/programstudi/programstudi');
    }

    public function edit($id){
        $ps = ProgramStudi::find($id);
        return view('admin.program_studi.edit', compact('ps'));
    }

    public function update(Request $request, $id){
        $ps = ProgramStudi::find($id);
        $ps->kode_prodi = $request->kode_prodi;
        $ps->nama_prodi = $request->nama_prodi;
        $ps->deskripsi = $request->deskripsi;
        $ps->save();

        return redirect('/programstudi/programstudi');
    }

    public function show($id){
        $ps = ProgramStudi::find($id);
        $visimisiprodi = VisiMisiProdi::where('id_prodi',$id)->get();
        $tujuanprodi = TujuanProdi::where('id_prodi', $id)->get();
        $kompetensiprodi = KompetensiProdi::where('id_prodi', $id)->get();
        $etikaprodi = EtikaProdi::where('id_prodi', $id)->get();
        $profillulusan = ProfilLulusan::where('id_prodi', $id)->get();
        return view('admin.program_studi.show', compact('ps', 'visimisiprodi', 'tujuanprodi','kompetensiprodi', 'etikaprodi','profillulusan'));
        // dd($visimisiprodi->visi);
    }

    public function hapus($id){
        \App\ProgramStudi::find($id)->delete();
        return redirect('/programstudi/programstudi');
    }
}
