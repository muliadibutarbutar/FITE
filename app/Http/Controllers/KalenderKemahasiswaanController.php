<?php

namespace App\Http\Controllers;
use App\KalenderKemahasiswaan;

use Illuminate\Http\Request;

class KalenderKemahasiswaanController extends Controller
{
    public function index(){
        $fs = KalenderKemahasiswaan::all();
        return view('admin.kalender_kemahasiswaan.index', compact('fs'));
    }

    public function index2(){
        $ka = KalenderKemahasiswaan::all();
        return view('user.kalender_kemahasiswaan.index', compact('ka'));
    }

    public function create(){
        return view('admin.kalender_kemahasiswaan.create');
    }

    public function add(Request $request){
        $fs = KalenderKemahasiswaan::create($request->all());
        if($request->hasFile('pdf')){
            $request->file('pdf')->move('images/',$request->file('pdf')->getClientOriginalName());
            $fs->pdf = $request->file('pdf')->getClientOriginalName();
            $fs->save();
        }
        return redirect('/akademik/kalenderkemahasiswaan');
    }

    public function edit($id){
        $fs = KalenderKemahasiswaan::find($id);
        return view('admin.kalender_kemahasiswaan.edit', compact('fs'));
    }

    public function update(Request $request, $id){
        $fs = KalenderKemahasiswaan::find($id);
        $fs->update($request->all());
        if($request->hasFile('pdf')){
            $request->file('pdf')->move('images/',$request->file('pdf')->getClientOriginalName());
            $fs->pdf = $request->file('pdf')->getClientOriginalName();
            $fs->save();
        }
        return redirect('/akademik/kalenderkemahasiswaan');
    }

    public function hapus($id){
        \App\KalenderKemahasiswaan::find($id)->delete();
        return redirect('/akademik/kalenderkemahasiswaan');
    }
}
