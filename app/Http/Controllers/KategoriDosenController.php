<?php

namespace App\Http\Controllers;
use App\KategoriDosen;

use Illuminate\Http\Request;

class KategoriDosenController extends Controller
{
    public function index(){
        $fs = KategoriDosen::all();
        return view('admin.kategori_dosen.index', compact('fs'));
    }

    public function create(){
        return view('admin.kategori_dosen.create');
    }

    public function add(Request $request){
        $fs = KategoriDosen::create($request->all());
        if($request->hasFile('gambar')){
            $request->file('gambar')->move('images/',$request->file('gambar')->getClientOriginalName());
            $fs->gambar = $request->file('gambar')->getClientOriginalName();
            $fs->save();
        }
        return redirect('/data-dosen/kategoridosen');
    }

    public function edit($id){
        $fs = KategoriDosen::find($id);
        return view('admin.kategori_dosen.edit', compact('fs'));
    }

    public function update(Request $request, $id){
        $fs = KategoriDosen::find($id);
        $fs->update($request->all());
        if($request->hasFile('gambar')){
            $request->file('gambar')->move('images/',$request->file('gambar')->getClientOriginalName());
            $fs->gambar = $request->file('gambar')->getClientOriginalName();
            $fs->save();
        }
        return redirect('/data-dosen/kategoridosen');
    }

    public function hapus($id){
        \App\KategoriDosen::find($id)->delete();
        return redirect('/data-dosen/kategoridosen');
    }
}
