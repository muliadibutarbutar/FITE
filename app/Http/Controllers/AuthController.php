<?php

namespace App\Http\Controllers;
use App\User;
use Auth;
use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function index(){
        return view('admin.auth.index');
    }

    public function postlogin(Request $request){
        if(Auth::attempt(['email' => $request->email, 'password' => $request->password, 'role' => 'admin'])){
            return redirect('/dashboard');
        }
        return redirect('/login')->with('gagal','Invalid login, please try again');
    }

    public function logout(){
        Auth::logout();
        return redirect('/login');
    }
}
