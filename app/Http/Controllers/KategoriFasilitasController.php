<?php

namespace App\Http\Controllers;
use App\KategoriFasilitas;

use Illuminate\Http\Request;

class KategoriFasilitasController extends Controller
{
    public function index(){
        $fs = KategoriFasilitas::all();
        return view('admin.kategori_fasilitas.index', compact('fs'));
    }

    public function create(){
        return view('admin.kategori_fasilitas.create');
    }

    public function add(Request $request){
        $fs = KategoriFasilitas::create($request->all());
        return redirect('/fasilitas/kategorifasilitas');
    }

    public function edit($id){
        $fs = KategoriFasilitas::find($id);
        return view('admin.kategori_fasilitas.edit', compact('fs'));
    }

    public function update(Request $request, $id){
        $fs = KategoriFasilitas::find($id);
        $fs->update($request->all());
        return redirect('/fasilitas/kategorifasilitas');
    }

    public function hapus($id){
        \App\KategoriFasilitas::find($id)->delete();
        return redirect('/fasilitas/kategorifasilitas');
    }
}
