<?php

namespace App\Http\Controllers;
use App\ProgramStudi;
use App\KompetensiProdi;

use Illuminate\Http\Request;

class KompetensiProdiController extends Controller
{
    public function create($id){
        $ps = ProgramStudi::find($id);
        return view('admin.program_studi.kompetensi.create', compact('ps'));
    }

    public function add(Request $request){
        $id = $request->id_prodi;

        $ps = KompetensiProdi::create([
            'kompetensi' => $request->kompetensi,
            'id_prodi' => $request->id_prodi
        ]);
        return redirect('/programstudi/programstudi');
    }

    public function edit($id){
        $ps = KompetensiProdi::where('id_prodi', $id)->first();
        return view('admin.program_studi.kompetensi.edit', compact('ps','id'));
    }

    public function update(Request $request, $id){
        $vs = KompetensiProdi::find($id);
        $vs->kompetensi = $request->kompetensi;
        $vs->save();
        return redirect('/programstudi/programstudi/show/'.$id);
    }
}
