<?php

namespace App\Http\Controllers;
use App\ProgramStudi;
use App\TujuanProdi;

use Illuminate\Http\Request;

class TujuanProdiController extends Controller
{
    public function create($id){
        $ps = ProgramStudi::find($id);
        return view('admin.program_studi.tujuan_prodi.create', compact('ps'));
    }

    public function add(Request $request){
        $id = $request->id_prodi;

        $ps = TujuanProdi::create([
            'tujuan' => $request->tujuan,
            'id_prodi' => $request->id_prodi
        ]);
        return redirect('/programstudi/programstudi');
    }

    public function edit($id){
        $ps = TujuanProdi::where('id_prodi', $id)->first();
        return view('admin.program_studi.tujuan_prodi.edit', compact('ps','id'));
    }

    public function update(Request $request, $id){
        $vs = TujuanProdi::find($id);
        $vs->tujuan = $request->tujuan;
        $vs->save();
        return redirect('/programstudi/programstudi/show/'.$id);
    }
}
