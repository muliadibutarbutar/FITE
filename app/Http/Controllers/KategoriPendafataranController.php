<?php
namespace App\Http\Controllers;
use App\KategoriPendaftaran;

use Illuminate\Http\Request;

class KategoriPendafataranController extends Controller
{
    public function index(){
        $k = KategoriPendaftaran::all();
        return view('admin.kategori_usm.index', compact('k'));
    }

    public function create(){
        return view('admin.kategori_usm.create');
    }

    public function add(Request $request){
        $k = KategoriPendaftaran::create($request->all());
        return redirect('/infopmb/kategoriusm');
    }

    public function edit($id){
        $k = KategoriPendaftaran::find($id);
        return view('admin.kategori_usm.edit', compact('k'));
    }

    public function update(Request $request, $id){
        $k = KategoriPendaftaran::find($id);
        $k->update($request->all());
        return redirect('/infopmb/kategoriusm');
    }

    public function hapus($id){
        \App\KategoriPendaftaran::find($id)->delete();
        return redirect('/infopmb/kategoriusm');
    }
}
