<?php

namespace App\Http\Controllers;
use App\EtikaProdi;
use App\ProgramStudi;

use Illuminate\Http\Request;

class EtikaProdiController extends Controller
{
    public function create($id){
        $ps = ProgramStudi::find($id);
        return view('admin.program_studi.etika.create', compact('ps'));
    }

    public function add(Request $request){
        $id = $request->id_prodi;

        $ps = EtikaProdi::create([
            'etika_prodi' => $request->etika_prodi,
            'id_prodi' => $request->id_prodi
        ]);
        return redirect('/programstudi/programstudi');
    }

    public function edit($id){
        $ps = EtikaProdi::where('id_prodi', $id)->first();
        return view('admin.program_studi.etika.edit', compact('ps','id'));
    }

    public function update(Request $request, $id){
        $vs = EtikaProdi::find($id);
        $vs->etika_prodi = $request->etika_prodi;
        $vs->save();
        return redirect('/programstudi/programstudi/show/'.$id);
    }
}
