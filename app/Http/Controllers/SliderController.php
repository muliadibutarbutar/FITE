<?php

namespace App\Http\Controllers;
use App\Slider;

use Illuminate\Http\Request;

class SliderController extends Controller
{
    public function index(){
        $fs = Slider::all();
        return view('admin.slider.index', compact('fs'));
    }

    public function create(){
        return view('admin.slider.create');
    }

    public function add(Request $request){
        $fs = Slider::create($request->all());
        if($request->hasFile('gambar')){
            $request->file('gambar')->move('images/',$request->file('gambar')->getClientOriginalName());
            $fs->gambar = $request->file('gambar')->getClientOriginalName();
            $fs->save();
        }
        return redirect('/slider');
    }

    public function edit($id){
        $fs = Slider::find($id);
        return view('admin.slider.edit', compact('fs'));
    }

    public function update(Request $request, $id){
        $fs = Slider::find($id);
        $fs->update($request->all());
        if($request->hasFile('gambar')){
            $request->file('gambar')->move('images/',$request->file('gambar')->getClientOriginalName());
            $fs->gambar = $request->file('gambar')->getClientOriginalName();
            $fs->save();
        }
        return redirect('/slider');
    }

    public function hapus($id){
        \App\Slider::find($id)->delete();
        return redirect('/slider');
    }
}
