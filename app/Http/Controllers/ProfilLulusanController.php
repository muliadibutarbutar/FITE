<?php

namespace App\Http\Controllers;
use App\ProgramStudi;
use App\ProfilLulusan;

use Illuminate\Http\Request;

class ProfilLulusanController extends Controller
{
    public function create($id){
        $ps = ProgramStudi::find($id);
        return view('admin.program_studi.profil_lulusan.create', compact('ps'));
    }

    public function add(Request $request){
        $id = $request->id_prodi;

        $ps = ProfilLulusan::create([
            'profil_lulusan' => $request->profil_lulusan,
            'jenis_pekerjaan' => $request->jenis_pekerjaan,
            'deskripsi' => $request->deskripsi,
            'id_prodi' => $request->id_prodi
        ]);
        return redirect('/programstudi/programstudi/show/'.$id);
    }

    public function edit($id){
        $p = ProfilLulusan::find($id);
        return view('admin.program_studi.profil_lulusan.edit', compact('p'));
    }

    public function update(Request $request, $id){
        $p = ProfilLulusan::find($id);
        $p->profil_lulusan = $request->profil_lulusan;
        $p->jenis_pekerjaan = $request->jenis_pekerjaan;
        $p->deskripsi = $request->deskripsi;
        $p->save();

        return redirect('/programstudi/programstudi/show/'.$id);
    }

    public function hapus($id){
        \App\ProfilLulusan::find($id)->delete();
        return redirect('/programstudi/programstudi/show/'.$id);
    }

    public function show($id){
        $p = ProfilLulusan::find($id);
        return view('admin.program_studi.profil_lulusan.show', compact('p'));
    }
}
