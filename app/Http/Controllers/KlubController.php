<?php

namespace App\Http\Controllers;
use App\Klub;

use Illuminate\Http\Request;

class KlubController extends Controller
{
    public function index(){
        $bf = Klub::all();
        return view('admin.klub.index', compact('bf'));
    }

    public function index2(){
        $klub = Klub::all();
        return view('user.klub.index', compact('klub'));
    }

    public function create(){
        return view('admin.klub.create');
    }

    public function add(Request $request){
        $bf = Klub::create($request->all());
        if($request->hasFile('gambar')){
            $request->file('gambar')->move('images/',$request->file('gambar')->getClientOriginalName());
            $bf->gambar = $request->file('gambar')->getClientOriginalName();
            $bf->save();
        }
        return redirect('/kemahasiswaan/klub');
    }

    public function show($id){
        $bf = Klub::find($id);
        return view('admin.klub.show', compact('bf'));
    }

    public function edit($id){
        $bf = Klub::find($id);
        return view('admin.klub.edit', compact('bf'));
    }

    public function update(Request $request, $id){
        $bf = Klub::find($id);
        $bf->update($request->all());
        if($request->hasFile('gambar')){
            $request->file('gambar')->move('images/',$request->file('gambar')->getClientOriginalName());
            $bf->gambar = $request->file('gambar')->getClientOriginalName();
            $bf->save();
        }
        return redirect('/kemahasiswaan/klub');
    }

    public function hapus($id){
        \App\Klub::find($id)->delete();
        return redirect('/kemahasiswaan/klub');
    }
}
