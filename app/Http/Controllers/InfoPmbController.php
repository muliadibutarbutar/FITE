<?php

namespace App\Http\Controllers;
use App\InfoPmb;
use App\KategoriPendaftaran;
use App\LokasiTes;

use Illuminate\Http\Request;

class InfoPmbController extends Controller
{
    public function index(){
        $un = InfoPmb::all();
        return view('admin.info_pmb.index', compact('un'));
    }

    public function index2(){
        $ip = InfoPmb::all();
        $lokasi = LokasiTes::all();
        return view('user.infopmb.index', compact('ip','lokasi'));
    }

    public function create(){
        $ka = KategoriPendaftaran::all();
        return view('admin.info_pmb.create', compact('ka'));
    }

    public function add(Request $request){
        $ka = InfoPmb::create($request->all());
        return redirect('/infopmb/infopmb');
    }

    public function edit($id){
        $ka = KategoriPendaftaran::all();
        $un = InfoPmb::find($id);
        return view('admin.info_pmb.edit', compact('un','ka'));
    }

    public function update(Request $request, $id){
        $un = InfoPmb::find($id);
        $un->update($request->all());
        return redirect('/infopmb/infopmb');
    }

    public function hapus($id){
        \App\InfoPmb::find($id)->delete();
        return redirect('/infopmb/infopmb');
    }
}
