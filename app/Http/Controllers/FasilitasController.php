<?php

namespace App\Http\Controllers;
use App\Fasilitas;
use App\KategoriFasilitas;

use Illuminate\Http\Request;

class FasilitasController extends Controller
{
    public function index(){
        $fs = Fasilitas::all();
        return view('admin.fasilitas.index', compact('fs'));
    }

    public function index2($id){
        $fasilitas = Fasilitas::whereIdKategori($id)->get();
        return view('user.fasilitas.index', compact('fasilitas'));
    }

    public function create(){
        $fs = KategoriFasilitas::all();
        return view('admin.fasilitas.create', compact('fs'));
    }

    public function add(Request $request){
        $fs = Fasilitas::create($request->all());
        if($request->hasFile('gambar')){
            $request->file('gambar')->move('images/',$request->file('gambar')->getClientOriginalName());
            $fs->gambar = $request->file('gambar')->getClientOriginalName();
            $fs->save();
        }
        return redirect('/fasilitas/fasilitas');
    }

    public function edit($id){
        $fs = Fasilitas::find($id);
        $ka = KategoriFasilitas::all();
        return view('admin.fasilitas.edit', compact('fs','ka'));
    }

    public function update(Request $request, $id){
        $fs = Fasilitas::find($id);
        $fs->update($request->all());
        if($request->hasFile('gambar')){
            $request->file('gambar')->move('images/',$request->file('gambar')->getClientOriginalName());
            $fs->gambar = $request->file('gambar')->getClientOriginalName();
            $fs->save();
        }
        return redirect('/fasilitas/fasilitas');
    }

    public function hapus($id){
        \App\Fasilitas::find($id)->delete();
        return redirect('/fasilitas/fasilitas');
    }
}
