<?php

namespace App\Http\Controllers;
use App\Prestasi;

use Illuminate\Http\Request;

class PrestasiController extends Controller
{
    public function index(){
        $ps = Prestasi::all();
        return view('admin.prestasi.index', compact('ps'));
    }

    public function index2(){
        $prestasi = Prestasi::orderBy('id','desc')->get();
        return view('user.prestasi.index', compact('prestasi'));
    }

    public function create(){
        return view('admin.prestasi.create');
    }

    public function add(Request $request){
        $ps = Prestasi::create($request->all());
        return redirect('/prestasi');
    }

    public function edit($id){
        $fs = Prestasi::find($id);
        return view('admin.prestasi.edit', compact('fs'));
    }

    public function update(Request $request, $id){
        $fs = Prestasi::find($id);
        $fs->update($request->all());
        return redirect('/prestasi');
    }

    public function hapus($id){
        \App\Prestasi::find($id)->delete();
        return redirect('/prestasi');
    }
}
