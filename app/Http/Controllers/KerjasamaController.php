<?php

namespace App\Http\Controllers;
use App\Kerjasama;

use Illuminate\Http\Request;

class KerjasamaController extends Controller
{
    public function index(){
        $fs = Kerjasama::all();
        return view('admin.kerjasama.index', compact('fs'));
    }

    public function create(){
        return view('admin.kerjasama.create');
    }

    public function add(Request $request){
        $fs = Kerjasama::create($request->all());
        if($request->hasFile('gambar')){
            $request->file('gambar')->move('images/',$request->file('gambar')->getClientOriginalName());
            $fs->gambar = $request->file('gambar')->getClientOriginalName();
            $fs->save();
        }
        return redirect('/kerjasama');
    }

    public function edit($id){
        $fs = Kerjasama::find($id);
        return view('admin.kerjasama.edit', compact('fs'));
    }

    public function update(Request $request, $id){
        $fs = Kerjasama::find($id);
        $fs->update($request->all());
        if($request->hasFile('gambar')){
            $request->file('gambar')->move('images/',$request->file('gambar')->getClientOriginalName());
            $fs->gambar = $request->file('gambar')->getClientOriginalName();
            $fs->save();
        }
        return redirect('/kerjasama');
    }

    public function hapus($id){
        \App\Kerjasama::find($id)->delete();
        return redirect('/kerjasama');
    }
}
