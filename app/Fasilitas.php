<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Fasilitas extends Model
{
    //

    protected $table = 'fasilitas';
    protected $fillable = ['nama','gambar','deskripsi','id_kategori'];

    public function kategori()
    {
        return $this->belongsTo('App\KategoriFasilitas', 'id_kategori');
    }
}
