<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Alumni extends Model
{
    protected $table = 'alumni';
    protected $fillable =['nama','tahun_lulus','testimoni','id_prodi','link'];

    public function prodi()
    {
        return $this->belongsTo('App\ProgramStudi', 'id_prodi');
    }
}
