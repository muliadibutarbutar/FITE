-- phpMyAdmin SQL Dump
-- version 4.9.7deb1
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Waktu pembuatan: 16 Sep 2021 pada 21.38
-- Versi server: 8.0.26-0ubuntu0.21.04.3
-- Versi PHP: 7.4.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `fite`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `alumni`
--

CREATE TABLE `alumni` (
  `id` int UNSIGNED NOT NULL,
  `nama` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tahun_lulus` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `testimoni` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `link` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_prodi` int UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `alumni`
--

INSERT INTO `alumni` (`id`, `nama`, `tahun_lulus`, `testimoni`, `link`, `id_prodi`, `created_at`, `updated_at`) VALUES
(12, 'Hans Theo C. Simorangkir', '2019', '192753928_768080700557721_7148863362960554740_n.jpg', 'https://www.instagram.com/p/CB3U1eLHCuz-cUwRot1ql2P-srSp2MB0pq0bI80/?igshid=b7rmnpx4c0wt', 1, '2021-09-15 19:48:02', '2021-09-15 19:48:02'),
(13, 'Hans Theo C. Simorangkir', '2019', '192753928_768080700557721_7148863362960554740_n.jpg', 'https://www.instagram.com/p/CB3U1eLHCuz-cUwRot1ql2P-srSp2MB0pq0bI80/?igshid=b7rmnpx4c0wt', 1, '2021-09-15 19:48:28', '2021-09-15 19:48:28'),
(14, 'Hans Theo C. Simorangkir', '2019', '192753928_768080700557721_7148863362960554740_n.jpg', 'https://www.instagram.com/p/CB3U1eLHCuz-cUwRot1ql2P-srSp2MB0pq0bI80/?igshid=b7rmnpx4c0wt', 1, '2021-09-15 19:48:28', '2021-09-15 19:48:29'),
(15, 'Hans Theo C. Simorangkir', '2019', '192753928_768080700557721_7148863362960554740_n.jpg', 'https://www.instagram.com/p/CB3U1eLHCuz-cUwRot1ql2P-srSp2MB0pq0bI80/?igshid=b7rmnpx4c0wt', 1, '2021-09-15 19:48:47', '2021-09-15 19:48:47');

-- --------------------------------------------------------

--
-- Struktur dari tabel `beasiswa`
--

CREATE TABLE `beasiswa` (
  `id` int UNSIGNED NOT NULL,
  `nama` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `logo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deskripsi` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `beasiswa`
--

INSERT INTO `beasiswa` (`id`, `nama`, `logo`, `deskripsi`, `created_at`, `updated_at`) VALUES
(2, 'Beasiswa Prestasi', 'twibbon.png', '<p>Beasiswa Prestasi adalah jenis beasiswa yang diperoleh dari nilai IPK akhir semester per tahun ajaran yang diakumulasikan mendapat voucher Beasiswa SPP berdasarkan ketentuan akademik dari IPK perihal dana beasiswa prestasi. Beasiswa ini berlaku bagi seluruh mahasiswa IT Del setiap semester.</p>', '2021-09-09 21:31:05', '2021-09-13 23:11:41'),
(3, 'Beasiswa Bidikmisi', 'Screenshot from 2021-09-10 00-38-00.png', '<p>Beasiswa Bidikmisi adalah jenis beasiswa yang diberikan oleh LLDikti 1 Medan. Tahap seleksi beasiswa dilakukan dengan calon peserta beasiswa yang telah mendaftarkan diri sebelumnya dari sekolah asal SMA sebelumnya dan kemudian diseleksi oleh pihak kampus untuk seleksi administrasi berkas-berkas untuk disampaikan kepada pihak LLDikti 1 Medan. Adapun jumlah dana beasiswa yang diberikan berupa SPP/biaya kuliah sebesar 2.400.000/semester dan uang saku sebesar Rp.3.900.000/semester (biaya dapat berubah sewaktu-waktu berdasarkan surat edaran ketentuan dari pihak LLDikti 1 Medan setiap tahunnya). Pemberian beasiswa diberikan berdasarkan kuota yang diterima setiap tahunnya oleh pihak kampus untuk selanjutnya diseleksi dan ditetapkan sebagai penerima beasiswa. Beasiswa ini hanya berlaku bagi mahasiswa Diploma III angkatan baru setiap tahunnya.</p>', '2021-09-13 23:11:55', '2021-09-13 23:11:56'),
(4, 'Beasiswa PPA (Peningkatan Prestasi Akademik)', 'Screenshot from 2021-09-10 00-38-00.png', '<p>Beasiswa PPA adalah jenis beasiswa prestasi akademik (berdasarkan IPK tertinggi) yang diberikan kepada mahasiswa terpilih seleksi setiap tahunnya oleh pihak kampus. Adapun jumlah dana beasiswa yang diberikan berupa uang saku sebesar Rp.2.400.000,- /semester (biaya dapat berubah sewaktu-waktu berdasarkan surat edaran ketentuan dari pihak LLDikti 1 Medan setiap tahunnya). Pemberian beasiswa diberikan berdasarkan kuota yang diterima setiap tahunnya oleh pihak kampus untuk selanjutnya diseleksi dan ditetapkan sebagai penerima beasiswa. Beasiswa ini berlaku bagi seluruh mahasiswa khusus tingkat I, II dan III (prodi sarjana, tidak berlaku untuk mahasiswa tingkat akhir diploma dan sarjana).</p>', '2021-09-13 23:12:13', '2021-09-13 23:12:16'),
(5, 'Beasiswa Tanoto', 'Screenshot from 2021-09-10 00-38-00.png', '<p>Beasiswa Tanoto adalah jenis beasiswa yang diberikan oleh pihak Tanoto Foundation yang bergerak pada bidang perusahaan berupa minyak, karet, dan teh. Adapun beasiswa yang diberikan berupa biaya SPP per semester dan uang saku sebesar Rp.3.600.000,-/semester. Seleksi beasiswa setiap tahunnya dilakukan oleh pihak Tanoto secara langsung melalui seleksi berkas, psikotes dan wawancara langsung ataupun skype. Kuota penerima beasiswa setiap tahunnya sebanyak 5 (lima) orang. Beasiswa ini berlaku bagi seluruh mahasiswa angkatan baru setiap tahunnya.</p>', '2021-09-13 23:12:30', '2021-09-13 23:12:31'),
(6, 'Beasiswa Rajawali', 'Screenshot from 2021-09-10 00-36-56.png', '<p>Beasiswa Rajawali adalah jenis beasiswa yang diberikan oleh pihak Rajawali Foundation yang bergerak pada bidang perusahaan. Adapun beasiswa yang diberikan berupa biaya SPP per semester. Seleksi beasiswa setiap tahunnya dilakukan oleh pihak Rajawali secara langsung melalui seleksi berkas, psikotes dan wawancara via skype. Kuota penerima beasiswa setiap tahunnya sebanyak 5 (lima) orang. Beasiswa ini berlaku bagi seluruh mahasiswa angkatan baru setiap tahunnya.</p>', '2021-09-13 23:12:44', '2021-09-13 23:12:46'),
(7, 'Beasiswa Alumni', 'Screenshot from 2021-09-10 00-36-56.png', '<p>Beasiswa Alumni adalah jenis beasiswa yang diberikan oleh ikatan mahasiswa alumni Del. Adapun beasiswa yang diberikan berupa bantuan dana sejumlah mulai dari 1.000.000 &ndash; 2.500.000 disesuaikan setiap tahunnya berdasarkan dana SPP masing-masing mahasiswa penerima beasiswa berdasarkan angkatan tahun masuk perkuliahan. Seleksi dilakukan secara administrasi berkas dan wawancara.</p>', '2021-09-13 23:13:02', '2021-09-13 23:13:03'),
(8, 'Beasiswa BNI', 'Screenshot from 2021-09-10 00-36-56.png', '<p>Beasiswa BNI adalah jenis beasiswa yang diberikan oleh pihak Bank Negara Indonesia (BNI). Adapun beasiswa yang diberikan berupa bantuan dana SPP, asrama, dan uang makan per semester dalam 1 (satu) tahun ajaran. Seleksi dilakukan secara administrasi berkas dan wawancara.</p>', '2021-09-13 23:13:29', '2021-09-13 23:13:30');

-- --------------------------------------------------------

--
-- Struktur dari tabel `berita_fakultas`
--

CREATE TABLE `berita_fakultas` (
  `id` int UNSIGNED NOT NULL,
  `nama` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `gambar` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deskripsi` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_user` bigint UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `berita_fakultas`
--

INSERT INTO `berita_fakultas` (`id`, `nama`, `gambar`, `deskripsi`, `id_user`, `created_at`, `updated_at`) VALUES
(2, 'Lomba Menggambar dan Webinar IT', 'd3tk_1.jpeg', '<p>Sabtu, 26 Juni 2021 &ndash; Program Studi D3 Teknologi Komputer mengadakan program &ldquo;Lomba Menulis Artikel &amp; Menggambar dan Webinar IT&rdquo; yang dilaksanakan secara online melalui platform Zoom. Kegiatan Lomba dan Webinar ini adalah bentuk dari kegiatan promosi prodi D3TK.<br />\r\nPromosi adalah sebuah cara yang digunakan untuk memperkenalkan program studi D3 Teknologi Komputer (D3TK) yang mencakup kurikulum D3TK, kegiatan ekstrakurikuler yang diselenggarakan oleh institusi maupun program studi, serta himpunan mahasiswa program studi sebagai wadah bagi mahasiswa untuk berkarya dan berkontribusi kepada program studi. Tujuan dari kegiatan promosi ini adalah untuk memperkenalkan jenjang Diploma 3 dan khususnya program studi D3TK kepada siswa/siswi SMA dan SMK, sebagai peserta yang berpotensi menjadi calon mahasiswa D3TK.<br />\r\nProgram studi D3TK merencanakan serangkaian kegiatan promosi dalam bentuk webinar, perlombaan, dan juga give away melalui Instagram. Kegiatan-kegiatan ini disusun dan dilaksanakan oleh ketua program studi, dosen program studi, mahasiswa dan alumni. Kegiatan webinar tersebut mengangkat topik mengenai penjelasan mengenai mengapa harus memilih D3, keunggulan D3TK, berbagi pengalaman mahasiswa D3TK, serta pengalaman kerja nyata dari alumni.</p>', 1, '2021-09-02 00:37:46', '2021-09-05 21:32:21'),
(3, 'Webinar Pengenalan D3 Teknologi Komputer', 'd3tk_2.jpeg', '<p>Sabtu, 06 Maret 2021 &ndash; Institut Teknologi Del, D3 Teknologi Komputer mengadakan Webinar Pengenalan D3 Tekbologi Komputer. Pada workshop yang diadakan, mahasiswa dikenalkan mengenai prodi D3 Teknologi Komputer. Berbagi ilmu tentang penggunaan produk IoT sederhana yang dapat dilakukan secara pribadi serta kelompok mahasiswa juga membawa beberapa project Tugas Akhir untuk ditampilkan / ditunjukkan pada siswa/siswi SMA N 1 Lumban Julu agar menarik perhatian siswa untuk mengetahui lebih dalam lagi mengenai pembuatan Project IoT. Pada kegiatan yang dilaksanakan kelompok mahasiswa juga mengajarkan dasar pengetahuan mengenai IoT dengan bantuan dosen dan asisten dosen, melalui kegiatan ini siswa tertarik untuk mengetahui penggunaan produk IoT dalam kehidupan sehari-hari.</p>', 1, '2021-09-02 00:39:13', '2021-09-02 00:39:13'),
(4, 'Seminar & Workshop di SMA Negeri 1 LumbanJulu', 'd3tk_3.jpeg', '<p>Sabtu, 04 Maret 2018 &ndash; Laguboti, Institut Teknologi Del &ndash; Program Studi D3 Teknologi Komputer menggelar program Seminar &amp; Workshop di SMA Negeri 1 LumbanJulu, di mana program ini merupakan program yang rutin diadakan prodi D3 Teknologi Komputer sebagai bentuk pengabdian masyarakat ke Sekolah-sekolah di Sekitar Toba Samosir Seminar dan Workshop ini menghadirkan beberapa mahasiswa, Asisten Dosen dan Dosen program studi D3 Teknologi Komputer.</p>\r\n\r\n<p>Pada workshop yang diadakan, mahasiswa berbagi ilmu tentang penggunaan produk IoT sederhana yang dapat dilakukan secara pribadi serta kelompok mahasiswa juga membawa beberapa project Tugas Akhir untuk ditampilkan / ditunjukkan pada siswa/siswi SMA N 1 Lumban Julu agar menarik perhatian siswa untuk mengetahui lebih dalam lagi mengenai pembuatan Project IoT. Pada kegiatan yang dilaksanakan kelompok mahasiswa juga mengajarkan dasar pengetahuan mengenai IoT dengan bantuan dosen dan asisten dosen, melalui kegiatan ini siswa tertarik untuk mengetahui penggunaan produk IoT dalam kehidupan sehari-hari.</p>', 1, '2021-09-02 00:40:07', '2021-09-02 00:40:09'),
(5, 'Paralel Workshop: Pemrograman VHDL dengan ALTERA DE1 & Pengolahan Sinyal Digital dengan Blackfin 707', 'te.jpg', '<p>Pada tanggal 17-18 September, Program Studi Teknik Elektro menyelenggarakan dua kegiatan workshop secara paralel. Workshop pertama adalah pemgoraman VHDL dengan mempergunakan perangkat ALTERA DE1. Workshop kedua adalah pengolahan sinyal digital dengan mempergunakan perangkat Blackfin 707.</p>\r\n\r\n<p>Workshop diikuti 25 orang peserta, terdiri atas dosen, asisten dosen dan laboran di teknik elektro. Koordinator Asistem Lab Dasar Teknik Elektro Insitut Teknologi Bandung, Bapak Mahenda dan Bapak Hilmi sengajar didatangkan untuk mempercepat &lsquo;transfer knowledge&rsquo; penggunakan perangkat lab yang masih baru di TE IT Del.</p>\r\n\r\n<p>Seluruh peserta workshop sangat bersemangat, kendatipun pelaksanaan dilaksanakan pada hari sabtu-minggu. Workshop paralel ini dilasanakan dari pukul 08.00 sampai 21.00.</p>', 1, '2021-09-02 00:59:26', '2021-09-02 00:59:27');

-- --------------------------------------------------------

--
-- Struktur dari tabel `capaian_lulusan`
--

CREATE TABLE `capaian_lulusan` (
  `id` int UNSIGNED NOT NULL,
  `deskripsi` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_prodi` int UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `data_dosen`
--

CREATE TABLE `data_dosen` (
  `id` int UNSIGNED NOT NULL,
  `nama` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `gambar` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `riwayatpendidikan` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `link` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_prodi` int UNSIGNED NOT NULL,
  `id_kategori` int UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `data_dosen`
--

INSERT INTO `data_dosen` (`id`, `nama`, `gambar`, `riwayatpendidikan`, `link`, `id_prodi`, `id_kategori`, `created_at`, `updated_at`) VALUES
(2, 'Indra Hartarto Tambunan, ST., M.S.,Ph.D', 'indra.png', '<p>S1 Teknik Elektro Institut Teknologi Bandung</p>\r\n\r\n<p>S2 Konhuk Univ.Seoul Aerospce Informatic Engneering</p>\r\n\r\n<p>S3 Konhuk Univ.Seoul Aerospce Informatic Engneering</p>', 'https://scholar.google.co.id/citations?hl=en&user=trhaaiYAAAAJ', 4, 3, '2021-09-12 10:02:24', '2021-09-15 19:55:42'),
(3, 'Ike Fitriyaningsih, S.Si., M.Si', 'ike.png', '<p>S1 Statistika Universitas Brawijaya 2012</p>\r\n\r\n<p>S2 Statistika Institut Teknologi Sepuluh Nopember 2015</p>', 'https://scholar.google.co.id/citations?user=wEGsLv4AAAAJ&hl=en', 1, 3, '2021-09-12 10:13:14', '2021-09-15 19:55:17'),
(4, 'Teamsar Muliadi Panggabean, S.Kom., PGCert', 'default1.jpg', '<p>S1 Universitas Bina Nusantara</p>\r\n\r\n<p>S2 The University of Edinburgh</p>', 'https://scholar.google.co.id/citations?hl=en&user=vA_yo3sAAAAJ', 1, 2, '2021-09-15 19:57:10', '2021-09-15 19:59:58'),
(5, 'Hernawati S. Samosir, SST., M.Kom.', 'default1.jpg', '<p>DIV Institut Teknologi Bandung</p>\r\n\r\n<p>S2 Institut Teknologi Sepuluh November</p>', 'https://google.com', 1, 2, '2021-09-15 20:02:01', '2021-09-15 20:02:01'),
(6, 'Togu Novriansyah Turnip, S.S.T., M.IM', 'togu.png', '<p>S1 Politeknik Bandung</p>\r\n\r\n<p>S2 National Taiwan University of Science and Technology</p>', 'https://scholar.google.co.id/citations?hl=en&user=9KWoMukAAAAJ', 1, 2, '2021-09-15 20:03:45', '2021-09-15 20:03:45'),
(7, 'Monalisa Pasaribu, SS., M.Ed', 'default1.jpg', '<p>S1 Universitas Sumatera Utara</p>\r\n\r\n<p>S2 University of Wllongong</p>', 'https://scholar.google.co.id/citations?hl=en&user=rlLcWiUAAAAJ', 1, 2, '2021-09-15 20:07:44', '2021-09-15 20:08:17'),
(8, 'Goklas Henry Agus Panjaitan, S.Tr.Kom', 'default1.jpg', '<p>DIV Institut Teknologi Del</p>\r\n\r\n<p>S2 Institut Teknologi Bandung</p>', 'https://google.com', 1, 2, '2021-09-15 20:09:13', '2021-09-15 20:09:13'),
(9, 'Tegar Arifin Prasetyo, S.Si., M.Si.', 'default1.jpg', '<p>-&nbsp;</p>\r\n\r\n<p>S2 Institut Teknologi Bandung</p>', 'https://scholar.google.co.id/citations?hl=en&user=ZLbuKXYAAAAJ', 1, 2, '2021-09-15 20:10:09', '2021-09-15 20:10:09'),
(10, 'Tiurma Lumban Gaol, SP, M.P', 'default1.jpg', '<p>S1 Universitas Sumatera Utara</p>\r\n\r\n<p>S2 Institut Pertanian Bogor</p>', 'https://scholar.google.co.id/citations?hl=en&user=yN0op8sAAAAJ', 1, 2, '2021-09-15 20:11:02', '2021-09-15 20:11:02'),
(11, 'Eka Stephani Sinambela, SST., M.Sc', 'default1.jpg', '<p>S1 Institut Teknologi Bandung</p>\r\n\r\n<p>S2 Vrije Universiteit Amsterdam</p>', 'https://google.com', 2, 3, '2021-09-15 20:12:15', '2021-09-15 20:12:15'),
(12, 'Pandapotan Siagian, ST, M.Eng', 'default1.jpg', '<p>S1 Universitas HKBP Nomensen</p>\r\n\r\n<p>S2 Universitas Gadjah Mada</p>', 'https://scholar.google.co.id/citations?hl=en&user=xW51aeMAAAAJ', 2, 2, '2021-09-15 20:13:44', '2021-09-15 20:13:44'),
(13, 'Ahmad Z. Purwalaksana, S.Si.,M.Si', 'default1.jpg', '<p>S1 Universitas Pendidikan Bandung</p>\r\n\r\n<p>S2 Institut Teknologi Bandung</p>', 'https://google.com', 2, 2, '2021-09-15 20:17:14', '2021-09-15 20:17:26'),
(14, 'Marojahan Mula Timbul Sigiro, ST, M.Sc', 'default1.jpg', '<p>S1 Institut Teknologi Bandung</p>\r\n\r\n<p>S2 Computer Science Delft Universiy of Technology</p>', 'https://google.com', 2, 2, '2021-09-15 20:18:29', '2021-09-15 20:18:29'),
(15, 'Sari Muthia Silalahi, S.Pd., M.Ed', 'default1.jpg', '<p>S1 Universitas Negeri Medan</p>\r\n\r\n<p>S2 National Taiwan University of Science and Technology</p>', 'https://scholar.google.co.id/citations?hl=en&user=kdYKKfwAAAAJ', 2, 2, '2021-09-15 20:21:41', '2021-09-15 20:21:42'),
(16, 'Gerry Italiano Wowiling, S.Tr.Kom', 'default1.jpg', '<p>DIV Politeknik Elektronika Negerti Surabaya</p>\r\n\r\n<p>S2 Institut Teknologi Bandung</p>', 'https://google.com', 2, 2, '2021-09-15 20:45:39', '2021-09-15 20:45:39'),
(17, 'Istas Manalu, S.Si., M.Sc', 'default1.jpg', '<p>S1 Universitas Sumatera Utara</p>\r\n\r\n<p>S2 Chang Gung University</p>', 'https://scholar.google.co.id/citations?hl=en&user=spzw8MUAAAAJ', 2, 2, '2021-09-15 20:49:06', '2021-09-15 20:49:06'),
(18, 'Dr. Arnaldo Marulitua Sinaga, S.T, M.InfoTech', 'default1.jpg', '<p>S1 Institut Teknologi Bandung</p>\r\n\r\n<p>S2 University of Wollongong</p>\r\n\r\n<p>S3 University of Wollongong</p>', 'https://scholar.google.co.id/citations?hl=en&user=6W1KYjgAAAAJ', 5, 2, '2021-09-15 20:53:45', '2021-09-15 20:53:45'),
(19, 'Mukhammad Solikhin, S.Si, M.Si', 'default1.jpg', '<p>S1 Universitas Negeri Surabaya</p>\r\n\r\n<p>S2 Institut Teknologi Bandung</p>', 'https://scholar.google.co.id/citations?hl=en&user=TF3NQt0AAAAJ', 5, 2, '2021-09-15 20:55:55', '2021-09-15 20:55:55'),
(20, 'Roy Deddy Hasiholan Lumban Tobing, S.T., M.ICT', 'default1.jpg', '<p>S1 Institut Teknologi Harapan Bangsa</p>\r\n\r\n<p>S2 University of Wollongong</p>', 'https://scholar.google.co.id/citations?hl=en&user=gn-NWR0AAAAJ', 5, 2, '2021-09-15 20:57:54', '2021-09-15 20:57:54'),
(21, 'Riyanthi Angrainy Sianturi, S.Sos, M.Ds', 'default1.jpg', '<p>S1 Universitas Sumatera Utara</p>\r\n\r\n<p>S2 Institut Teknologi Bandung</p>', 'https://scholar.google.co.id/citations?hl=en&user=uT7Ey3wAAAAJ', 5, 2, '2021-09-15 20:58:57', '2021-09-15 20:58:57'),
(22, 'Rumondang Miranda Marsaulina, S.P, M.Si', 'default1.jpg', '<p>S1 Institut Pertanian Bogor</p>\r\n\r\n<p>S2 Universitas Indonesia</p>', 'https://scholar.google.co.id/citations?hl=en&user=fkH5_0sAAAAJ', 5, 2, '2021-09-15 21:00:24', '2021-09-15 21:00:25'),
(23, 'Verawaty Situmorang, S.Kom., M.T.I', 'default1.jpg', '<p>S1 Universitas Bina Nusantara</p>\r\n\r\n<p>S2 Universitas Indonesia</p>', 'https://google.com', 5, 3, '2021-09-15 21:01:57', '2021-09-15 21:01:57'),
(24, 'Yohanssen Pratama, S.Si, M.T', 'default1.jpg', '<p>S1 Institut Teknologi Bandung</p>\r\n\r\n<p>S2 Institut Teknologi Bandung</p>', 'https://scholar.google.co.id/citations?hl=en&user=OrL-peMAAAAJ', 5, 2, '2021-09-15 21:03:27', '2021-09-15 21:03:29'),
(25, 'Regina Ayunita Tarigan, S.Si., M.Sc', 'default1.jpg', '<p>S1 Universitas Negeri Medan</p>\r\n\r\n<p>S2 National Central University</p>', 'https://scholar.google.co.id/citations?hl=en&user=4hvrKM8AAAAJ', 5, 2, '2021-09-15 21:05:00', '2021-09-15 21:05:00'),
(26, 'Rini Juliana Sipahutar, S.Tr. Kom', 'default1.jpg', '<p>S1 DIV Institut Teknologi Del</p>\r\n\r\n<p>S2 Universitas Indonesia</p>', 'https://google.com', 5, 2, '2021-09-15 21:06:20', '2021-09-15 21:06:20'),
(27, 'Dr. Arlinta Christy Barus, S.T., M.InfoTech', 'default1.jpg', '<p>S1 Institut Teknologi Bandung&nbsp;</p>\r\n\r\n<p>S2 University of Wollongong</p>\r\n\r\n<p>S3 Swinburne University of Technology</p>', 'https://scholar.google.co.id/citations?hl=en&user=DspQ-LEAAAAJ', 6, 2, '2021-09-15 21:09:24', '2021-09-15 21:09:24'),
(28, 'Arie Satia Dharma, S.T, M.Kom', 'default1.jpg', '<p>S1 Universitas Kristen Maranatha</p>\r\n\r\n<p>S2 Universitas Sumatera Utara</p>', 'https://scholar.google.co.id/citations?hl=en&user=KpvGjxMAAAAJ', 6, 3, '2021-09-15 21:11:00', '2021-09-15 21:11:00'),
(29, 'Anthon Roberto Tampubolon, S.Kom, M.T', 'default1.jpg', '<p>S1 STIKOM UYELINDO Kupang</p>\r\n\r\n<p>S2 Institut Teknologi Bandung</p>', 'https://scholar.google.co.id/citations?hl=en&user=agA_xDoAAAAJ', 6, 2, '2021-09-15 21:12:30', '2021-09-15 21:12:30'),
(30, 'Inte Christinawati Bu\'ulolo, ST', 'default1.jpg', '<p>S1 Institut Teknologi Bandung</p>\r\n\r\n<p>S2 Universitas Indonesia</p>', 'https://scholar.google.co.id/citations?hl=en&user=aa_W8r4AAAAJ', 6, 2, '2021-09-15 21:13:45', '2021-09-15 21:13:45'),
(31, 'Lit Malem Ginting, S.Si, MT', 'default1.jpg', '<p>S1 Institut Teknologi Bandung</p>\r\n\r\n<p>S2 Institut Teknologi Bandung</p>', 'https://scholar.google.co.id/citations?hl=en&user=as9YoTYAAAAJ', 6, 2, '2021-09-15 21:14:58', '2021-09-15 21:14:58'),
(32, 'Iustisia N. Simbolon, S.Kom., M.T.', 'default1.jpg', '<p>S2 Institut Teknologi Bandung</p>', 'https://google.com', 6, 2, '2021-09-15 21:15:57', '2021-09-15 21:15:57'),
(33, 'Dr. Johannes Harungguan Sianipar, S.T., M.T.', 'default1.jpg', '<p>S1 STT Telkom</p>\r\n\r\n<p>S2 Institut Teknologi Bandung</p>\r\n\r\n<p>S3 Hasso Plattner Institut</p>', 'https://scholar.google.co.id/citations?hl=en&user=BRV3FGgAAAAJ', 6, 2, '2021-09-15 21:19:43', '2021-09-15 21:19:43'),
(34, 'Tahan H.J. Sihombing, S.Pd., M. App Ling', 'default1.jpg', '<p>S2 The University of Queensland</p>', 'https://scholar.google.co.id/citations?hl=en&user=U7KNoBQAAAAJ', 6, 2, '2021-09-15 21:21:32', '2021-09-15 21:21:33'),
(35, 'Nenni Mona Aruan, S.Pd., M.Si', 'default1.jpg', '<p>S1 Universitas Pendidikan Bandung</p>\r\n\r\n<p>S2 Institut Teknologi Bandung</p>', 'https://scholar.google.co.id/citations?hl=en&user=KeV36bAAAAAJ', 6, 2, '2021-09-15 21:23:19', '2021-09-15 21:23:19'),
(36, 'Yaya Setiyadi, S.Si, M.T', 'default1.jpg', '<p>S1 Institut Teknologi Bandung</p>\r\n\r\n<p>S2 Institut Teknologi Bandung</p>', 'https://google.com', 6, 2, '2021-09-15 21:25:44', '2021-09-15 21:25:44'),
(37, 'Mario Elyezer Subekti Simaremare, S.Kom., M.Sc.', 'default1.jpg', '<p>S1 Universitas Indonesia</p>\r\n\r\n<p>S2 vrije Universiteit Amsterdam</p>', 'https://scholar.google.co.id/citations?hl=en&user=or8TzTIAAAAJ', 3, 3, '2021-09-15 21:27:25', '2021-09-15 21:27:26'),
(38, 'Parmonangan Rotua Togatorop, S.Kom., M.T.I', 'default1.jpg', '<p>S1 Universitas Indonesia</p>\r\n\r\n<p>S2 Universitas Indonesia</p>', 'https://scholar.google.co.id/citations?hl=en&user=1tKo5jMAAAAJ', 3, 2, '2021-09-15 21:28:40', '2021-09-15 21:28:40'),
(39, 'Samuel Indra Gunawan Situmeang, S.TI., M.Sc.', 'default1.jpg', '<p>S1 Universitas Sumatera Utara</p>\r\n\r\n<p>S2 National Chung Cheng University</p>', 'https://scholar.google.co.id/citations?hl=en&user=vaIsPqcAAAAJ', 3, 2, '2021-09-15 21:30:23', '2021-09-15 21:30:23'),
(40, 'Tennov Simanjuntak, S.T, M.Sc', 'default1.jpg', '<p>S1 Institut Teknologi Bandung</p>\r\n\r\n<p>S2 Delft University of Technology</p>', 'https://google.com', 3, 2, '2021-09-15 21:35:47', '2021-09-15 21:35:47'),
(41, 'Humasak Tomy Argo Simanjuntak, ST, M.ISD', 'default1.jpg', '<p>S1 Institut Teknologi Bandung</p>\r\n\r\n<p>S2 Hogeschool van Arnehm and Nijmegen University</p>', 'https://scholar.google.co.id/citations?hl=en&user=48DqP4AAAAAJ', 3, 2, '2021-09-15 21:37:45', '2021-09-15 21:37:45'),
(42, 'Rosni Lumbantoruan, ST, M.ISD', 'default1.jpg', '<p>S1 Institut Teknologi Bandung</p>\r\n\r\n<p>S2 Hogeschool van Arnehm and Nijmegen University</p>', 'https://scholar.google.co.id/citations?hl=en&user=9Hwod2wAAAAJ', 3, 2, '2021-09-15 21:39:27', '2021-09-15 21:39:27'),
(43, 'Junita Amalia, S.Pd., M.Si', 'default1.jpg', '<p>S1 Universitas Negeri Padang</p>\r\n\r\n<p>S2 Institut Teknologi Sepuluh Nopember</p>', 'https://scholar.google.co.id/citations?hl=en&user=yLZwixoAAAAJ', 3, 2, '2021-09-15 21:40:59', '2021-09-15 21:40:59'),
(44, 'Albert Sagala, S.T, M.T', 'default1.jpg', '<p>S1 Institut Teknologi Bandung</p>\r\n\r\n<p>S2 Institut Teknologi Bandung</p>', 'https://scholar.google.co.id/citations?hl=en&user=L5VCO_MAAAAJ', 4, 2, '2021-09-15 21:42:56', '2021-09-15 21:42:56'),
(45, 'I Gde Eka Dirgayussa, S.Pd,M.Si', 'default1.jpg', '<p>S1 Universitas Pendidikan Bandung</p>\r\n\r\n<p>S2 Institut Teknologi Bandung</p>', 'https://scholar.google.co.id/citations?hl=en&user=fMcb8lsAAAAJ', 4, 3, '2021-09-15 21:44:10', '2021-09-15 21:44:10'),
(46, 'Good Fried Panggabean, ST, MT, Ph.D', 'default1.jpg', '<p>S1 Institut Teknologi Bandung</p>\r\n\r\n<p>S2 Institut Teknologi Bandung</p>\r\n\r\n<p>S3 Chiba University</p>', 'https://scholar.google.co.id/citations?hl=en&user=FhktAi8AAAAJ', 4, 2, '2021-09-15 21:46:06', '2021-09-15 21:46:06'),
(47, 'Santi Febri Arianti, S.Si, M.Sc', 'default1.jpg', '<p>S1 Universitas Pendidikan Bandung</p>\r\n\r\n<p>S2 Universitas Gajah Mada</p>', 'https://scholar.google.co.id/citations?hl=en&user=GWKjF4gAAAAJ', 4, 2, '2021-09-15 21:48:02', '2021-09-15 21:48:02'),
(48, 'Guntur Purba Siboro, S.T.,M.T', 'default1.jpg', '<p>S1 Institut Teknologi Bandung</p>\r\n\r\n<p>S2 Institut Teknologi Bandung</p>', 'https://google.com', 4, 2, '2021-09-15 21:49:16', '2021-09-15 21:49:16'),
(49, 'Deni Parlindungan Lumbantoruan , ST, M.Eng', 'default1.jpg', '<p>S1 Institut Teknologi Bandung</p>\r\n\r\n<p>S2 Chiba University</p>\r\n\r\n<p>S3 King&#39;s Collage London</p>', 'https://scholar.google.co.id/citations?hl=en&user=u4yOpSoAAAAJ', 4, 2, '2021-09-15 21:50:55', '2021-09-15 21:50:55'),
(50, 'Alfriska Oktarina Silalahi, S.Pd., M.Si.', 'default1.jpg', '<p>S1 Universitas Negeri Medan</p>\r\n\r\n<p>S2 Institut Teknologi Bandung</p>', 'https://scholar.google.co.id/citations?hl=en&user=pus6AoIAAAAJ', 4, 2, '2021-09-15 21:52:01', '2021-09-15 21:52:01');

-- --------------------------------------------------------

--
-- Struktur dari tabel `dekan`
--

CREATE TABLE `dekan` (
  `id` int UNSIGNED NOT NULL,
  `nama` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `foto` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deskripsi` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `dekan`
--

INSERT INTO `dekan` (`id`, `nama`, `foto`, `deskripsi`, `created_at`, `updated_at`) VALUES
(2, 'Indra Hartarto Tambunan, ST., M.S.,Ph.D', 'indra.png', '<p>Fakultas Informatika dan Teknik Elektro adalah salah satu fakultas yang diselenggarakan Institut Teknologi Del yang terletak di daerah pedesaan Tobasa, yang membuka peluang bagi siswa berprestasi untuk mengenyam pendidikan dengan mutu yang tidak kalah dengan pendidikan bermutu bagus yang diselenggarakan di kota besar.</p>\r\n\r\n<p>S1 Teknik Elektro Institut Teknologi Bandung</p>\r\n\r\n<p>S2 Konhuk Univ.Seoul Aerospce Informatic Engneering</p>\r\n\r\n<p>S3 Konhuk Univ.Seoul Aerospce Informatic Engneering</p>', '2021-09-09 06:07:36', '2021-09-09 06:11:56');

-- --------------------------------------------------------

--
-- Struktur dari tabel `dosen_prodi`
--

CREATE TABLE `dosen_prodi` (
  `id` int UNSIGNED NOT NULL,
  `nama` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deskripsi` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `jabatan` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_prodi` int UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `etika_prodi`
--

CREATE TABLE `etika_prodi` (
  `id` int UNSIGNED NOT NULL,
  `etika_prodi` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_prodi` int UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `etika_prodi`
--

INSERT INTO `etika_prodi` (`id`, `etika_prodi`, `id_prodi`, `created_at`, `updated_at`) VALUES
(1, '<ol>\r\n	<li>Sikap dan perilaku bertakwa</li>\r\n	<li>Bekerja sama, disiplin, bertanggung jawab, dan berkarakter Del</li>\r\n	<li>Matematika terapan, algoritma, konsep dasar rekayasa perangkat lunak</li>\r\n	<li>Komunikasi interpersonal dan perkembangan teknologi</li>\r\n	<li>Mengumpulkan, mengelola dan menampilkan informasi</li>\r\n	<li>Menganalisis data dan menyusun laporan</li>\r\n	<li>Software Requirements Spesification dan Testing</li>\r\n</ol>', 1, '2021-09-01 00:12:01', '2021-09-13 06:17:22'),
(2, '<ol>\r\n	<li>Sikap religious, berperikemanusiaan, Mar-Tuhan, Mar-Roha, Mar-Bisuk (nilai Del)</li>\r\n	<li>Sains rekayasa</li>\r\n	<li>Bekerja sama dan bertanggung jawab.</li>\r\n	<li>Jaringan Komputer.</li>\r\n	<li>Infrakstruktur Komputasi Awan.</li>\r\n	<li>Internet of Things (IoT).</li>\r\n</ol>', 2, '2021-09-13 07:08:04', '2021-09-13 07:14:27'),
(3, '<ol>\r\n	<li>Sikap dan perilaku bertaqwa</li>\r\n	<li>Visualisasi Data</li>\r\n	<li>Programming</li>\r\n	<li>Ekonomi</li>\r\n	<li>Website</li>\r\n	<li>Analisis</li>\r\n	<li>Security</li>\r\n	<li>science</li>\r\n</ol>', 3, '2021-09-13 07:17:47', '2021-09-13 07:17:47'),
(4, '<ol>\r\n	<li>Mampu menunjukkan sikap dan prilaku bertakwa kepada Tuhan yang Maha Esa dan berkontribusi dalam peningkatan mutu kehidupan bermasyarakat, berbangsa, bernegara berdasarkan Pancasila.</li>\r\n	<li>Sikap dan prilaku bertakwa</li>\r\n	<li>Bekerja sama, disiplin, bertanggung jawab, dan berkarakter Del</li>\r\n	<li>Menguasai konsep matematika, sains dan rekayasa</li>\r\n	<li>Menguasai ilmu pengetahuan dan teknologi khususnya dibidang Teknik Elektro</li>\r\n	<li>Merancang, mengimplementasikan, menguji, mengoperasikan, dan pemeliharaa sistem elektrik serta jaringan komputer</li>\r\n</ol>', 4, '2021-09-13 07:27:06', '2021-09-13 07:27:06'),
(5, '<ol>\r\n	<li>Sikap dan prilaku bertakwa</li>\r\n	<li>Bekerja sama, disiplin, bertanggung jawab, dan berkarakter Del</li>\r\n	<li>Matematika dasar dan pengembangan perangkat lunak</li>\r\n	<li>Kajian teknis</li>\r\n	<li>Software developer dan quality assurance</li>\r\n	<li>Mengambil keputusan</li>\r\n	<li>Software Requirements Specification</li>\r\n</ol>', 5, '2021-09-16 07:01:06', '2021-09-16 07:01:06'),
(6, '<ol>\r\n	<li>Sikap dan perilaku bertakwa</li>\r\n	<li>Bekerja sama, disiplin, bertanggung jawab, dan berkarakter Del</li>\r\n	<li>Pemikiran logis, kritis, sistematis, dan inovatif</li>\r\n	<li>Matematika Diskrit, Probalistik dan Statistika</li>\r\n	<li>Algoritma dan Pemrograman</li>\r\n	<li>Rekayasa perangkat lunak</li>\r\n	<li>Keamanan</li>\r\n	<li>Kecerdasan buatan</li>\r\n</ol>', 6, '2021-09-16 07:07:10', '2021-09-16 07:07:10');

-- --------------------------------------------------------

--
-- Struktur dari tabel `fasilitas`
--

CREATE TABLE `fasilitas` (
  `id` int UNSIGNED NOT NULL,
  `nama` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `gambar` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deskripsi` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_kategori` int UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `fasilitas`
--

INSERT INTO `fasilitas` (`id`, `nama`, `gambar`, `deskripsi`, `id_kategori`, `created_at`, `updated_at`) VALUES
(2, 'Laboratorium Dasar Teknik Elektro', 'Screenshot from 2021-09-10 00-38-00.png', '<p>Guntur Purba Siboro, S.T.,M.T</p>\r\n\r\n<p>Deskripsi : Laboratorium yang membantu mahasiswa mempelajari dasar-dasar atau prinsip-prinsip teknik elektro. Laboratorium ini sudah dilengkapi dengan perangkat pengukuran dan pemantauan terbaru. Ini akan membantu mahasiswa saat melakukan beberapa percobaan prinsip instrumentasi menggunakan berbagai jenis sensor, fenomena dasar rangkaian RLC, dan respon rangkaian menggunakan osiloskop GW Instek</p>', 2, '2021-09-09 12:37:07', '2021-09-13 22:57:02'),
(4, 'Lab Sistem Kendali', 'Screenshot from 2021-09-10 00-36-56.png', '<p>Laboratorium</p>', 2, '2021-09-09 12:42:22', '2021-09-09 12:42:25'),
(5, 'Laboratorium Sistem Digital', 'Screenshot from 2021-09-10 00-38-00.png', '<p>Good Fried Panggabean, ST., MT, Ph.D</p>\r\n\r\n<p>Deskripsi : Laboratorium yang dikhususkan untuk enyelenggara kegiatan Praktikum mahasiswa terkait matakuliah prasyarat dan kegiatan Penelitian seputar Sistem Digital. Saat ini, bidang keilmuan yang menjadi cakupan lab Digital adalah Teknik Digital, Rangkaian Logika, Sistem Mikroprosesor, Internet of Things.</p>', 2, '2021-09-13 22:57:51', '2021-09-13 22:57:53'),
(6, 'Laboratorium Sistem Kendali', 'Screenshot from 2021-09-10 00-38-00.png', '<p>Albert Sagala, S.T, M.T</p>\r\n\r\n<p>Deskripsi : Laboratorium penunjang untuk kegiatan mahasiswa dalam melaksanakan praktikum dan pengerjaan skripsi. Laboratorium Sistem Kendali memiliki fokus bidang pada sistem kendali, otomatis peralatan elektronik atau industri dan robotika .</p>', 2, '2021-09-13 22:58:11', '2021-09-13 22:58:13'),
(7, 'Laboratorium Jaringan Komputer', 'Screenshot from 2021-09-10 00-38-00.png', '<p>Pandapotan Siagian, ST, M.Eng</p>\r\n\r\n<p>Deskripsi : Laboratorium ini berisikan perangkat-perangkat praktikum yang berkaitan dengan jaringan komputer dan pemograman. Mulai dari praktikum dasar yang wajib diikuti oleh semua mahasiswa semester awal seperti algoritma dan pemograman hingga ke praktikum tingkat lanjut yang dikhususkan bagi mahasiswa yang mengambil konsentrasi bidang komputer dan informatika.</p>', 2, '2021-09-13 22:58:42', '2021-09-13 22:58:45'),
(8, 'Laboratorium Infrastruktur Komputasi Awan dan Cisco Academy', 'Screenshot from 2021-09-10 00-38-00.png', '<p>Marojahan MT. Sigiro, ST., M.Sc</p>\r\n\r\n<p>Deskripsi : Laboratorium yang digunakan untuk embelajaran dan penelitian cloud computing bagi mahasiswa Teknik Elektro</p>', 2, '2021-09-13 22:59:05', '2021-09-13 22:59:05'),
(9, 'Laboratorium Arsitektur  Komputer dan Sistem Tertanam', 'Screenshot from 2021-09-10 00-38-00.png', '<p>Istas Manalu, S.Si., M.Sc</p>\r\n\r\n<p>Deskripsi : Laboratorium yang memberi sarana kepada para mahasiswa dalam mempelajari dasar sistem elektronika dan hukum-hukum rangkaian listrik dan aplikasinya pada sistem elektronika umum khususnya pada</p>', 2, '2021-09-13 22:59:25', '2021-09-13 22:59:25'),
(10, 'Laboratorium Komputasi Berkinerja Tinggi (HPC)', 'Screenshot from 2021-09-10 00-36-56.png', '<p>Samuel Indra Gunawan Situmeang, S.Ti., M.Sc.</p>\r\n\r\n<p>Deskripsi : Laboratorium yang memiliki kemampuan analitik di bidang arsitektur komputer dan performanya serta kemampuan merancang sistem komputer intelijen seperti robot</p>', 2, '2021-09-13 22:59:46', '2021-09-13 22:59:48'),
(11, 'Laboratorium Komputer', 'Screenshot from 2021-09-10 00-36-56.png', '<p>Deskripsi : tempat riset ilmiah, eksperimen, pengukuran ataupun pelatihan ilmiah yang berhubungan dengan ilmu komputer dan memiliki beberapa komputer dalam satu jaringan</p>', 2, '2021-09-13 23:00:06', '2021-09-13 23:00:06'),
(12, 'Laboratorium Bahasa', 'Screenshot from 2021-09-10 00-36-56.png', '<p>Deskripsi : Laboratorium yang digunakan dalam proses pembelajaran Bahasa Inggris</p>', 2, '2021-09-13 23:00:25', '2021-09-13 23:00:26'),
(13, 'Perpustakaan', 'Screenshot from 2021-09-10 00-36-56.png', '<p>Perpustakaan IT Del memberikan sistem pelayanan open acces (terbuka) kepada pemustaka. Setiap sivitas yang berkunjung dapat mengakses koleksi secara langsung sehingga lebih leluasa memilih koleksi yang ingin dibaca atau dipinjam. Koleksi bahan pustaka yang mutakhir, pemanfatan teknologi informasi, dan tersedianya ruangan yang nyaman yang mendukung proses belajar dengan baik merupakan daya tarik bagi sivitas akademika IT Del untuk berkunjung ke Perpustakaan IT Del.</p>\r\n\r\n<p>Perpustakaan Institut Teknologi Del memiliki fungsi utama yaitu fungsi pendidikan. Penyediaan koleksi diarahkan pada terpenuhinya kebutuhan bahan pustaka yang mendukung kegiatan belajar mengajar. Namun demikian, pengadaan koleksi yang sifatnya rekreasi seperti Novel dan buku-buku yang sifatnya untuk pengembangan diri tetap diupayakan tersedia dan diperbarui. Pengadaan koleksi dan pengembangan minat baca internal dan eksternal Institut Teknologi Del dilakukan sebagai bagian dari kegiatan diseminasi kegiatan peningkatan minat baca</p>\r\n\r\n<p>Perpustakaan IT Del memiliki beragam prestasi. Prestasi yang dicapai tidak hanya pada tingkat lokal tetapi juga tingkat nasional dan internasional. Capaian itu tentunya tidak lepas dari dukungan Yayasan Del, pimpinan di IT Del, dan juga pustakawan-pustakawan IT Del. Pencapaian prestasi ini tentunya tidak membuat puas tetapi menjadi pemacu untuk meningkatkan kualitas layanan di Perpustakaan IT Del.</p>', 4, '2021-09-13 23:01:23', '2021-09-13 23:01:24'),
(14, 'Kantin', 'Screenshot from 2021-09-10 00-38-00.png', '<p>Kantin Institut Teknologi Del menyediakan menu makanan yang bergizi dan sehat untuk semua mahasiswa/i. Menu yang disediakan selalu memenuhi standard gizi, sehingga mahasiswa bisa melaksanakan aktifitas perkuliahan dengan sehat dan prima. Setiap mahasiswa diwajibkan untuk makan di kantin tiga kali setiap harinya.<br />\r\nUntuk Sabtu sore dan hari Minggu, layanan kantin untuk mahasiswa ditiadakan, dimana untuk waktu tersebut, mahasiswa diperbolehkan keluar kampus (termasuk ijin bermalam) untuk makan.</p>\r\n\r\n<p>Dalam pelaksanaan makan, mahasiswa dilatih juga untuk memiliki manner. Etika makan yang berlaku adalah sebagai berikut:</p>\r\n\r\n<p>a. Mahasiswa berpakaian rapi dan bersih.</p>\r\n\r\n<p>b. Mengikuti tata cara makan yang baik (table manner):</p>\r\n\r\n<p>c. Menggunakan sendok dan garpu.</p>\r\n\r\n<p>d. Mulut tertutup rapat pada saat mengunyah makanan.</p>\r\n\r\n<p>e. Tidak menimbulkan bunyi dari peralatan makan.</p>\r\n\r\n<p>f. Berbicara sewajarnya ( tidak terlalu keras sehingga tidak mengganggu orang lain).</p>\r\n\r\n<p>g. Mengambil makanan secukupnya dan menghabiskannya tanpa sisa.</p>\r\n\r\n<p>Selain kantin, makanan/jajanan ringan bisa diperoleh di IT Del Shop. Dengan harga yang terjangkau, Pidelshop menyediakan berbagai kebutuhan mahasiswa seperti:</p>\r\n\r\n<p>&ndash; Kebutuhan alat tulis</p>\r\n\r\n<p>&ndash; Kebutuhan mandi dan cuci</p>\r\n\r\n<p>&ndash; Asesoris Pidel</p>\r\n\r\n<p>&ndash; Makanan ringan</p>\r\n\r\n<p><br />\r\n&nbsp;</p>', 6, '2021-09-13 23:02:03', '2021-09-13 23:02:03'),
(15, 'Asrama dan Rusunawa', 'Screenshot from 2021-09-10 00-36-56.png', '<p>IT Del merupakan perguruan tinggi teknologi yang menyelenggarakan proses pendidikan bagi para mahasiswanya serentak dengan sistem hidup berasrama.</p>\r\n\r\n<p>Asrama IT Del memiliki dua fungsi utama, yaitu:</p>\r\n\r\n<p>1) Asrama menyediakan tempat tinggal yang nyaman dan aman bagi para mahasiswa. Sebagai tempat tinggal, IT Del memiliki 8 gedung asrama dan bahkan sedang mempersiapkan 1 lagi gedung asrama baru. Semuanya mampu memfasilitasi sekitar +1600 orang mahasiswa. Di setiap gedung asrama dilengkapi dengan berbagai fasilitas yang mendukung aktivitas harian mahasiswa.</p>\r\n\r\n<p>2) Asrama sebagai sarana pembinaan karakter. Dalam rangka pengembangan karakter, mahasiswa akan dilatih untuk menghidupi kebiasaan-kebiasaan yang terpuji sehari-hari. Seperti: pola hidup yang teratur dan disiplin, bersih dan rapi, hidup sehat dan makanan yang bergizi, serta disiplin beribadah. Mahasiswa juga akan belajar untuk hidup bekerja sama di dalam tim sekaligus bekerja secara mandiri. IT Del juga menyiapkan layanan lainnya, seperti: layanan kesehatan (melalui tim Dokter kampus) dan layanan konseling (melalui Psikolog) yang siap membantu para mahasiswa. Masih ada juga berbagai kegiatan seru mahasiswa lainnya yang dilakukan di sini, seperti: kegiatan refreshing, retreat, pembekalan soft skill, dsb.</p>', 8, '2021-09-13 23:02:54', '2021-09-13 23:02:54'),
(16, 'Klinik Kesehatan', 'Screenshot from 2021-09-10 00-36-56.png', '<p>Praktek umum dokter Del resmi didirikan pada bulan Mei tahun 2009, terletak disamping pintu masuk ke area kampus Institut Teknologi Del. Tujuan didirikan praktek umum ini yang terutama adalah untuk melayani siswa, mahasiswa, pegawai Del tetapi selain itu juga untuk melayani masyarakat disekitar lingkungan kampus. Praktek umum ini bersifat sosial, non-profit, tetapi juga tetap berusaha menjaga mutu pelayanan agar sesuai dengan standar pelayanan termutakhir. Praktek umum ini juga bekerja sama dengan Jamsostek sebagai PPK I untuk melayani pasien-pasien yang terdaftar sebagai tanggungan Jamsostek dalam wilayah ini. Praktek umum ini bisa melayani pengobatan dasar (meliputi anak dan dewasa) hingga yang membutuhkan tindakan bedah sederhana. Di dalam praktek umum ini juga sudah tersedia obat sehingga tidak perlu membeli obat lagi di tempat lain. Saat ini praktek umum ini sedang dalam proses pengembangan menjadi klinik Yayasan Del.</p>', 7, '2021-09-13 23:03:20', '2021-09-13 23:03:20'),
(17, 'Lapangan Olahraga', 'Screenshot from 2021-09-10 00-38-00.png', '<p>Institut Teknologi Del juga menyediakan sarana lapangan olahraga bagi warga kampus IT Del. Sarana ini dimaksudkan sebagai media untuk menjaga kesehatan dan kebugaran tubuh</p>\r\n\r\n<p>Institut Teknologi memiliki fasilitas lapangan :</p>\r\n\r\n<ol>\r\n	<li>\r\n	<p>3 Lapangan Futsal</p>\r\n	</li>\r\n	<li>\r\n	<p>2 Lapangan Basket</p>\r\n	</li>\r\n	<li>\r\n	<p>1 Lapangan Voli</p>\r\n	</li>\r\n	<li>\r\n	<p>1 Lapangan Badminton</p>\r\n	</li>\r\n	<li>\r\n	<p>Track Jogging</p>\r\n	</li>\r\n</ol>', 9, '2021-09-13 23:04:51', '2021-09-13 23:04:51'),
(18, 'Koperasi', 'Screenshot from 2021-09-10 00-36-56.png', '<p>Institut Teknologi Del juga menyediakan koperasi tempat dimana mahasiswa bisa beristirahat sambil membeli makanan ringan atau membeli kebutuhan sehari-hari di asrama</p>', 10, '2021-09-13 23:05:13', '2021-09-13 23:05:15'),
(19, 'Ruang Terbuka', 'Screenshot from 2021-09-10 00-36-56.png', '<p>Saat kita memperhatikan lingkungan kampus di Indonesia maka mungkin hasilnya akan bertolak belakang dengan saat kita membayangkan lingkungan kampus di negara-negara maju. Membayangkan para mahasiswa kita duduk di bawah pohon-pohon hijau dan beralaskan rumput yang segar, atau membayangkan para mahasiswa melepaskan lelah setelah kuliah di kelas, mengistirahatkan otak mereka dari tumpukan teori sambil bermain bola, atau permainan lainnya di lapangan hijau sangat mungkin jarang kita lihat. Kampus Institut Teknologi Del (IT Del) tidak hanya berfungsi sebagai lembaga pendidikan saja.</p>\r\n\r\n<p>Tapi kampus Del bisa difungsikan sebagai Ruang Terbuka Hijau (RTH). Luas IT Del sekitar 12 hektar dan 60% ruang terbuka hijau. IT Del terbagi dalam tiga area yaitu Open Theater (OT), Asrama, dan Entrance Hall (EH). Selain itu, IT Del terbentang luas pemandangan Danau Toba di area belakang kampus. Fasilitas ruang terbuka sering digunakan oleh mahasiswa di lokasi OT dan EH untuk kerja kelompok, belajar individu, dan bersantai.</p>', 5, '2021-09-13 23:05:42', '2021-09-13 23:05:42'),
(20, 'Auditorium dan Gedung Serbaguna', 'Screenshot from 2021-09-10 00-38-00.png', '<p>Institut Teknologi Del juga memiliki Auditorium dan Gedung Serbaguna yang biasanya digunakan untuk kegiatan acara kampus, fakultas, prodi atau keasramaan seperti Wisuda, Inagurasi, Nonton Bersama dan lain lain</p>', 11, '2021-09-13 23:06:52', '2021-09-13 23:06:53'),
(21, 'Kontener Area', 'Screenshot from 2021-09-10 00-36-56.png', '<p>Institut Teknologi Del memiliki Kontener Area dimana setiap Himpunan Prodi dan Organisasi Mahasiswa memiliki ruang kerja/kantor yang terbuat dari bekas kontener yang di buat menjadi nyaman sebagai ruang kerja/kantor.</p>', 12, '2021-09-13 23:09:04', '2021-09-13 23:09:06');

-- --------------------------------------------------------

--
-- Struktur dari tabel `gambar`
--

CREATE TABLE `gambar` (
  `id` int UNSIGNED NOT NULL,
  `gambar` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `gambar`
--

INSERT INTO `gambar` (`id`, `gambar`, `created_at`, `updated_at`) VALUES
(2, 'test.jpeg', '2021-09-11 09:30:31', '2021-09-11 09:30:31');

-- --------------------------------------------------------

--
-- Struktur dari tabel `himapro`
--

CREATE TABLE `himapro` (
  `id` int UNSIGNED NOT NULL,
  `nama` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `aktifitas` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `deskripsi` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `gambar` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `himapro`
--

INSERT INTO `himapro` (`id`, `nama`, `aktifitas`, `deskripsi`, `gambar`, `created_at`, `updated_at`) VALUES
(2, 'HIMATIF', '<p>HIMATIF(Himpunan Mahasiswa Teknologi Informasi) di Institut Teknologi Del. Program Studi D3 Teknologi Informasi Institut Teknologi Del memiliki Visi dan Misi sebagai berikut. VISI Menjadi program studi unggulan pada program pendidikan dan penelitian terapan di bidang pengembangan teknologi informasi yang bertaraf nasional dan internasional pada tahun 2024. MISI Menyelenggarakan pendidikan vokasi yang unggul untuk menghasilkan sumber daya manusia yang profesional di bidang teknologi informasi dan komunikasi. Meningkatkan program penelitian terapan yang inovatif dan bertaraf nasional maupun internasional di bidang teknologi informasi dan komunikasi. Meningkatkan program pengabdian masyarakat melalui kerjasama dengan berbagai institusi pemerintahan dan industri di tingkat regional maupun nasional.</p>', '<p>HIMATIF(Himpunan Mahasiswa Teknologi Informasi) di Institut Teknologi Del. Program Studi D3 Teknologi Informasi Institut Teknologi Del memiliki Visi dan Misi sebagai berikut. VISI Menjadi program studi unggulan pada program pendidikan dan penelitian terapan di bidang pengembangan teknologi informasi yang bertaraf nasional dan internasional pada tahun 2024. MISI Menyelenggarakan pendidikan vokasi yang unggul untuk menghasilkan sumber daya manusia yang profesional di bidang teknologi informasi dan komunikasi. Meningkatkan program penelitian terapan yang inovatif dan bertaraf nasional maupun internasional di bidang teknologi informasi dan komunikasi. Meningkatkan program pengabdian masyarakat melalui kerjasama dengan berbagai institusi pemerintahan dan industri di tingkat regional maupun nasional.</p>', 'Screenshot from 2021-09-03 23-54-25.png', '2021-09-06 02:08:10', '2021-09-13 23:23:01'),
(3, 'HME', '<p>Himpunan Mahasiswa Elektro Institut Teknologi Del, disingkat dengan HME IT Del. HME didirikan di Sitoluama, Laguboti pada tanggal 14 Oktober 2015. HME IT Del berkedudukan di Fakultas Teknik Elektro dan Informatika Institut Teknologi Del.</p>', '<p>Himpunan Mahasiswa Elektro Institut Teknologi Del, disingkat dengan HME IT Del. HME didirikan di Sitoluama, Laguboti pada tanggal 14 Oktober 2015. HME IT Del berkedudukan di Fakultas Teknik Elektro dan Informatika Institut Teknologi Del.</p>', 'Screenshot from 2021-09-10 00-38-00.png', '2021-09-13 23:21:57', '2021-09-13 23:21:58'),
(4, 'HIMATEK', '<p>HIMATIF(Himpunan Mahasiswa Teknologi Informasi) di Institut Teknologi Del. Program Studi D3 Teknologi Informasi Institut Teknologi Del memiliki Visi dan Misi sebagai berikut. VISI Menjadi program studi unggulan pada program pendidikan dan penelitian terapan di bidang pengembangan teknologi informasi yang bertaraf nasional dan internasional pada tahun 2024. MISI Menyelenggarakan pendidikan vokasi yang unggul untuk menghasilkan sumber daya manusia yang profesional di bidang teknologi informasi dan komunikasi. Meningkatkan program penelitian terapan yang inovatif dan bertaraf nasional maupun internasional di bidang teknologi informasi dan komunikasi. Meningkatkan program pengabdian masyarakat melalui kerjasama dengan berbagai institusi pemerintahan dan industri di tingkat regional maupun nasional.</p>', '<p>HIMATIF(Himpunan Mahasiswa Teknologi Informasi) di Institut Teknologi Del. Program Studi D3 Teknologi Informasi Institut Teknologi Del memiliki Visi dan Misi sebagai berikut. VISI Menjadi program studi unggulan pada program pendidikan dan penelitian terapan di bidang pengembangan teknologi informasi yang bertaraf nasional dan internasional pada tahun 2024. MISI Menyelenggarakan pendidikan vokasi yang unggul untuk menghasilkan sumber daya manusia yang profesional di bidang teknologi informasi dan komunikasi. Meningkatkan program penelitian terapan yang inovatif dan bertaraf nasional maupun internasional di bidang teknologi informasi dan komunikasi. Meningkatkan program pengabdian masyarakat melalui kerjasama dengan berbagai institusi pemerintahan dan industri di tingkat regional maupun nasional.</p>', 'Screenshot from 2021-09-10 00-38-00.png', '2021-09-13 23:24:31', '2021-09-13 23:24:32'),
(5, 'HIMATERA', '<p>Official Account Of Himpunan Mahasiswa DIV Teknologi Rekayasa Perangkat Lunak</p>', '<p>Official Account Of Himpunan Mahasiswa DIV Teknologi Rekayasa Perangkat Lunak</p>', 'Screenshot from 2021-09-10 00-36-56.png', '2021-09-13 23:25:11', '2021-09-13 23:25:12'),
(6, 'HIMASTI', '<p>Official Account Of Himpunan Mahasiswa DIV Teknologi Rekayasa Perangkat Lunak</p>', '<p>Official Account Of Himpunan Mahasiswa DIV Teknologi Rekayasa Perangkat Lunak</p>', 'Screenshot from 2021-09-10 00-36-56.png', '2021-09-13 23:26:39', '2021-09-13 23:26:40'),
(7, 'HIMSI', '<p>Official Account Of Himpunan Mahasiswa DIV Teknologi Rekayasa Perangkat Lunak</p>', '<p>Official Account Of Himpunan Mahasiswa DIV Teknologi Rekayasa Perangkat Lunak</p>', 'Screenshot from 2021-09-10 00-36-56.png', '2021-09-13 23:26:52', '2021-09-13 23:26:54');

-- --------------------------------------------------------

--
-- Struktur dari tabel `info_pmb`
--

CREATE TABLE `info_pmb` (
  `id` int UNSIGNED NOT NULL,
  `jadwal1` date NOT NULL,
  `jadwal2` date NOT NULL,
  `jenis_tes` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_usm` int UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `info_pmb`
--

INSERT INTO `info_pmb` (`id`, `jadwal1`, `jadwal2`, `jenis_tes`, `id_usm`, `created_at`, `updated_at`) VALUES
(2, '2021-09-05', '2021-09-06', 'Nilai Rapor + Wawancara + Psikotes', 2, '2021-09-05 02:27:08', '2021-09-05 02:27:14'),
(3, '2021-09-16', '2021-09-30', 'Nilai Rapor + Wawancara + Psikotes', 3, '2021-09-07 07:25:26', '2021-09-07 07:25:26');

-- --------------------------------------------------------

--
-- Struktur dari tabel `jumlah_lulusan`
--

CREATE TABLE `jumlah_lulusan` (
  `id` int UNSIGNED NOT NULL,
  `id_prodi` int UNSIGNED NOT NULL,
  `tahun_lulus` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jumlah` int NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `jumlah_lulusan`
--

INSERT INTO `jumlah_lulusan` (`id`, `id_prodi`, `tahun_lulus`, `jumlah`, `created_at`, `updated_at`) VALUES
(8, 6, '2018', 239, '2021-09-13 19:28:54', '2021-09-13 19:28:54'),
(9, 3, '2018', 237, '2021-09-13 19:29:19', '2021-09-13 19:29:19'),
(10, 4, '2018', 216, '2021-09-13 19:29:33', '2021-09-13 19:29:33'),
(11, 5, '2018', 144, '2021-09-13 19:29:43', '2021-09-13 19:29:43'),
(12, 1, '2018', 162, '2021-09-13 19:30:09', '2021-09-13 19:30:09'),
(13, 2, '2018', 119, '2021-09-13 19:30:32', '2021-09-13 19:30:32'),
(14, 6, '2019', 245, '2021-09-13 19:30:54', '2021-09-13 19:30:54'),
(15, 3, '2019', 234, '2021-09-13 19:31:12', '2021-09-13 19:31:12'),
(16, 4, '2019', 188, '2021-09-13 19:31:25', '2021-09-13 19:31:25'),
(17, 5, '2019', 180, '2021-09-13 19:31:53', '2021-09-13 19:31:53'),
(18, 1, '2019', 184, '2021-09-13 19:32:08', '2021-09-13 19:32:08'),
(19, 2, '2019', 115, '2021-09-13 19:32:20', '2021-09-13 19:32:20'),
(20, 6, '2020', 235, '2021-09-13 19:32:41', '2021-09-13 19:32:41'),
(21, 3, '2020', 253, '2021-09-13 19:32:59', '2021-09-13 19:32:59'),
(22, 4, '2020', 156, '2021-09-13 19:33:16', '2021-09-13 19:33:16'),
(23, 5, '2020', 211, '2021-09-13 19:33:34', '2021-09-13 19:33:34'),
(24, 1, '2020', 176, '2021-09-13 19:33:47', '2021-09-13 19:33:47'),
(25, 2, '2020', 137, '2021-09-13 19:34:07', '2021-09-13 19:34:07');

-- --------------------------------------------------------

--
-- Struktur dari tabel `kalender_akademik`
--

CREATE TABLE `kalender_akademik` (
  `id` int UNSIGNED NOT NULL,
  `nama` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `kalender` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `kalender_akademik`
--

INSERT INTO `kalender_akademik` (`id`, `nama`, `kalender`, `created_at`, `updated_at`) VALUES
(3, 'Kalender Akademik dan Kemahasiswaan TA 2021/2022', 'kalender_compressed.pdf', '2021-09-13 23:56:16', '2021-09-14 00:01:08');

-- --------------------------------------------------------

--
-- Struktur dari tabel `kalender_kemahasiswaan`
--

CREATE TABLE `kalender_kemahasiswaan` (
  `id` int UNSIGNED NOT NULL,
  `nama` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pdf` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `kategori_dosen`
--

CREATE TABLE `kategori_dosen` (
  `id` int UNSIGNED NOT NULL,
  `nama` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `kategori_dosen`
--

INSERT INTO `kategori_dosen` (`id`, `nama`, `created_at`, `updated_at`) VALUES
(2, 'Dosen', '2021-09-12 09:46:06', '2021-09-12 09:46:06'),
(3, 'Kaprodi', '2021-09-12 09:48:39', '2021-09-12 09:48:39');

-- --------------------------------------------------------

--
-- Struktur dari tabel `kategori_fasilitas`
--

CREATE TABLE `kategori_fasilitas` (
  `id` int UNSIGNED NOT NULL,
  `kategori` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `kategori_fasilitas`
--

INSERT INTO `kategori_fasilitas` (`id`, `kategori`, `created_at`, `updated_at`) VALUES
(2, 'Lab', '2021-09-09 12:20:27', '2021-09-09 12:20:27'),
(3, 'Ruangan Kelas', '2021-09-09 12:24:28', '2021-09-09 12:24:28'),
(4, 'Perpustakaan', '2021-09-09 12:24:42', '2021-09-09 12:24:42'),
(5, 'Ruangan Terbuka', '2021-09-09 12:25:11', '2021-09-09 12:25:11'),
(6, 'Kantin', '2021-09-09 12:25:25', '2021-09-09 12:25:25'),
(7, 'Klinik', '2021-09-09 12:25:57', '2021-09-09 12:25:57'),
(8, 'Asrama dan Rusunawa', '2021-09-13 23:02:20', '2021-09-13 23:02:20'),
(9, 'Lapangan', '2021-09-13 23:03:32', '2021-09-13 23:03:32'),
(10, 'Koperasi', '2021-09-13 23:03:46', '2021-09-13 23:03:46'),
(11, 'Auditorium dan Gedung Serbaguna', '2021-09-13 23:04:13', '2021-09-13 23:04:13'),
(12, 'Area Kontainer', '2021-09-13 23:04:21', '2021-09-13 23:04:21');

-- --------------------------------------------------------

--
-- Struktur dari tabel `kategori_unduhan`
--

CREATE TABLE `kategori_unduhan` (
  `id` int UNSIGNED NOT NULL,
  `nama` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `kategori_unduhan`
--

INSERT INTO `kategori_unduhan` (`id`, `nama`, `created_at`, `updated_at`) VALUES
(3, 'SK', '2021-09-04 02:15:44', '2021-09-04 02:15:44'),
(4, 'UAS', '2021-09-04 03:09:07', '2021-09-04 03:09:07'),
(5, 'Sertifikasi Akreditasi', '2021-09-09 10:10:51', '2021-09-09 10:10:51'),
(6, 'Selebaran', '2021-09-09 10:12:49', '2021-09-09 10:12:49');

-- --------------------------------------------------------

--
-- Struktur dari tabel `kategori_usm`
--

CREATE TABLE `kategori_usm` (
  `id` int UNSIGNED NOT NULL,
  `nama` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `link` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `kategori_usm`
--

INSERT INTO `kategori_usm` (`id`, `nama`, `link`, `created_at`, `updated_at`) VALUES
(2, 'PMDK', 'http://spmb.del.ac.id/index.php?r=pmb-syarat-usm%2Fview&id=1', '2021-09-05 00:57:26', '2021-09-05 00:57:26'),
(3, 'USM 1', 'http://spmb.del.ac.id/index.php?r=pmb-syarat-usm%2Fview&id=2', '2021-09-05 00:57:42', '2021-09-05 00:57:42');

-- --------------------------------------------------------

--
-- Struktur dari tabel `kerjasama`
--

CREATE TABLE `kerjasama` (
  `id` int UNSIGNED NOT NULL,
  `nama` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `gambar` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `kerjasama`
--

INSERT INTO `kerjasama` (`id`, `nama`, `gambar`, `created_at`, `updated_at`) VALUES
(2, 'PT. Omron Electronics', 'PT. Omron Electronics.png', '2021-09-09 10:54:41', '2021-09-14 00:10:24'),
(3, 'PT. Asuransi Sinar Mas', 'PT Asuransi Sinar Mas.png', '2021-09-09 10:54:59', '2021-09-14 00:10:37'),
(4, 'PT. Kalibrr Teknologi Personalia', 'PT. Kalibrr Teknologi Personalia.png', '2021-09-09 10:55:25', '2021-09-14 00:10:49'),
(5, 'PT. Metrodata Electronics, Tbk', 'PT. MetrodataElectronics,Tbk.png', '2021-09-09 10:55:43', '2021-09-14 00:11:02'),
(6, 'PT. Medan Inovasi Bersama', 'Screenshot from 2021-09-02 22-08-14.png', '2021-09-09 10:56:07', '2021-09-13 23:32:17'),
(8, 'PT. Solusi Transportasi Indonesia (GRAB)', 'PT. Solusi Transportasi Indonesia (GRAB).png', '2021-09-13 23:32:26', '2021-09-14 00:12:06'),
(9, 'PT. Huawei Tech Investment', 'PT. Huawei Tech Investment.png', '2021-09-13 23:32:36', '2021-09-14 00:12:16'),
(10, 'PT. Home Credit', 'PT Home Credit.png', '2021-09-13 23:32:44', '2021-09-14 00:12:27'),
(11, 'PT. Monster Group', 'PT Monster Grup.png', '2021-09-13 23:32:53', '2021-09-14 00:12:39'),
(12, 'PT. Global Digital Niaga', 'PT. Global Digital Niaga.png', '2021-09-13 23:33:03', '2021-09-14 00:12:51'),
(13, 'PT. Lifelong Learning', 'Screenshot from 2021-09-10 00-36-56.png', '2021-09-13 23:33:11', '2021-09-13 23:33:11'),
(14, 'PT. Asahan Alumunium', 'PT Asahan Alumunium.png', '2021-09-13 23:33:22', '2021-09-14 00:14:30'),
(15, 'PT. Finansia Multi Finance', 'PT. Finansia Multi Finance.png', '2021-09-13 23:33:31', '2021-09-14 00:14:40'),
(16, 'PT. Dana Indonesia', 'PT. Dana Indonesia.png', '2021-09-13 23:33:40', '2021-09-14 00:14:51'),
(17, 'Sea Group', 'Sea Group.png', '2021-09-13 23:33:48', '2021-09-14 00:15:00');

-- --------------------------------------------------------

--
-- Struktur dari tabel `klub`
--

CREATE TABLE `klub` (
  `id` int UNSIGNED NOT NULL,
  `nama` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `prestasi` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `gambar` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deskripsi` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `klub`
--

INSERT INTO `klub` (`id`, `nama`, `prestasi`, `gambar`, `deskripsi`, `created_at`, `updated_at`) VALUES
(2, 'Del Programming Club', '<p>test</p>', 'Screenshot from 2021-09-03 23-54-17.png', '<p>test</p>', '2021-09-06 02:17:03', '2021-09-13 23:28:17'),
(3, 'Del Mobile Application Club', '<p>test</p>', 'Screenshot from 2021-09-10 00-38-00.png', '<p>test</p>', '2021-09-13 23:28:29', '2021-09-13 23:28:31'),
(4, 'Del Robotic Club', '<p>test</p>', 'Screenshot from 2021-09-10 00-38-00.png', '<p>test</p>', '2021-09-13 23:28:45', '2021-09-13 23:28:46'),
(5, 'Del Cyber Security', '<p>test</p>', 'Screenshot from 2021-09-10 00-36-56.png', '<p>test</p>', '2021-09-13 23:29:02', '2021-09-13 23:29:06'),
(6, 'Del Software & Game Development', '<p>test</p>', 'Screenshot from 2021-09-10 00-36-56.png', '<p>test</p>', '2021-09-13 23:29:18', '2021-09-13 23:29:21'),
(7, 'Del Data Science Club', '<p>test</p>', 'Screenshot from 2021-09-10 00-36-56.png', '<p>test</p>', '2021-09-13 23:29:37', '2021-09-13 23:29:38'),
(8, 'Del UI/UX Club', '<p>test</p>', 'Screenshot from 2021-09-10 00-38-00.png', '<p>test</p>', '2021-09-13 23:29:48', '2021-09-13 23:29:49'),
(9, 'Del Cloud Club', '<p>test</p>', 'Screenshot from 2021-09-10 00-38-00.png', '<p>test</p>', '2021-09-13 23:30:03', '2021-09-13 23:30:04'),
(10, 'Del IoT Club', '<p>test</p>', 'Screenshot from 2021-09-10 00-36-49.png', '<p>test</p>', '2021-09-13 23:30:19', '2021-09-13 23:30:20'),
(11, 'Del E-gov Club', '<p>test</p>', 'Screenshot from 2021-09-10 00-36-56.png', '<p>test</p>', '2021-09-13 23:30:31', '2021-09-13 23:30:36'),
(12, 'Del Toba Tourism Club', '<p>test</p>', 'Screenshot from 2021-09-10 00-36-56.png', '<p>test</p>', '2021-09-13 23:30:44', '2021-09-13 23:30:45');

-- --------------------------------------------------------

--
-- Struktur dari tabel `kompetensi`
--

CREATE TABLE `kompetensi` (
  `id` int UNSIGNED NOT NULL,
  `kompetensi` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_prodi` int UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `kompetensi`
--

INSERT INTO `kompetensi` (`id`, `kompetensi`, `id_prodi`, `created_at`, `updated_at`) VALUES
(1, '<ol>\r\n	<li>Mampu menunjukkan sikap bertakwa kepada Tuhan yang Maha Esa, menjunjung tinggi nilai kemanusiaan, berkontribusi dalam peningkatan mutu kehidupan bermasyarakat, berbangsa, bernegara berdasarkan Pancasila dan memiliki nasionalisme dan tanggung jawab pada negara dan bangsa.</li>\r\n	<li>Mampu menghargai keanekaragaman budaya, pandangan dan agama, taat hukum, disiplin, menginternalisasi nilai, norma dan etika akademik, bertanggungjawab dan menginternalisasi nilai-nilai Del, yaitu MarTuhan, marroha, marbisuk.</li>\r\n	<li>Menguasai konsep teoritis matematika terapan, algoritma, konsep dasar rekayasa perangkat lunak, dan metode pengujian unit perangkat lunak aplikasi meliputi pendekatan black&ndash;box dan white-box functional testing.</li>\r\n	<li>Menguasai pengetahuan tentang teknik komunikasi interpersonal, perkembangan teknologi dan kakas terkini terkait pengembangan perangkat lunak.</li>\r\n	<li>Menguasai prinsip dan cara mengumpulkan, menyimpan, dan mengelola informasi dengan mempertimbangkan permasalahan keamanan data, kebutuhan informasi, integritas data dan inovasi teknologi informasi.</li>\r\n	<li>Mampu menyelesaikan pekerjaan berlingkup luas dan menganalisis data, memecahkan masalah pekerjaan dengan sifat dan konteks yang sesuai dengan bidang keahlian terapannya serta menyusun laporan hasil dan proses kerja secara akurat dan sahih serta mengomunikasikannya secara efektif.</li>\r\n	<li>Mampu memanfaatkan matematika terapan, algoritma, dan prinsip rekayasa perangkat lunak sesuai prosedur dan praktik teknikal. Berpikir komputasi untuk mengidentifikasi dan menyelesaikan persoalan dan solusinya ke dalam representasi data dan algoritma.</li>\r\n	<li>Mampu melakukan transformasi algoritma menjadi source program dengan bahasa pemrograman terkini yang sesuai dengan platform teknologi yang dipersyaratkan pada Software Requirements Specifications (SRS), dan pengujian pada source code perangkat lunak menggunakan pendekatan black&ndash;box dan white-box functional testing .</li>\r\n</ol>', 1, '2021-09-01 00:11:46', '2021-09-13 06:23:48'),
(2, '<p>Kompetensi lulusan yang diharapkan adalah Ahli Madya Teknik Komputer yang mampu kerja dalam standar pekerjaan di sektor industri teknologi informatika dengan kualifikasi:</p>\r\n\r\n<ol>\r\n	<li>Mampu menunjukkan sikap religious, menjunjung tinggi kemanusiaan, dan berkontribusi dalam peningkatan mutu kehidupan bermasyarakat, berbangsa, bernegara, berbangsa berdasarkan pancasila.</li>\r\n	<li>Menginternalisasi nilai-nilai Del yang di cerminkan dalam sikap MarTuhan, Marroha, Marbisuk.</li>\r\n	<li>Mampu memecahkan masalah pekerjaan dengan sifat dan konteks yang sesuai dengan bidang keahlian terapannya didasarkan pada pemikiran logis, inovatif, dan bertanggung jawab atas hasilnya secara mandiri</li>\r\n	<li>Menerapkan konsep sains alam, aplikasi matematika rekayasa; prinsip-prinsip rekayasa (engineering principles), sains rekayasa&nbsp;dan perancangan rekayasa yang diperlukan untuk analisis dan perancangan sistem komputer, proses, produk atau komponen.</li>\r\n	<li>Mampu menerapkan konsep secara umum yang berkaitan dengan sistem komputer dan jaringan komputer, Internet of Things, serta infrastruktur komputasi awan.</li>\r\n	<li>Mampu merancang, membangun, mengelola, dan memelihara jaringan komputer dengan mengikuti fase pembangunan jaringan komputer yang benar melalui penerapan metodologi pembangunan sistem dan kakas pendukung.</li>\r\n	<li>Mampu merancang, mengimplementasikan dan mengelola sistem keamanan jaringan komputer sesuai kebutuhan pengguna.</li>\r\n	<li>Mampu merancang, mengimplementasikan dan mengelola infrastruktur komputasi awan pada platform terkini.</li>\r\n	<li>Mampu merancang, dan mengimplementasikan sistem kontrol elektronik berbasis komputer atau mikrokontroller.</li>\r\n	<li>Mampu merancang dan mengimplementasikan Internet of Things.</li>\r\n</ol>', 2, '2021-09-02 00:34:20', '2021-09-13 06:40:53'),
(3, '<ol>\r\n	<li>Mampu menerapkan konsep matematika, sains, rekayasa, dan pola berpikir komputasional (computational thinking) dalam upaya penyelesaian persoalan;</li>\r\n	<li>Mampu melakukan pengumpulan, interpretasi, pemrosesan, dan visualisasi data dalam rangka membentuk informasi untuk menemukan suatu peluang atau menyelesaikan suatu persoalan bisnis;</li>\r\n	<li>Mampu merancang, merealisasikan, menguji, serta mengevaluasi solusi sistem informasi melalui pendekatan analitis dengan mempertimbangkan standar teknis, unjuk kerja, keamanan, serta faktor-faktor ekonomi, sosial, dan lingkungan;</li>\r\n	<li>Mampu mengelola proyek dalam upaya penyelesaianpersoalan di bidang sistem informasi; dan</li>\r\n	<li>Mampu melakukan riset di bidang sistem informasi.</li>\r\n</ol>', 3, '2021-09-09 01:50:05', '2021-09-13 07:00:03'),
(4, '<ol>\r\n	<li>Mampu menunjukkan sikap dan prilaku bertakwa kepada Tuhan yang Maha Esa dan berkontribusi dalam peningkatan mutu kehidupan bermasyarakat, berbangsa, bernegara berdasarkan Pancasila.</li>\r\n	<li>Mampu bekerja sama, disiplin, bertanggung jawab, dan berkarakter Del: MarTuhan, marroha, marbisuk.</li>\r\n	<li>Menguasai konsep matematika, sains dan rekayasa yang diperlukan untuk analisis dan perancangan sistem, komponen atau proses dalam bidang teknik elektro.</li>\r\n	<li>Mampu mengkaji implikasi pengembangan atau implementasi ilmu pengetahuan teknologi dan mampu mengambil keputusan secara tepat dalam konteks penyelesaian masalah di bidang keahliannya, berdasarkan hasil analisis informasi dan data.</li>\r\n	<li>Mampu menerapkan matematika, sains, dan prinsip rekayasa (engineering principles) dan mampu menemukan sumber masalah rekayasa pada sistem elektronika, sistem kendali (control system), jaringan komputer terdistribusi dan sistem komunikasi nirkabel.</li>\r\n	<li>Mampu melakukan riset yang mencakup identifikasi, formulasi dan analisis masalah rekayasa pada sistem elektronika, sistem kendali (control system), jaringan komputer terdistribusi dan sistem komunikasi nirkabel.</li>\r\n	<li>Mampu merancang, mengimplementasikan, menguji, mengoperasikan, dan pemeliharaan sistem elektronika, sistem kendali (control system), jaringan komputer terdistribusi dan sistem komunikasi nirkabel dengan pendekatan analitis dan mampu mengaplikasikan prinsip manajemen proyek pada sistem elektronika.</li>\r\n</ol>', 4, '2021-09-13 07:24:31', '2021-09-13 07:24:31'),
(5, '<ol>\r\n	<li>Mampu menunjukkan sikap dan prilaku bertakwa kepada Tuhan yang Maha Esa dan berkontribusi dalam peningkatan mutu kehidupan bermasyarakat, berbangsa, bernegara berdasarkan Pancasila.</li>\r\n	<li>Mampu bekerja sama, disiplin, bertanggung jawab, dan berkarakter Del: MarTuhan, marroha, marbisuk.</li>\r\n	<li>Menguasai konsep teoritis matematika dasar dan pengembangan perangkat lunak dengan kompleksitas sedang untuk platform yang berbasis web dan mobile, penjaminan kualitas perangkat lunak secara mendalam mulai dari perencanaan sampai pada tahap pemeliharaan.</li>\r\n	<li>Memiliki pengetahuan dan kemampuan untuk melakukan kajian teknis dalam menentukan perangkat yang tepat untuk digunakan didalam pengembangan perangkat lunak, memformulasikan suatu solusi dengan menggunakan algoritma yang memiliki kompleksitas sedang dan dapat melakukan supervisi, manajemen, dan evaluasi tugas serta pencapaian dalam pengembangan perangkat lunak</li>\r\n	<li>Mampu mengkaji kasus penerapan ilmu pengetahuan dan teknologi yang memperhatikan dan menerapkan nilai humaniora sesuai dengan bidang keahlian sebagai software developer dan quality assurance .</li>\r\n	<li>Mampu mengambil keputusan secara tepat berdasarkan prosedur baku, spesifikasi desain, persyaratan keselamatan dan keamanan kerja dalam melakukan supervisi dan evaluasi pada pekerjaan sebagai software developer dan quality assurance</li>\r\n	<li>Mampu mengidentifikasi, memformulasikan, melakukan penelusuran referensi/standar/codes/database, menganalisis, merancang dan mengimplementasikan desain yang ada berdasarkan kebutuhan pengguna yang dipersyaratkan pada dokumen Software Requirements Specification (SRS)</li>\r\n</ol>', 5, '2021-09-16 06:59:54', '2021-09-16 07:00:38'),
(6, '<ol>\r\n	<li>Mampu menunjukkan sikap dan prilaku bertakwa kepada Tuhan yang Maha Esa dan berkontribusi dalam peningkatan mutu kehidupan bermasyarakat, berbangsa, bernegara berdasarkan Pancasila.</li>\r\n	<li>Mampu bekerja sama, disiplin, bertanggung jawab, dan berkarakter Del: MarTuhan, marroha, marbisuk.</li>\r\n	<li>Menguasai dan memahami konsep teoretis secara umum dari matematika dasar untuk komputer yaitu: Matematika Diskrit, Probalistik, Statistika dan Kalkulus dapat menggunakan konsep teoretis secara umum mengenai kerekayasaan perangkat lunak dan pengelolaan proyek pengembangan perangkat lunak.</li>\r\n	<li>Menguasai dan memahami konsep teoretis secara umum mengenai basis data serta dapat mempertimbangkan sejumlah konsep dalam rangka menghasilkan basis data, prinsip dan teknik desain UI dan UX, teknik keamanan perangkat lunak, sistem komputer dalam rangka menghasilkan solusi komputasi, kecerdasan buatan dan mesin pembelajar.</li>\r\n	<li>Mampu menerapkan pemikiran logis, kritis, sistematis, dan inovatif dalam konteks pengembangan atau implementasi ilmu pengetahuan dan teknologi yang memperhatikan dan menerapkan nilai humaniora yang sesuai dengan bidang keahliannya.</li>\r\n	<li>Mampu melakukan rekayasa perangkat lunak sesuai dengan pedoman kerekayasaan yang baik dan benar dalam rangka memperbaiki, mengembangkan, atau menghasilkan perangkat lunak dan menyusun laporan tertulis pada rekayasa perangkat lunak.</li>\r\n	<li>Mampu merancang, menerapkan, dan menganalisis hingga mengkombinasikan algoritma yang sesuai untuk menyelesaikan permasalahan di bidang komputasi, mempertimbangkan dan memutuskan metode atau prinsip keamanan.</li>\r\n	<li>Mampu menerapkan, membandingkan, atau mengkombinaskan algoritma kecerdasan buatan dan kaidah-kaidah pada kecerdasan buatan untuk menyelesaikan persoalan dan proses penjaminan mutu perangkat lunak.</li>\r\n</ol>', 6, '2021-09-16 07:06:35', '2021-09-16 07:06:35');

-- --------------------------------------------------------

--
-- Struktur dari tabel `kurikulum`
--

CREATE TABLE `kurikulum` (
  `id` int UNSIGNED NOT NULL,
  `id_prodi` int UNSIGNED NOT NULL,
  `id_tahun` int UNSIGNED NOT NULL,
  `pdf` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `kurikulum`
--

INSERT INTO `kurikulum` (`id`, `id_prodi`, `id_tahun`, `pdf`, `created_at`, `updated_at`) VALUES
(2, 1, 3, 'kurikulum_2019.pdf', '2021-09-08 19:28:43', '2021-09-09 06:42:52'),
(4, 2, 3, 'd3tk_2019.pdf', '2021-09-13 23:46:33', '2021-09-13 23:46:35'),
(5, 3, 3, 's1si_2019.pdf', '2021-09-13 23:47:11', '2021-09-13 23:47:11'),
(6, 4, 3, 's1te_2019.pdf', '2021-09-13 23:47:34', '2021-09-13 23:47:35'),
(7, 5, 3, 'd4_2019.pdf', '2021-09-13 23:47:49', '2021-09-13 23:47:51'),
(9, 6, 3, 's1ti_2019.pdf', '2021-09-13 23:48:11', '2021-09-13 23:48:11');

-- --------------------------------------------------------

--
-- Struktur dari tabel `lokasi_tes`
--

CREATE TABLE `lokasi_tes` (
  `id` int UNSIGNED NOT NULL,
  `nama` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lokasi` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `lokasi_tes`
--

INSERT INTO `lokasi_tes` (`id`, `nama`, `lokasi`, `created_at`, `updated_at`) VALUES
(2, 'Kampus Institut Teknologi Del', 'Jl. Sisingamangaraja, Sitoluama Laguboti, Toba Samosir Sumatera Utara', '2021-09-12 10:49:17', '2021-09-12 10:49:17'),
(3, 'Medan', 'Kampus Universitas HKBP Nommensen (Medan) Jl Perintis Kemerdekaan No.23, Perintis, Medan', '2021-09-12 10:49:25', '2021-09-12 10:49:25'),
(4, 'Jakarta', 'Sopo Del Office Towers & Lifestyle Tower A Lt. 9 Jl. Mega Kuningan Barat III Lot 10.1-6 Jakarta Selatan 12950 No. tlp : 021 - 5080 6565', '2021-09-12 10:49:34', '2021-09-12 10:49:34');

-- --------------------------------------------------------

--
-- Struktur dari tabel `migrations`
--

CREATE TABLE `migrations` (
  `id` int UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2021_08_26_074458_create_visi_misi_table', 1),
(4, '2021_08_26_074522_create_program_studi_table', 1),
(5, '2021_08_26_075137_create_visi_misi_prodi_table', 1),
(6, '2021_08_26_075204_create_profil_lulusan_table', 1),
(7, '2021_08_26_075237_create_dosen_prodi_table', 1),
(8, '2021_08_26_075304_create_tujuan_prodi_table', 1),
(9, '2021_08_26_075320_create_kompetensi_table', 1),
(10, '2021_08_26_075417_create_etika_prodi_table', 1),
(11, '2021_08_26_075440_create_berita_fakultas_table', 1),
(12, '2021_08_26_084928_create_capaian_lulusan_table', 1),
(13, '2021_09_03_033238_create_tentang_table', 2),
(14, '2021_09_04_023032_create_fasilitas_table', 3),
(15, '2021_09_04_023240_create_prestasi_table', 3),
(16, '2021_09_04_042413_create_kategori_unduhan_table', 4),
(17, '2021_09_04_042442_create_unduhan_table', 4),
(18, '2021_09_05_060020_create_kategori_pendaftaran_table', 5),
(19, '2021_09_05_073842_create_kategori_usm_table', 6),
(20, '2021_09_05_074400_create_kategori_usm_table', 7),
(21, '2021_09_05_074425_create_info_pmb_table', 8),
(22, '2021_09_05_093147_create_alumni_table', 9),
(23, '2021_09_05_094915_create_alumni_table', 10),
(24, '2021_09_06_074031_create_jumlah_lulusan_table', 11),
(25, '2021_09_06_084702_create_himapro_table', 12),
(26, '2021_09_06_084715_create_klub_table', 12),
(27, '2021_09_06_092322_create_kalender_akademik_table', 13),
(28, '2021_09_08_044722_create_semester_table', 14),
(29, '2021_09_09_013214_create_tahun_table', 15),
(30, '2021_09_09_013249_create_kurikulum_table', 15),
(31, '2021_09_09_124647_create_dekan_table', 16),
(32, '2021_09_09_131428_create_struktur_table', 17),
(33, '2021_09_09_152414_create_pendidikan_table', 18),
(34, '2021_09_09_154936_create_penelitian_table', 19),
(35, '2021_09_09_163255_create_pengabdian_table', 20),
(36, '2021_09_09_174137_create_kerjasama_table', 21),
(37, '2021_09_09_185948_create_kategori_fasilitas_table', 22),
(38, '2021_09_09_190942_create_fasilitas_table', 23),
(39, '2021_09_09_200358_create_data_dosen_table', 24),
(40, '2021_09_10_041852_create_beasiswa_table', 25),
(41, '2021_09_10_041931_create_kalender_kemahasiswaan_table', 25),
(42, '2021_09_11_033100_create_gambar_table', 26),
(43, '2021_09_11_033119_create_slider_table', 26),
(44, '2021_09_12_163622_create_kategori_dosen_table', 27),
(45, '2021_09_12_163655_create_data_dosen_table', 27),
(46, '2021_09_12_173444_create_lokasi_tes_table', 28),
(47, '2021_09_14_074401_create_tujuan_table', 29);

-- --------------------------------------------------------

--
-- Struktur dari tabel `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `pendidikan`
--

CREATE TABLE `pendidikan` (
  `id` int UNSIGNED NOT NULL,
  `nama` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `gambar` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deskripsi` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `tanggal` date NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `pendidikan`
--

INSERT INTO `pendidikan` (`id`, `nama`, `gambar`, `deskripsi`, `tanggal`, `created_at`, `updated_at`) VALUES
(2, 'test', 'Screenshot from 2021-09-03 23-54-25.png', '<p>test</p>', '2021-09-09', '2021-09-09 08:43:29', '2021-09-09 08:43:29'),
(3, 'test12', 'Screenshot from 2021-09-03 23-54-25.png', '<p>test</p>', '2021-09-10', '2021-09-09 10:02:03', '2021-09-09 10:02:03');

-- --------------------------------------------------------

--
-- Struktur dari tabel `penelitian`
--

CREATE TABLE `penelitian` (
  `id` int UNSIGNED NOT NULL,
  `nama` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `gambar` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deskripsi` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `penelitian`
--

INSERT INTO `penelitian` (`id`, `nama`, `gambar`, `deskripsi`, `created_at`, `updated_at`) VALUES
(2, 'test update', 'Screenshot from 2021-09-03 23-54-17.png', '<p>test</p>', '2021-09-09 08:59:25', '2021-09-09 09:31:59');

-- --------------------------------------------------------

--
-- Struktur dari tabel `pengabdian`
--

CREATE TABLE `pengabdian` (
  `id` int UNSIGNED NOT NULL,
  `nama` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `gambar` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deskripsi` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `tanggal` date NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `pengabdian`
--

INSERT INTO `pengabdian` (`id`, `nama`, `gambar`, `deskripsi`, `tanggal`, `created_at`, `updated_at`) VALUES
(2, 'test', 'Screenshot from 2021-09-03 23-54-25.png', '<p>test</p>', '2021-09-09', '2021-09-09 09:41:26', '2021-09-09 09:41:27');

-- --------------------------------------------------------

--
-- Struktur dari tabel `prestasi`
--

CREATE TABLE `prestasi` (
  `id` int UNSIGNED NOT NULL,
  `nama` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `waktu` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tingkatan` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `dicapai` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deskripsi` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `prestasi`
--

INSERT INTO `prestasi` (`id`, `nama`, `waktu`, `tingkatan`, `dicapai`, `deskripsi`, `created_at`, `updated_at`) VALUES
(3, 'Agricode Programming Contest', '18 - 19 September 2015', 'Nasional', 'Finalis', '<p>Agricode Programming Contest</p>', '2021-09-11 10:00:39', '2021-09-13 19:22:09'),
(4, 'Huawei Certified Network Associate', '23 Mei 2016', 'Nasional', 'Certified', '<p>Huawei Certified Network Associate</p>', '2021-09-13 19:23:34', '2021-09-13 19:23:34'),
(5, 'Microsoft Student Partner', '24 Juni 2016', 'Nasional', 'Anggota/Peserta', '<p>Microsoft Student Partner</p>', '2021-09-13 19:24:21', '2021-09-13 19:24:21'),
(6, 'IdeaFuse Programming Conteset', '11 Mei 2016', 'Nasional', 'Finalis', '<p>IdeaFuse Programming Conteset</p>', '2021-09-13 19:25:07', '2021-09-13 19:25:07'),
(7, 'Compfest 8', '17 September 2016', 'Nasional', 'Finalis', '<p>Compfest 8</p>', '2021-09-13 19:25:34', '2021-09-13 19:35:54'),
(8, 'BYOC Hackathon', '5 Mei 2016', 'Nasional', 'Top 10', '<p>BYOC Hackathon</p>', '2021-09-13 19:26:13', '2021-09-13 19:26:13'),
(9, 'Huawei Certified Network Associate', '3 April 2016', 'Nasional', 'Certified', '<p>Huawei Certified Network Associate</p>', '2021-09-13 19:37:43', '2021-09-13 19:37:43'),
(10, 'Microsoft Student Partner', 'September 2016', 'Nasional', 'Anggota/Student', '<p>Microsoft Student Partner</p>', '2021-09-13 19:39:10', '2021-09-13 19:40:34'),
(11, 'Kompetisi Gemastik 9', '16 Agustus 2016', 'Nasional', 'Peserta', '<p>Kompetisi Gemastik 9&nbsp;</p>', '2021-09-13 19:40:15', '2021-09-13 19:40:15'),
(12, 'KPK 2017', '2017', 'Nasional', 'Finalis( Runner Up)', '<p>KPK 2017</p>\r\n\r\n<p>&nbsp;</p>', '2021-09-13 19:41:59', '2021-09-13 19:41:59'),
(13, 'PHBD 2017', '2017', 'Nasional', 'Peserta', '<p>2017</p>\r\n\r\n<p>&nbsp;</p>', '2021-09-13 19:43:52', '2021-09-13 19:43:52'),
(14, 'IToba Fest 2017; Robotik', '2017', 'Wilayah', 'Peserta', '<p>Robotic</p>', '2021-09-13 19:45:13', '2021-09-13 19:45:13'),
(15, 'Pemilihan Mahasiswa Berprestasi (Pilmapres) 2017 program Sarjana', '2017', 'Wilayah', 'Finalis (Juara Harapan 1)', '<p>Pemilihan Mahasiswa Berprestasi (Pilmapres) 2017 program Sarjana</p>', '2021-09-13 19:47:51', '2021-09-13 19:47:51'),
(16, 'Kompetisi Olimpiade Nasional Matematika dan Ilmu Pengetahuan Perguruan Tinggi (ON MIPA - PT) 2017', '2017', 'Internasional', 'Finalis', '<p>Kompetisi Olimpiade Nasional Matematika dan Ilmu Pengetahuan Perguruan Tinggi (ON MIPA - PT) 2017</p>', '2021-09-13 19:48:23', '2021-09-13 19:49:43'),
(17, 'Program Internship the Gyeonggi ICT Global Startup Program 2017', '2017', 'Nasional', 'Finalis', '<p>Program Internship the Gyeonggi ICT Global Startup Program 2017</p>', '2021-09-13 19:48:44', '2021-09-13 19:48:44'),
(18, 'Kompetisi  Collaborative Coding Challenge di Batam', '2017', 'Nasional', 'Finalis', '<p>Kompetisi &nbsp;Collaborative Coding Challenge di Batam</p>', '2021-09-13 19:50:17', '2021-09-13 19:50:17'),
(19, 'Program Young Leaders for Indonesia National Program', '2017', 'Internasional', '-', '<p>Program Young Leaders for Indonesia National Program&nbsp;</p>', '2021-09-13 19:50:47', '2021-09-13 19:51:46'),
(20, 'Computer Festival 2017 Kategori Data Science Academy', '2017', 'Nasional', 'Finalis', '<p>Computer Festival 2017 Kategori Data Science Academy&nbsp;</p>', '2021-09-13 19:51:16', '2021-09-13 19:51:16'),
(21, 'ACM-ICPC Regional Asia-Jakarta', '2017', 'Nasional', 'Finalis', '<p>ACM-ICPC Regional Asia-Jakarta</p>', '2021-09-13 19:52:13', '2021-09-13 19:52:13'),
(22, 'Business Plan Competition i-Toba Festival 2017', '2017', 'Nasional', 'Juara 1', '<p>Business Plan Competition i-Toba Festival 2017</p>', '2021-09-13 19:53:05', '2021-09-13 19:53:05'),
(23, 'Design Competition i-Toba Festival 2017', '2017', 'Nasional', 'Juara 1', '<p>&nbsp;Design Competition i-Toba Festival 2017</p>', '2021-09-13 19:53:40', '2021-09-13 19:53:40'),
(24, 'International Conference Paper di Palembang', '2017', 'Internasional', 'Finalis', '<p>International Conference Paper di Palembang&nbsp;</p>', '2021-09-13 19:54:37', '2021-09-13 19:54:37'),
(25, 'Kompetisi Olimpiade Nasional Matematika dan Ilmu Pengetahuan Perguruan Tinggi (ON MIPA - PT) 2018', '2018', 'Nasional', 'Juara Harapan 1', '<p>Kompetisi Olimpiade Nasional Matematika dan Ilmu Pengetahuan Perguruan Tinggi (ON MIPA - PT) 2018</p>', '2021-09-13 19:55:27', '2021-09-13 19:55:27'),
(26, 'IT FEST Universitas Sumatera Utara', '2018', 'Wilayah', 'Juara 2', '<p>IT FEST Universitas Sumatera Utara</p>', '2021-09-13 19:56:14', '2021-09-13 19:56:14'),
(27, 'Pemilihan Mahasiswa Berprestasi (Pilmapres) 2018 program Sarjana', '2018', 'Wilayah', 'Finalis( Juara Harapan 1)', '<p>Pemilihan Mahasiswa Berprestasi (Pilmapres) 2018 program Sarjana</p>', '2021-09-13 19:57:20', '2021-09-13 19:57:20'),
(28, 'Asia World Model United Nations (AWMUN) II', '30 Januari- 2 Februari', 'Internasional', 'Selected Presenter', '<p>Asia World Model United Nations (AWMUN) II</p>', '2021-09-13 19:58:36', '2021-09-13 19:58:36'),
(29, 'Kompetisi membuat Aplikasi DUIT Pada Kompetisi Hackathon 2019 (Kemenkeu) di Jakarta', '4 Maret 2019', 'Nasional', 'Juara 1', '<p>Kompetisi membuat Aplikasi DUIT Pada Kompetisi Hackathon 2019 (Kemenkeu) di Jakarta</p>', '2021-09-13 19:59:17', '2021-09-13 19:59:17'),
(30, 'Pemilihan Mahasiswa Berprestasi (PILMAPRES) 2019 tingkat Sarjana di Medan', '4-5 April 2019', 'Wilayah', 'Juara 3', '<p>Pemilihan Mahasiswa Berprestasi (PILMAPRES) 2019 tingkat Sarjana di Medan</p>', '2021-09-13 20:00:06', '2021-09-13 20:00:06'),
(31, 'Kontes Robot  Indonesia (KRI) 2019 di Lampung', '4-6 April 2019', 'Wilayah', 'Finalis', '<p>Kontes Robot &nbsp;Indonesia (KRI) 2019 di Lampung</p>', '2021-09-13 20:00:50', '2021-09-13 20:00:50'),
(32, 'Kompetisi E-TIME (Electro Activities Programme) tahun 2019, di Politeknik Negeri Jakarta', '8-11 April 2019', 'Nasional', 'Finalis', '<p>Kompetisi E-TIME (Electro Activities Programme) tahun 2019, di Politeknik Negeri Jakarta</p>', '2021-09-13 20:01:31', '2021-09-13 20:01:31'),
(33, 'Kumparan Academy Development Program Bootcamp di Jakarta', '15 April -15 Juni 2019', 'Nasional', 'Selected Participant', '<p>Kumparan Academy Development Program Bootcamp di Jakarta</p>', '2021-09-13 20:02:46', '2021-09-13 20:02:46'),
(34, 'Kompetisi Olimpiade Nasional Matematika dan Ilmu Pengetahuan Alam (ON MIPA) 2019 di Makasar', '25 -29 April 2019', 'Nasional', 'Finalis', '<p>Nasional</p>', '2021-09-13 20:05:21', '2021-09-13 20:05:21'),
(35, 'Juara ke 2 Pada Lomba 9TH Product Design Competition Region: SUMUT & ACEH', '19 -21 Juni 2019', 'Regional', 'Juara 2', '<p>Juara ke 2 Pada Lomba 9TH Product Design Competition Region: SUMUT &amp; ACEH</p>', '2021-09-13 20:06:27', '2021-09-13 20:06:27'),
(36, 'Kontes Robot Indonesia (KRI) 2019 di Semarang', '20 - 23 Juni 2019', 'Nasional', 'Finalis', '<p>Kontes Robot Indonesia (KRI) 2019 di Semarang</p>', '2021-09-13 20:08:01', '2021-09-13 20:08:01'),
(37, 'Finalist Ideafuse ACM-ICPC Multi-Provincial Programming Contest', '2019', 'Nasional', 'Finalis', '<p>Finalist Ideafuse ACM-ICPC Multi-Provincial Programming Contest</p>', '2021-09-13 20:08:47', '2021-09-13 20:08:47'),
(38, 'Kontes Robot Terbang Indonesia', '2019', 'Nasional', 'Finalis', '<p>Kontes Robot Terbang Indonesia (KRTI)</p>', '2021-09-13 20:09:50', '2021-09-13 20:09:50'),
(39, 'Kompetisi Techporia', '2019', 'Nasional', 'Finalis', '<p>Kompetisi Techporia&nbsp;</p>', '2021-09-13 20:10:16', '2021-09-13 20:10:16'),
(40, 'Kompetisi Robot Seni Tari Indonesia (KRSTI)', '2019', 'Nasional', 'Finalis', '<p>Kompetisi Robot Seni Tari Indonesia (KRSTI)</p>', '2021-09-13 20:10:44', '2021-09-13 20:10:44'),
(41, 'Pemenang lomba foto HUT RI Ke-74', '2019', 'Finalis', 'Regional Tobasa', '<p>Pemenang lomba foto HUT RI Ke-74</p>', '2021-09-13 20:11:27', '2021-09-13 20:11:27'),
(42, 'Lolos sebagai Peserta GO-JEK Engineering Bootcamp di Bangalore, India', '5 Agustus -13 Oktober 2019', 'Internasional', 'Finalis', '<p>Lolos sebagai Peserta GO-JEK Engineering Bootcamp di Bangalore, India</p>', '2021-09-13 20:16:45', '2021-09-13 20:16:45'),
(43, 'PKM AI – tim Jomen Pardede prodi S1 Teknik Elektro', '2019', 'Nasional', 'Pemenang', '<p>PKM AI &ndash; tim Jomen Pardede prodi S1 Teknik Elektro</p>', '2021-09-13 20:17:15', '2021-09-13 20:17:15'),
(44, 'Kompetisi Hology Programming 2019', '26 -28 Oktober 2019', 'Nasional', 'Finalis', '<p>Kompetisi Hology Programming 2019</p>', '2021-09-13 20:18:06', '2021-09-13 20:18:06'),
(45, 'Kompetisi Nasional Technology Euphoria 2019', '16 - 19 Oktober 2019', 'Nasional', 'Finalis', '<p>Kompetisi Nasional Technology Euphoria 2019</p>', '2021-09-13 20:19:17', '2021-09-13 20:19:17'),
(46, 'Finalis Pagelaran Mahasiswa Nasional Bidang Teknologi Informasi dan Komunikasi (GemasTIK) ke-12 Tahun 2019', '10 – 14 Oktober 2019', 'Nasional', 'Finalis', '<p>Finalis Pagelaran Mahasiswa Nasional Bidang Teknologi Informasi dan Komunikasi (GemasTIK) ke-12 Tahun 2019</p>', '2021-09-13 20:20:02', '2021-09-13 20:20:02'),
(47, 'ICPC 2019', '26-28 Oktober 2019', 'Nasional', 'Finalis', '<p>ICPC 2019</p>', '2021-09-13 20:20:38', '2021-09-13 20:20:38'),
(48, 'Kompetisi Komunitas Siber Indonesia TNI - AD Tahun 2019', '9 - 10 Nopember 2019', 'Nasional', 'Finalis', '<p>Kompetisi Komunitas Siber Indonesia TNI - AD Tahun 2019</p>', '2021-09-13 20:21:20', '2021-09-13 20:21:20'),
(49, 'Kompetisi INACOM  Tahun 2019', '13 - 15 Nopember 2019', 'Nasional', 'Finalis', '<p>Kompetisi INACOM &nbsp;Tahun 2019</p>', '2021-09-13 20:21:54', '2021-09-13 20:21:54'),
(50, 'Lolos sebagai Peserta GO-JEK Engineering Bootcamp di Bangalore, India', '5 Agustus - 13 Oktober 2019', 'Internasional', 'Participant', '<p>Lolos sebagai Peserta GO-JEK Engineering Bootcamp di Bangalore, India</p>', '2021-09-13 20:22:42', '2021-09-13 20:22:42'),
(51, 'International Collegiate Programming Contest  (ICPC) Maranatha 2019', '27 - 29 September 2019', 'Nasional', 'Finalis', '<p>International Collegiate Programming Contest &nbsp;(ICPC) Maranatha 2019</p>', '2021-09-13 20:23:12', '2021-09-13 20:23:12'),
(52, 'Program Kreativitas Mahasiswa (PKM) Kategori Artikel Ilmiah Pendanaan Tahun 2019', 'Oktober 2019', 'Nasional', 'Pemenang', '<p>Program Kreativitas Mahasiswa (PKM) Kategori Artikel Ilmiah Pendanaan Tahun 2019</p>', '2021-09-13 20:24:07', '2021-09-13 20:24:07'),
(53, 'Kompetisi Nasional Technology Euphoria 2019', '16 - 19 Oktober 2019', 'Nasional', 'Finalis', '<p>Kompetisi Nasional Technology Euphoria 2019</p>', '2021-09-13 20:24:59', '2021-09-13 20:24:59'),
(54, 'Finalis Pagelaran Mahasiswa Nasional Bidang Teknologi Informasi dan Komunikasi (GemasTIK) ke-12 Tahun 2019', '23 - 27 Oktober 2019', 'Nasional', 'Finalis', '<p>Finalis Pagelaran Mahasiswa Nasional Bidang Teknologi Informasi dan Komunikasi (GemasTIK) ke-12 Tahun 2019</p>', '2021-09-13 20:25:23', '2021-09-13 20:25:23'),
(55, 'Kompetisi Hology Programming 2019', '26 - 28 Oktober 2019', 'Nasional', 'Finalis', '<p>Kompetisi Hology Programming 2019</p>', '2021-09-13 20:25:47', '2021-09-13 20:25:47'),
(56, 'Kompetisi International Collegiate Programming Contest  (ICPC)Asia Jakarta Regional Contest 2019', '26 - 28 Oktober 2019', 'Nasional', 'Finalis', '<p>Kompetisi International Collegiate Programming Contest &nbsp;(ICPC)Asia Jakarta Regional Contest 2019</p>', '2021-09-13 20:26:17', '2021-09-13 20:26:17'),
(57, 'Inovative App Competition (INACOM) Tahun 2019', '13 - 15 Nopember 2019', 'Nasional', 'Finalis', '<p>Inovative App Competition (INACOM) Tahun 2019</p>', '2021-09-13 20:27:29', '2021-09-13 20:27:29'),
(58, 'Festival Danau Toba Tahun 2019 - Paduan Suara Mahasiswa (PSM)', '9 - 10 Desember 2019', 'Nasional', 'Juara 2', '<p>Festival Danau Toba Tahun 2019 - Paduan Suara Mahasiswa (PSM)</p>', '2021-09-13 20:27:53', '2021-09-13 20:27:53'),
(59, 'Conference Gojek Student Summit Tahun 2019', '14 - 16 Desember 2019', 'Nasional', 'Participant', '<p>Conference Gojek Student Summit Tahun 2019</p>', '2021-09-13 20:28:27', '2021-09-13 20:28:27'),
(60, 'Diklat Animasi 3D di Bali', '17 Januari - 11 Februari 2020', 'Nasional', 'Finalis', '<p>Diklat Animasi 3D di Bali</p>', '2021-09-13 20:29:25', '2021-09-13 20:29:25'),
(61, 'Spring 2020  Students Exchange Programme to Faculty of Bussiness and Economics, Amsterdam University of Applied Sciences', 'Januari - Juni 2020', 'Internasional', 'Participant', '<p>Spring 2020 &nbsp;Students Exchange Programme to Faculty of Bussiness and Economics, Amsterdam University of Applied Sciences</p>', '2021-09-13 20:30:56', '2021-09-13 20:30:56'),
(62, 'KN - MIPA 2020', '20 Maret 2020', 'Lokal/Daerah/Kampus', 'Finalis', '<p>KN - MIPA 2020</p>', '2021-09-13 20:31:37', '2021-09-13 20:31:37'),
(63, 'Pemilihan Mahasiswa Prestasi (PILMAPRES) 2020', 'Mei - Juni 2020', 'Nasional', 'Finalis', '<p>Pemilihan Mahasiswa Prestasi (PILMAPRES) 2020</p>', '2021-09-13 20:32:00', '2021-09-13 20:32:00'),
(64, 'Program Kreativitas Mahasiswa Kategori Karya Tulis PKM - KT) Pendanaan Tahun 2020', '22 Juli 2020', 'Nasional', 'Pemenang', '<p>Program Kreativitas Mahasiswa Kategori Karya Tulis PKM - KT) Pendanaan Tahun 2020</p>', '2021-09-13 20:32:49', '2021-09-13 20:32:49'),
(65, 'Program Kreativitas Mahasiswa 5 Bidang (PKM) Pendanaan Tahun 2020', '5 Agustus 2020', 'Nasional', 'Pemenang', '<p>Program Kreativitas Mahasiswa 5 Bidang (PKM) Pendanaan Tahun 2020</p>', '2021-09-13 20:33:26', '2021-09-13 20:33:26'),
(66, 'National  University Debate Championship (NUDC) 2020', '1 - 14 Agustus 2020', 'Wilayah', 'Finalis', '<p>National &nbsp;University Debate Championship (NUDC) 2020</p>', '2021-09-13 20:33:56', '2021-09-13 20:33:56'),
(67, 'Kompetisi Nasional Matematika dan Ilmu Pengetahuan Alam (KN - MIPA) 2020', '19 Agustus 2020', 'Wilayah', 'Finalis', '<p>Kompetisi Nasional Matematika dan Ilmu Pengetahuan Alam (KN - MIPA) 2020</p>', '2021-09-13 20:34:44', '2021-09-13 20:34:44'),
(68, 'ICStar Hackathon Competition 2020', '2 September 2020', 'Nasional', 'Finalis', '<p>ICStar Hackathon Competition 2020</p>', '2021-09-13 20:35:22', '2021-09-13 20:35:22'),
(69, 'Festival Danau Toba Tahun 2019 - Paduan Suara Mahasiswa (PSM)', '9 - 10 Desember 2019', 'Nasional', 'Juara 2', '<p>Festival Danau Toba Tahun 2019 - Paduan Suara Mahasiswa (PSM)</p>', '2021-09-13 20:35:55', '2021-09-13 20:35:55'),
(70, 'Program Kreativitas Mahasiswa Kategori Karya Tulis PKM - KT) Pendanaan Tahun 2020', '22b Juli 2020', 'Nasional', 'Penerima insentif (PKM GT)', '<p>Program Kreativitas Mahasiswa Kategori Karya Tulis PKM - KT) Pendanaan Tahun 2020</p>', '2021-09-13 20:36:26', '2021-09-13 20:36:26'),
(71, 'Program Kreativitas Mahasiswa 5 Bidang (PKM) Pendanaan Tahun 2020', '5 Agustus 2020', 'Nasional', 'Penerima Pendanaan (PKM 5 Bidang)', '<p>Program Kreativitas Mahasiswa 5 Bidang (PKM) Pendanaan Tahun 2020</p>', '2021-09-13 20:37:07', '2021-09-13 20:37:07'),
(72, 'ICStar Hackathon Competition 2020 - Robotic Process Automation', '20 September 2020', 'Nasional', 'Juara 3', '<p>ICStar Hackathon Competition 2020 - Robotic Process Automation</p>', '2021-09-13 20:38:35', '2021-09-13 20:38:35'),
(73, 'Program Kreativitas Mahasiswa 5 Bidang (PKM) Pendanaan Tahun 2021', '2021', 'Nasional', 'Penerima Pendanaan (PKM 5 Bidang)', '<p>Program Kreativitas Mahasiswa 5 Bidang (PKM) Pendanaan Tahun 2021</p>', '2021-09-13 20:39:12', '2021-09-13 20:39:12'),
(74, 'Pemilihan Mahasiswa Prestasi (PILMAPRES) 2021', '8 Juni 2021', 'Nasional', 'Finalis', '<p>Pemilihan Mahasiswa Prestasi (PILMAPRES) 2021</p>', '2021-09-13 20:39:46', '2021-09-13 20:39:46'),
(75, 'Hackaton Maritim', '16 April 2021', 'Nasional', 'Juara harapan', '<p>Hackaton Maritim</p>', '2021-09-13 20:40:25', '2021-09-13 20:40:25'),
(76, 'Kontes Robot Indonesia (KRI) 2021', '22 Juni 2021', 'Nasional', 'Finalis', '<p>Kontes Robot Indonesia (KRI) 2021 pada&nbsp;22 Juni 2021</p>', '2021-09-13 20:41:03', '2021-09-13 20:41:03');

-- --------------------------------------------------------

--
-- Struktur dari tabel `profil_lulusan`
--

CREATE TABLE `profil_lulusan` (
  `id` int UNSIGNED NOT NULL,
  `profil_lulusan` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jenis_pekerjaan` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deskripsi` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_prodi` int UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `profil_lulusan`
--

INSERT INTO `profil_lulusan` (`id`, `profil_lulusan`, `jenis_pekerjaan`, `deskripsi`, `id_prodi`, `created_at`, `updated_at`) VALUES
(2, 'Programmer', 'Web programmer, Mobile programmer, Desktop programmer, Database Programmer, System  programmer', '<p>Mengonversi spesifikasi dan&nbsp;<em>problem statement&nbsp;</em>beserta prosedur menjadi&nbsp;<em>logical flow&nbsp;</em>yang detail sehingga siap diimplementasi menjadi program komputer yang siap dimanfaatkan oleh &ldquo;<em>user&rdquo;&nbsp;</em>(manusia, program aplikasi, atau</p>\r\n\r\n<p>sistem komputasi).</p>', 1, '2021-09-01 01:27:59', '2021-09-01 01:27:59'),
(3, 'Pengelola Data', 'Database Administrator, Data Analyst. Data Architect, Data Entry Supervisor', '<p>Orang yang mampu membuat desain&nbsp;<em>database&nbsp;</em>dan dapat mengimplementasikannya serta mampu melakukan instalasi konfigurasi,&nbsp;<em>upgrade</em>, adaptasi,&nbsp;<em>monitoring</em>, dan</p>\r\n\r\n<p>pemeliharaan&nbsp;<em>database&nbsp;</em>dalam suatu organisasi.</p>', 1, '2021-09-01 01:28:26', '2021-09-01 01:28:26'),
(4, 'Software Quality Assurance (Penjamin Kualitas Perangkat Lunak)', 'Software Quality Assurance Tester, Data Security Administrator, Information Security Administrator, Safety and Security Program Supervisor, Information Security Supervisor, IT Security  Management Supervisor', '<p>Mampu berperan sebagai penguji perangkat lunak yang bertanggung jawab atas kebenaran fungsional (sesuai dengan hasil yang diharapkan) dan non-fungsional (contohnya: kegunaan, kinerja, keamanan, skalabilitas) dari sebuah perangkat lunak baik dengan manual maupun secara otomatis. Tahapan pengujian yang dicakup mulai dari perancangan, pelaksanaan, evaluasi, dan dokumentasi hasil</p>\r\n\r\n<p>uji.</p>', 1, '2021-09-01 01:28:44', '2021-09-01 01:28:44'),
(5, 'Software Developer', 'Software Developer', '<p>Software developer adalah seseorang yang terlibat dalam fase-fase pengembangan perangkat lunak yang meliputi penggalian kebutuhan, analisis, perancangan, pemrograman dan pengujian perangkat lunak.</p>', 5, '2021-09-16 06:56:46', '2021-09-16 06:56:46'),
(6, 'Software Quality Assurance', 'Software Quality Assurance', '<p>Software quality assurance adalah seorang praktisi yang bekerja untuk memastikan kualitas dari perangkat lunak</p>', 5, '2021-09-16 06:59:04', '2021-09-16 06:59:04'),
(7, 'IT Researcher', 'IT Researcher', '<p>Lulusan informatika dapat bekerja sebagai peneliiti di bidangnya</p>', 5, '2021-09-16 06:59:23', '2021-09-16 06:59:23'),
(8, 'IT Preneur', 'IT Preneur', '<p>IT Preneur adalah seseorang yang memiliki usaha di bidang teknologi</p>', 5, '2021-09-16 06:59:37', '2021-09-16 06:59:37'),
(9, 'Software Engineer', 'Software Analyst, Software Tester', '<p>Mobile Application Developer, Web Developer (front-end, back-end, service), Full-Stack Developer, User Interface Designer, User Experience Designer, Software Analyst, Database Engineer, Software Quality Assurance dan Software Tester</p>', 6, '2021-09-16 07:10:00', '2021-09-16 07:10:00'),
(10, 'Cybersecurity Practitioner', 'Cybersecurity Practitioner', '<p>CSX-P tetap menjadi sertifikasi kinerja komprehensif pertama dan satu-satunya yang menguji kemampuan seseorang untuk melakukan keterampilan keamanan siber yang divalidasi secara global yang mencakup lima fungsi keamanan &ndash; Identifikasi, Lindungi, Deteksi, Tanggapi, dan Pulihkan &ndash; yang berasal dari&nbsp;<a href=\"https://www.nist.gov/cyberframework\">Kerangka Kerja</a>&nbsp;Keamanan&nbsp;<a href=\"https://www.nist.gov/cyberframework\">Siber NIST</a>&nbsp;.CSX-P tetap menjadi sertifikasi kinerja komprehensif pertama dan satu-satunya yang menguji kemampuan seseorang untuk melakukan keterampilan keamanan siber yang divalidasi secara global yang mencakup lima fungsi keamanan &ndash; Identifikasi, Lindungi, Deteksi, Tanggapi, dan Pulihkan &ndash; yang berasal dari&nbsp;<a href=\"https://www.nist.gov/cyberframework\">Kerangka Kerja</a>&nbsp;Keamanan&nbsp;<a href=\"https://www.nist.gov/cyberframework\">Siber NIST</a>&nbsp;.</p>', 6, '2021-09-16 07:11:51', '2021-09-16 07:11:51');

-- --------------------------------------------------------

--
-- Struktur dari tabel `program_studi`
--

CREATE TABLE `program_studi` (
  `id` int UNSIGNED NOT NULL,
  `nama_prodi` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `kode_prodi` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deskripsi` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `program_studi`
--

INSERT INTO `program_studi` (`id`, `nama_prodi`, `kode_prodi`, `deskripsi`, `created_at`, `updated_at`) VALUES
(1, 'D3 Teknologi Informasi', 'D3TI', '<p>Program Studi Teknologi Informasi Diploma Tiga adalah salah satu dari 3 (tiga) program studi yang dikelola oleh Institut Teknologi Del (IT Del) yang berdiri pada tahun 2001 sesuai SK No. 222/D/O/2001 tertanggal 28 September 2001 dengan nama Program Studi Teknologi Informasi. Program studi ini berlokasi di Jl. Sisingamangaraja, Desa Sitoluama, Kecamatan Laguboti, Toba Samosir, Propinsi Sumatera Utara berjarak kurang lebih 200 km (lima jam perjalanan mobil) dari Medan sebagai Ibukota Propinsi Sumatera Utara. Desa Sitoluama adalah suatu desa kecil yang berada di tepi Danau Toba dan dilalui oleh jalan raya lintas propinsi dan berjarak sekitar 10 KM dari Balige sebagai ibukota Kabupaten Tobasa.Program Studi Teknologi Informasi Diploma Tiga mempunyai sasaran untuk menyelenggarakan proses pembelajaran yang dapat menumbuhkan-kembangkan daya nalar, daya cipta, daya kreasi dan keterampilan yang tinggi, yang dapat dikomunikasikan dan diaplikasikan pada bidang kehidupan. Prodi ini memperoleh perpanjangan ijin penyelenggaraan Program Studi Teknologi Informasi Diploma Tiga (10802) sesuai dengan SK Direktur Jenderal Pendidikan Tinggi Nomor 3649/D/T/2004 tertanggal 9 September 2004. Kemudian pada tanggal 11 Oktober 2007 memperoleh SK perpanjangan ulang No. 3169/D/T/2007 dengan sebutan nama program studi adalah Program Studi Teknologi Informasi Diploma Tiga. Kemudian pada tanggal 3 Mei 2010 memperoleh SK perpanjangan ulang No. 1854/D/T/K-I/2010 dengan sebutan nama program studi kembali menjadi Program Studi Teknologi Informasi Diploma Tiga .</p>', '2021-09-01 00:01:16', '2021-09-13 06:02:57'),
(2, 'D3 Teknologi Komputer', 'D3TK', '<p>Perjalanan penyelenggaraan pendidikan di Politeknik Informatika Del khususnya Program Studi D III Teknik Komputer telah berlangsung selama 13 tahun sejak berdiri pada tahun 2001 sesuai SK No. 222/D/O/2001 tertanggal 28 September 2001. Lokasi penyelenggaraan pendidikan yang berada di remote area menjadikan Politeknik Informatika Del sebagai sebuah institusi yang unik dan berbeda, berkarakter, menjunjung tinggi kedisiplinan namun tetap berbudaya dan ceria. Pada awal berdirinya program studi DIII Teknik Komputer, kompetensi lulusan diprioritaskan pada Teknologi Jaringan Komputer. Konsentrasi TJK ini sangat mudah diserap oleh masyarakat karena mahasiswa/alumni memiliki kemampuan di bidang jaringan komputer tersebut.</p>', '2021-09-02 00:32:23', '2021-09-13 06:51:01'),
(3, 'S1 Sistem Informasi', 'S1 SI', '<p>Pada saat perubahan institusi dari Politeknik Informatika Del ke Institut Teknologi Del, dilakukan penambahan Program Studi S1 yang baru, yakni Teknik Elektro, Manajemen Rekayasa, Teknik Bioproses, Teknik Informatika, dan Sistem Informasi. Jurusan Sistem Informasi Institut Teknologi Del mulai melakukan penerimaan mahasiswa baru pada T.A 2014/2015. Jumlah mahasiswa yang diterima pada Tahun Ajaran tersebut adalah 56 mahasiswa. Dengan dukungan dosen yang ahli dalam sistem informasi, program studi ini dirancang untuk memenuhi kebutuhan terhadap tenaga-tenaga muda yang terampil dan profesional, terutama terkait dengan pengembangan, pemanfaatan, dan pengelolaan Sistem Informasi/Teknologi Informasi dalam suatu organisasi. Kurikulum Program Studi Sistem Informasi IT Del mengacu kepada beberapa kurikulum Sarjana Sistem Informasi di Indonesia maupun Internasional, seperti: Program Studi Sarjana Teknologi dan Sistem Informasi ITB, Program Studi Sarjana Sistem Informasi Universitas Indonesia serta mengacu kepada kurikulum ACM (Association for Computing Machinary) for Information System.</p>', '2021-09-02 00:43:52', '2021-09-13 06:52:57'),
(4, 'S1 Teknik Elektro', 'S1 TE', '<p>Sarjana Teknik Elektro berdiri pada tahun 2013 dengan nama Sarjana Teknik Elektro.</p>', '2021-09-02 00:57:46', '2021-09-13 07:18:39'),
(5, 'D4 Rekayasa Perangkat Lunak', 'D4TRPL', '<ol>\r\n	<li>Menigkatkan peringkat akreditasi program studi menjadi unggul</li>\r\n	<li>Mewujudkan tata kelola program strudi yang berkualitas</li>\r\n	<li>Menghasilkan alumni yang memiliki pengetahuan dan kterampilan dalam bidang informatika yang gayut terhadap kebutuhan industri, berjiwa kepemimpinan, serta berkarakter mar-Tuhan, marroha, dan marbisuk</li>\r\n	<li>Menigkatkan kualitas sumber daya manusia serta sarana dan prasarana akademik</li>\r\n	<li>Menyediakan organisasi serta sarana dan prasarana penelitian yang berkualitas demi terwujudnya rencana dan peta jalan penelitian yang bertaraf nasional dalam bidang informatika</li>\r\n</ol>', '2021-09-02 01:00:21', '2021-09-16 07:03:57'),
(6, 'S1 Teknik Informatika', 'S1 TI', '<p>Sarjana Informatika berdiri pada tahun 2013 dengan nama Sarjana Teknik Informatika. Pada tahun 2019 atas petunjuk Kemenderistek Dikti berubah menjadi Sarjana Informatika</p>', '2021-09-02 01:00:43', '2021-09-16 07:04:54');

-- --------------------------------------------------------

--
-- Struktur dari tabel `semester`
--

CREATE TABLE `semester` (
  `id` int UNSIGNED NOT NULL,
  `semester` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `slider`
--

CREATE TABLE `slider` (
  `id` int UNSIGNED NOT NULL,
  `gambar` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `slider`
--

INSERT INTO `slider` (`id`, `gambar`, `created_at`, `updated_at`) VALUES
(2, 'twibbon.png', '2021-09-11 09:46:18', '2021-09-11 09:46:19'),
(3, 'Riyanthi_Sianturi_Institut_Teknologi_Del_11.jpg', '2021-09-11 10:18:41', '2021-09-11 10:18:41'),
(4, 'back.png.png', '2021-09-11 10:19:01', '2021-09-11 10:19:01');

-- --------------------------------------------------------

--
-- Struktur dari tabel `struktur`
--

CREATE TABLE `struktur` (
  `id` int UNSIGNED NOT NULL,
  `nama` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `gambar` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `struktur`
--

INSERT INTO `struktur` (`id`, `nama`, `gambar`, `created_at`, `updated_at`) VALUES
(2, 'Struktur FITE', 'struktur.jpg', '2021-09-09 06:26:00', '2021-09-13 05:57:50');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tahun`
--

CREATE TABLE `tahun` (
  `id` int UNSIGNED NOT NULL,
  `tahun` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `tahun`
--

INSERT INTO `tahun` (`id`, `tahun`, `created_at`, `updated_at`) VALUES
(2, '2021', '2021-09-08 18:49:49', '2021-09-08 18:49:49'),
(3, '2019', '2021-09-08 20:51:49', '2021-09-08 20:51:49');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tentang`
--

CREATE TABLE `tentang` (
  `id` int UNSIGNED NOT NULL,
  `tentang` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `tentang`
--

INSERT INTO `tentang` (`id`, `tentang`, `created_at`, `updated_at`) VALUES
(1, '<p>Fakultas Informatika dan Teknik Elektro (FITE) IT Del berdiri pada tahun 2013 seiring dengan perubahan Politeknik Informatika Del (PI Del) menjadi IT Del. Pada awalnya berdiri Fakultas Informatika dan Teknik Elektro (FITE) bernama Fakultas Teknik Informatika dan Elektro (FTIE) dengan Dekan dipegang oleh Dr. Arnaldo Marulitua Sinaga, ST., M.InfoTech. Adapun tahun-tahun-tahun penting terkait sejarah FITE dapat di lihat sebagai berikut :&nbsp;</p>\r\n\r\n<p><strong>1. 31 Agustus 2013 :</strong> Berdirinya Fakultas Teknik Informatika dan Elektro (FTIE) dengan 6 Program Studi yaitu :</p>\r\n\r\n<ol>\r\n	<li>\r\n	<p>D3 Teknik Informatika</p>\r\n	</li>\r\n	<li>\r\n	<p>D3 Teknik Komputer</p>\r\n	</li>\r\n	<li>\r\n	<p>D4 Teknik Informatika</p>\r\n	</li>\r\n	<li>\r\n	<p>S1 Teknik Informatika</p>\r\n	</li>\r\n	<li>\r\n	<p>S1 Sistem Informasi</p>\r\n	</li>\r\n	<li>\r\n	<p>S1 Teknik Elektro</p>\r\n	</li>\r\n</ol>\r\n\r\n<p><strong>6 September 2013 - 1 November 2017 :</strong></p>\r\n\r\n<p>Pengangkatan Dr. Arnaldo Marulitua Sinaga, ST., M.InfoTech menjadi Dekan Fakultas Teknik Informatika dan Elektro (FTIE)</p>\r\n\r\n<p><strong>1 November 2017 - 2 November 2020 :</strong></p>\r\n\r\n<p>Pengangkatan Tennov Simanjuntak, S.T, M.Sc menjadi Dekan Fakultas Teknik Informatika dan Elektro (FTIE)</p>\r\n\r\n<p><strong>26 April 2019 :</strong></p>\r\n\r\n<p>Perubahan nama dari Fakultas Teknik Informatika dan Elektro (FITE) menjadi Fakultas Informatika dan Teknik Elektro (FITE)</p>\r\n\r\n<p><strong>26 Agustus 2019 :</strong></p>\r\n\r\n<p>Perubahan 4 nama Program Studi yaitu :</p>\r\n\r\n<ol>\r\n	<li>\r\n	<p>D3 Teknik Informatika menjadi D3 Teknologi Informasi</p>\r\n	</li>\r\n	<li>\r\n	<p>D3 Teknik Komputer menjadi D3 Teknologi Komputer</p>\r\n	</li>\r\n	<li>\r\n	<p>D4 Teknik Infornatika menjadi D4 Teknologi Rekayasa Perangkat Lunak</p>\r\n	</li>\r\n	<li>\r\n	<p>S1 Teknik Informatika menjadi S1 Informatika</p>\r\n	</li>\r\n</ol>\r\n\r\n<p><strong>2 November 2020 - Sekarang :</strong></p>\r\n\r\n<p>Pengangkatan Indra Hartarto Tambunan, Ph.D menjadi Dekan Fakultas Informatika dan Teknik Elektro (FITE)</p>\r\n\r\n<p><strong>Januari 2021 - Sekarang :&nbsp;</strong></p>\r\n\r\n<p>Pengangkatan Togu Novriansyah Turnip, S.S.T., M.IM menjadi Wakil Dekan Fakultas Informatika dan Teknik Elektro (FITE)</p>\r\n\r\n<p>Program Studi Diploma 3 sudah mulai diselenggarakan sejak berdirinya PI Del pada tahun 2021 dan Program Studi Diploma 4 mulai berjalan sejak tahun 2021. Sementara untuk Program Studi Sarjana mulai beroperasi sejak tahun 2014</p>\r\n\r\n<p>&nbsp;</p>', '2021-09-02 20:45:41', '2021-09-14 01:11:49');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tujuan`
--

CREATE TABLE `tujuan` (
  `id` int UNSIGNED NOT NULL,
  `tujuan` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `tujuan`
--

INSERT INTO `tujuan` (`id`, `tujuan`, `created_at`, `updated_at`) VALUES
(1, '<ol>\r\n	<li>Mewujudkan peringkat akreditasi unggul bagi Program Studi-Program Studi di Lingkungan FITE sesuai dengan standar BAN-PT</li>\r\n	<li>Menghasilkan lulusan yang memiliki pengetahuan dan keterampilan dalam bidang Informatika dan Teknik Elketro yang gayut terhadap kebutuhan industri, berjiwa kepemimpinan, serta berkarakter MarTuhan, Marroha, Marbisuk</li>\r\n	<li>Meningatkan kapasitas pendidikan FITE dengan meningkatkan prodi D3 Teknik Komputer menjadi D4 Teknologi Rekayasa Jaringan Komputer serta membuka Prodi Magister</li>\r\n	<li>Membentuk organisasi dan tata kelola penelitian yang berkualitas demi terwujudnya rencana dan peta jalan penelitian yang bertaraf nasional dalam bidang informatika dan teknik elektro</li>\r\n	<li>Melaksanakan dan menghasilkan penelitian yang bereputasi dalam bidang-bidang kajian yang menjadi unggulan Fakultas</li>\r\n	<li>Menghasilkan program-program pengabdian masyarakat dalam bidang informatika dan teknik elektro dalam rangka berkontribusi terhadap penyelesaian masalah-masalah nyata yang dihadapi masyarakat</li>\r\n	<li>Meningkatkan kuantitas dan kulitas kerjasama dengan institusi pemerintahan maupun industri untuk pengembangan maupun penenrapan ilmu pengetahuan dan teknologi dalam bidang informatika dan elektro.</li>\r\n	<li>Meningkatkan kulitas sumber daya manusia yang berdaya saing</li>\r\n	<li>Meningkatkan kulitas sarana dan prasarana akademik untuk mendukung program Tridarma perguruan tinggi dan memampukan peningkatan kapasitas FITE</li>\r\n</ol>', '2021-09-14 00:55:52', '2021-09-14 00:55:52');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tujuan_prodi`
--

CREATE TABLE `tujuan_prodi` (
  `id` int UNSIGNED NOT NULL,
  `tujuan` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_prodi` int UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `tujuan_prodi`
--

INSERT INTO `tujuan_prodi` (`id`, `tujuan`, `id_prodi`, `created_at`, `updated_at`) VALUES
(1, '<ol>\r\n	<li>Menigkatkan peringkat akreditasi program studi menjadi unggul</li>\r\n	<li>Mewujudkan tata kelola program strudi yang berkualitas</li>\r\n	<li>Menghasilkan alumni yang memiliki pengetahuan dan kterampilan dalam bidang informatika yang gayut terhadap kebutuhan industri, berjiwa kepemimpinan, serta berkarakter mar-Tuhan, marroha, dan marbisuk</li>\r\n	<li>Menigkatkan kualitas sumber daya manusia serta sarana dan prasarana akademik</li>\r\n	<li>Menyediakan organisasi serta sarana dan prasarana penelitian yang berkualitas demi terwujudnya rencana dan peta jalan penelitian yang bertaraf nasional dalam bidang informatika</li>\r\n</ol>', 1, '2021-09-01 00:11:22', '2021-09-13 06:06:56'),
(2, '<ol>\r\n	<li>Menghasilkan lulusan yang memiliki kemampuan dan keahlian untuk merancang, membangun dan memelihara Jaringan Komputer dengan mengikuti fase pembangunan jaringan komputer yang benar melalui penerapan metodologi pembangunan sistem dan kakas pendukung serta lulusan yang memiliki keinginan untuk selalu mengembangkan diri dengan meningkatkan keterampilan dan pengetahuan di bidang Jaringan Komputer.</li>\r\n	<li>Menghasilkan penelitian yang berkualitas dan berguna bagi kemajuan dan pengembangan ilmu serta pembelajaran di bidang ilmu teknik komputer.</li>\r\n	<li>Memberikan kontribusi secara langsung dengan membantu memecahkan masalah di masyarakat dalam bidang teknologi informasi dan komunikasi.</li>\r\n	<li>Memberikan kesempatan kepada anak desa yang kurang mampu secara ekonomi namun memiliki kemampuan akademik untuk mengejar cita-citanya di bidang teknologi komputer.</li>\r\n	<li>Menyajikan informasi akademik secara cepat, tepat waktu dan akurat kepada sivitas akademik dan pemangku kepentingan lainnya.</li>\r\n</ol>', 2, '2021-09-02 00:34:01', '2021-09-13 06:45:26'),
(3, '<ol>\r\n	<li>Menghasilkan tenaga profesional yang berperilaku MarTuhan, Marroha, Marbisuk, dengan ciri-ciri utama beriman dan bertaqwa kepada Tuhan Yang Maha Esa, bijak, dan terampil dalam bidang Sistem Informasi, berwawasan luas, bertanggung jawab, dan memiliki sikap kepemimpinan.</li>\r\n	<li>Menghasilkan karya-karya ilmiah di bidang Sistem Informasi yang memberi dampak mensejahterakan masyarakat.</li>\r\n	<li>Menyebarluaskan IPTEK melalui berbagai kegiatan pengabdian kepada masyarakat.</li>\r\n	<li>Mewujudkan sistem tata kelola program studi yang profesional, bermutu, efektif, efisien dan akuntabel.</li>\r\n</ol>', 3, '2021-09-02 00:44:25', '2021-09-13 06:57:08'),
(4, '<ol>\r\n	<li>Menghasilkan lulusan yang mampu menganalisis masalah dan selalu berupaya menyelesaikannya secara logis, sistematis dan praktis yang didukung oleh metode yang tepat guna.</li>\r\n	<li>Menghasilkan lulusan yang menguasai dasar-dasar matematika, fisika, dasar-dasar teknik elektro dan mampu mengembangkan perangkat lunak dan perangkat keras, serta mampu mengembangkan diri terhadap kemajuan teknologi.</li>\r\n	<li>Menghasilkan lulusan yang mampu menciptakan dan mengisi lapangan kerja dalam bidang elektronika, ketenagalistrikan, kendali industri, telekomunikasi, dan jaringan sistem komputer.</li>\r\n	<li>Menghasilkan karya ilmiah dalam bidang teknik elektro yang mampu bersaing secara nasional dan internasional.</li>\r\n	<li>Menghasilkan karya inovatif yang memiliki keunggulan di bidang teknik elektro yang dapat diandalkan dan memperoleh hak intelektual.</li>\r\n	<li>Menghasilkan teknologi tepat guna yang dapat dimanfaatkan oleh masyarakat.</li>\r\n	<li>Menghasilkan sistem penyelenggaraan pendidikan teknik elektro yang efisien.</li>\r\n</ol>', 4, '2021-09-02 00:58:10', '2021-09-02 00:58:10'),
(5, '<p>D4 Rekayasa Perangkat Lunak bertujuan untuk menciptakan capaian belajar dengan sangat baik juga membuat suasana belajar yang aman dan tentram agar setiap mahasiswa yang belajar dapat menjadi mahasiswa yang memiliki etos kerja 3M (MarTuhan, MarRoha, MarBisuk)</p>', 5, '2021-09-16 07:03:22', '2021-09-16 07:03:22'),
(6, '<ol>\r\n	<li>Menigkatkan peringkat akreditasi program studi menjadi unggul</li>\r\n	<li>Mewujudkan tata kelola program strudi yang berkualitas</li>\r\n	<li>Menghasilkan alumni yang memiliki pengetahuan dan kterampilan dalam bidang informatika yang gayut terhadap kebutuhan industri, berjiwa kepemimpinan, serta berkarakter mar-Tuhan, marroha, dan marbisuk</li>\r\n	<li>Menigkatkan kualitas sumber daya manusia serta sarana dan prasarana akademik</li>\r\n	<li>Menyediakan organisasi serta sarana dan prasarana penelitian yang berkualitas demi terwujudnya rencana dan peta jalan penelitian yang bertaraf nasional dalam bidang informatika</li>\r\n</ol>', 6, '2021-09-16 07:04:38', '2021-09-16 07:04:38');

-- --------------------------------------------------------

--
-- Struktur dari tabel `unduhan`
--

CREATE TABLE `unduhan` (
  `id` int UNSIGNED NOT NULL,
  `nama` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pdf` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_kategori` int UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `unduhan`
--

INSERT INTO `unduhan` (`id`, `nama`, `pdf`, `id_kategori`, `created_at`, `updated_at`) VALUES
(1, 'SK Dekan', 'my-cv (5).pdf', 3, '2021-09-04 02:30:47', '2021-09-09 06:42:15'),
(5, 'Sertifikasi Akreditasi', 'my-cv (5).pdf', 5, '2021-09-09 10:11:23', '2021-09-09 10:11:24'),
(6, 'Selebaran', 'my-cv (5).pdf', 6, '2021-09-09 10:13:12', '2021-09-09 10:13:12');

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE `users` (
  `id` bigint UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `role`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Admin', 'admin@admin.com', 'admin', NULL, '$2y$10$awUFuxHc7iWAxxTef7.yuu0GlXLPxRLzSUJb3ir9Gi6gWEw5IpyhG', '0VlSBt5vBjdNmB9Qu5vQGxVePqOOIcKxkBsD0Pg28MM8zLZvkhC8KBSQiinc', NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `visi_misi`
--

CREATE TABLE `visi_misi` (
  `id` int UNSIGNED NOT NULL,
  `visi` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `misi` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `visi_misi`
--

INSERT INTO `visi_misi` (`id`, `visi`, `misi`, `created_at`, `updated_at`) VALUES
(1, '<ol>\r\n	<li>Menjadi Fakultas yang menyelenggrakan program pendidikan yang unggul serta penelitian bertaraf nasional dalam bidang Informatika dan Teknik Elektro pada tahun 2024</li>\r\n</ol>', '<ol>\r\n	<li>Mengembangkan program pendidikan yang unggul dalam bidang Informatika dan Teknik Elektro dangan pengakuan dari Badan Akreditasi Nasional Perguruan Tinggi (BAN-PT).</li>\r\n	<li>Menyelenggrakan penelitian yang bertaraf nasional dalam bidang Informatika dan Teknik Elektro</li>\r\n	<li>Meningkatkan program pengabdian masyarakat melalui kerjasama dengan berbagai institusi pemerintah dan industri di tingkat regional maupun nasioanl dalam bidang kajian yang menjadi unggulan Fakultas.</li>\r\n	<li>Meningkatkan sumber daya Fakultas untuk mewujudkan pendidikan unggul dan institusi pra-riset</li>\r\n</ol>', '2021-08-31 23:56:03', '2021-09-13 05:59:32');

-- --------------------------------------------------------

--
-- Struktur dari tabel `visi_misi_prodi`
--

CREATE TABLE `visi_misi_prodi` (
  `id` int UNSIGNED NOT NULL,
  `visi` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `misi` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_prodi` int UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `visi_misi_prodi`
--

INSERT INTO `visi_misi_prodi` (`id`, `visi`, `misi`, `id_prodi`, `created_at`, `updated_at`) VALUES
(1, '<ol>\r\n	<li>Menjadi program studi unggulan pada program pendidikan dan penelitian terapan di bidang pengembangan teknologi informasi yang bertaraf nasional dan internasional pada tahun 2024</li>\r\n</ol>', '<ol>\r\n	<li>Menyelenggarakan pendidikan vokasi yang unggul untuk menghasilkan sumber daya manusia yang profesional di bidang teknologi informasi dan komunikasi;</li>\r\n	<li>Meningkatkan program penelitian terapan yang inovatif dan bertaraf nasional maupun internasional di bidang teknologi informasi dan komunikasi;</li>\r\n	<li>Meningkatkan program pengabdian masyarakat melalui kerjasama dengan berbagai institusi pemerintahan dan industri di tingkat regional maupun nasional.</li>\r\n</ol>', 1, '2021-09-01 00:11:05', '2021-09-05 17:30:09'),
(2, '<ol>\r\n	<li>Menjadikan Program Studi Teknologi Komputer Program Diploma Tiga sebagai batu loncatan menuju masa depan yang mengintegrasikan dunia pendidikan, penelitian dan pengabdian masyarakat di bidang Teknik Komputer dengan kebutuhan dunia usaha akan tenaga profesional Teknik Komputer yang mampu bersaing secara global.</li>\r\n</ol>', '<ol>\r\n	<li>Menyelenggarakan pengajaran yang mencetak dan membina Sumber Daya Manusia yang mampu bekerja pada industri teknik komputer yang mempunyai sikap pemimpin, penuh kreativitas, berdisiplin tinggi, dan mampu memberikan solusi di bidang teknik komputer dan membuat terobosan dalam dunia pendidikan dengan menciptakan lingkungan pendidikan yang sehat dengan dukungan tenaga pengajar yang ahli dan profesional.</li>\r\n	<li>Menyelenggarakan program penelitian yang menghasilkan produk unggul tepat guna untuk penerapan teknik komputer di berbagai bidang kehidupan masyarakat.</li>\r\n	<li>Menyelenggarakan pengabdian kepada masyarakat di bidang Teknik Komputer melalui berbagai program diseminasi teknologi terhadap masyarakat di berbagai bidang.</li>\r\n	<li>Menciptakan lingkungan civitas akademika dan unit pendukung yang kondusif.&nbsp;</li>\r\n</ol>', 2, '2021-09-02 00:32:53', '2021-09-09 01:48:59'),
(3, '<ol>\r\n	<li>Pada tahun 2024 menjadi Program Studi yang unggul dalam bidang sistem informasi dan analisis data di Indonesia serta turut aktif dalam kegiatan penelitian bertaraf nasional.</li>\r\n</ol>', '<ol>\r\n	<li>Menyelenggarakan pendidikan bermutu dan berorientasi pada kebutuhan industri.</li>\r\n	<li>Melakukan penelitian yang berkontribusi pada kemajuan IPTEK.</li>\r\n	<li>Melakukan pengabdian kepada masyarakat dalam bentuk pendeseminasian dan penerapan IPTEK, pelatihan, dan sertifikasi.</li>\r\n	<li>Menerapkan sistem pengelolaan program studi yang profesional, bermutu, efektif, efisien, dan akuntabel.</li>\r\n</ol>', 3, '2021-09-02 00:44:15', '2021-09-13 06:55:38'),
(4, '<ol>\r\n	<li>Menjadi program studi yang unggul dalam pembelajaran di bidang teknik elektro dengan menghasilkan teknologi tepat guna di daerah rural Indonesia khususnya di Sumatera pada tahun 2025.</li>\r\n</ol>', '<ol>\r\n	<li>Menyelenggarakan pendidikan teknik elektro yang bermutu, profesional dan menjadi rujukan secara nasional dan internasional.</li>\r\n	<li>Menyelenggarakan pendidikan teknik elektro yang mengarah pada penguasaan perangkat lunak dan perangkat keras.</li>\r\n	<li>Menyelenggarakan penelitian yang mendukung kemajuan IPTEK dan bemanfaat bagi pengembangan industri.</li>\r\n	<li>Mengelola PSTE secara efektif dan efisien.</li>\r\n	<li>Melakukan pengabdian kepada masyarakat dalam bidang teknik elektro.</li>\r\n</ol>', 4, '2021-09-02 00:58:03', '2021-09-13 07:48:48'),
(5, '<ol>\r\n	<li>Menjadikan Program Studi Teknologi Rekayasa Perangkat Lunak Program Sarjana Terapan sebagai batu loncatan menuju masa depan Teknologi Informasi yang mengintegrasikan dunia pendidikan dengan kebutuhan usaha akan tenaga profesional informatika yang mampu bersaing secara global.</li>\r\n</ol>', '<ol>\r\n	<li>Menyelenggarakan pendidikan vokasional yang mendidik dan menghasilkan sumber daya manusia yang mempunyai sikap memimpin, penuh kreativitas, berdisiplin tinggi, dan mampu memberikan solusi di bidang Teknologi Informasi secara global</li>\r\n	<li>Membuat terobosan dalam dunia pendidikan dengan menciptakan lingkungan pendidikan yang sehat, kondusif dengan dukungan tenaga pengajar yang ahli dan profesional.</li>\r\n	<li>Menyelenggarakan program penelitian yang menghasilkan produk teknologi informasi yang memberikan solusi tepat guna dan inovatif untuk diterapkan di berbagai bidang kehidupan masyarakat.</li>\r\n	<li>Menyelenggarakan proses pengabdian kepada masyarakat di dunia teknologi informasi melalui berbagai program diseminasi teknologi informasi terhadap masyarakat di berbagai bidang.</li>\r\n</ol>', 5, '2021-09-16 06:55:27', '2021-09-16 06:55:27'),
(6, '<ol>\r\n	<li>Menjadi program Teknik Informatika yang unggul yang berperan dalam menghasilkan dan memanfaatkan teknologi untuk mengembangkan potensi lokal bagi kemajuan bangsa pada tahun 2023</li>\r\n	<li>Menyelenggarakan pendidikan teknik informatika yang bermutu, profesional dan diperhitungkan secara nasional.</li>\r\n	<li>Menyelenggarakan penelitian yang menghasilkan dan memanfaatkan teknologi untuk mengembangkan potensi lokal.</li>\r\n	<li>Melakukan pengabdian kepada masyarakat dalam bidang teknik informatika.</li>\r\n</ol>', '<ol>\r\n	<li>Menyelenggarakan pendidikan teknik informatika yang bermutu, profesional dan diperhitungkan secara nasional.</li>\r\n	<li>Menyelenggarakan penelitian yang menghasilkan dan memanfaatkan teknologi untuk mengembangkan potensi lokal.</li>\r\n	<li>Melakukan pengabdian kepada masyarakat dalam bidang teknik informatika.</li>\r\n</ol>', 6, '2021-09-16 07:06:01', '2021-09-16 07:06:01');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `alumni`
--
ALTER TABLE `alumni`
  ADD PRIMARY KEY (`id`),
  ADD KEY `alumni_id_prodi_foreign` (`id_prodi`);

--
-- Indeks untuk tabel `beasiswa`
--
ALTER TABLE `beasiswa`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `berita_fakultas`
--
ALTER TABLE `berita_fakultas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `berita_fakultas_id_user_foreign` (`id_user`);

--
-- Indeks untuk tabel `capaian_lulusan`
--
ALTER TABLE `capaian_lulusan`
  ADD PRIMARY KEY (`id`),
  ADD KEY `capaian_lulusan_id_prodi_foreign` (`id_prodi`);

--
-- Indeks untuk tabel `data_dosen`
--
ALTER TABLE `data_dosen`
  ADD PRIMARY KEY (`id`),
  ADD KEY `data_dosen_id_prodi_foreign` (`id_prodi`),
  ADD KEY `data_dosen_id_kategori_foreign` (`id_kategori`);

--
-- Indeks untuk tabel `dekan`
--
ALTER TABLE `dekan`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `dosen_prodi`
--
ALTER TABLE `dosen_prodi`
  ADD PRIMARY KEY (`id`),
  ADD KEY `dosen_prodi_id_prodi_foreign` (`id_prodi`);

--
-- Indeks untuk tabel `etika_prodi`
--
ALTER TABLE `etika_prodi`
  ADD PRIMARY KEY (`id`),
  ADD KEY `etika_prodi_id_prodi_foreign` (`id_prodi`);

--
-- Indeks untuk tabel `fasilitas`
--
ALTER TABLE `fasilitas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fasilitas_id_kategori_foreign` (`id_kategori`);

--
-- Indeks untuk tabel `gambar`
--
ALTER TABLE `gambar`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `himapro`
--
ALTER TABLE `himapro`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `info_pmb`
--
ALTER TABLE `info_pmb`
  ADD PRIMARY KEY (`id`),
  ADD KEY `info_pmb_id_usm_foreign` (`id_usm`);

--
-- Indeks untuk tabel `jumlah_lulusan`
--
ALTER TABLE `jumlah_lulusan`
  ADD PRIMARY KEY (`id`),
  ADD KEY `jumlah_lulusan_id_prodi_foreign` (`id_prodi`);

--
-- Indeks untuk tabel `kalender_akademik`
--
ALTER TABLE `kalender_akademik`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `kalender_kemahasiswaan`
--
ALTER TABLE `kalender_kemahasiswaan`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `kategori_dosen`
--
ALTER TABLE `kategori_dosen`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `kategori_fasilitas`
--
ALTER TABLE `kategori_fasilitas`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `kategori_unduhan`
--
ALTER TABLE `kategori_unduhan`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `kategori_usm`
--
ALTER TABLE `kategori_usm`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `kerjasama`
--
ALTER TABLE `kerjasama`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `klub`
--
ALTER TABLE `klub`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `kompetensi`
--
ALTER TABLE `kompetensi`
  ADD PRIMARY KEY (`id`),
  ADD KEY `kompetensi_id_prodi_foreign` (`id_prodi`);

--
-- Indeks untuk tabel `kurikulum`
--
ALTER TABLE `kurikulum`
  ADD PRIMARY KEY (`id`),
  ADD KEY `kurikulum_id_prodi_foreign` (`id_prodi`),
  ADD KEY `kurikulum_id_tahun_foreign` (`id_tahun`);

--
-- Indeks untuk tabel `lokasi_tes`
--
ALTER TABLE `lokasi_tes`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indeks untuk tabel `pendidikan`
--
ALTER TABLE `pendidikan`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `penelitian`
--
ALTER TABLE `penelitian`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `pengabdian`
--
ALTER TABLE `pengabdian`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `prestasi`
--
ALTER TABLE `prestasi`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `profil_lulusan`
--
ALTER TABLE `profil_lulusan`
  ADD PRIMARY KEY (`id`),
  ADD KEY `profil_lulusan_id_prodi_foreign` (`id_prodi`);

--
-- Indeks untuk tabel `program_studi`
--
ALTER TABLE `program_studi`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `semester`
--
ALTER TABLE `semester`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `slider`
--
ALTER TABLE `slider`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `struktur`
--
ALTER TABLE `struktur`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tahun`
--
ALTER TABLE `tahun`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tentang`
--
ALTER TABLE `tentang`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tujuan`
--
ALTER TABLE `tujuan`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tujuan_prodi`
--
ALTER TABLE `tujuan_prodi`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tujuan_prodi_id_prodi_foreign` (`id_prodi`);

--
-- Indeks untuk tabel `unduhan`
--
ALTER TABLE `unduhan`
  ADD PRIMARY KEY (`id`),
  ADD KEY `unduhan_id_kategori_foreign` (`id_kategori`);

--
-- Indeks untuk tabel `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indeks untuk tabel `visi_misi`
--
ALTER TABLE `visi_misi`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `visi_misi_prodi`
--
ALTER TABLE `visi_misi_prodi`
  ADD PRIMARY KEY (`id`),
  ADD KEY `visi_misi_prodi_id_prodi_foreign` (`id_prodi`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `alumni`
--
ALTER TABLE `alumni`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT untuk tabel `beasiswa`
--
ALTER TABLE `beasiswa`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT untuk tabel `berita_fakultas`
--
ALTER TABLE `berita_fakultas`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT untuk tabel `capaian_lulusan`
--
ALTER TABLE `capaian_lulusan`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `data_dosen`
--
ALTER TABLE `data_dosen`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;

--
-- AUTO_INCREMENT untuk tabel `dekan`
--
ALTER TABLE `dekan`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `dosen_prodi`
--
ALTER TABLE `dosen_prodi`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `etika_prodi`
--
ALTER TABLE `etika_prodi`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT untuk tabel `fasilitas`
--
ALTER TABLE `fasilitas`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT untuk tabel `gambar`
--
ALTER TABLE `gambar`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `himapro`
--
ALTER TABLE `himapro`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT untuk tabel `info_pmb`
--
ALTER TABLE `info_pmb`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `jumlah_lulusan`
--
ALTER TABLE `jumlah_lulusan`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT untuk tabel `kalender_akademik`
--
ALTER TABLE `kalender_akademik`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `kalender_kemahasiswaan`
--
ALTER TABLE `kalender_kemahasiswaan`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `kategori_dosen`
--
ALTER TABLE `kategori_dosen`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `kategori_fasilitas`
--
ALTER TABLE `kategori_fasilitas`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT untuk tabel `kategori_unduhan`
--
ALTER TABLE `kategori_unduhan`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT untuk tabel `kategori_usm`
--
ALTER TABLE `kategori_usm`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `kerjasama`
--
ALTER TABLE `kerjasama`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT untuk tabel `klub`
--
ALTER TABLE `klub`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT untuk tabel `kompetensi`
--
ALTER TABLE `kompetensi`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT untuk tabel `kurikulum`
--
ALTER TABLE `kurikulum`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT untuk tabel `lokasi_tes`
--
ALTER TABLE `lokasi_tes`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48;

--
-- AUTO_INCREMENT untuk tabel `pendidikan`
--
ALTER TABLE `pendidikan`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `penelitian`
--
ALTER TABLE `penelitian`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `pengabdian`
--
ALTER TABLE `pengabdian`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `prestasi`
--
ALTER TABLE `prestasi`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=77;

--
-- AUTO_INCREMENT untuk tabel `profil_lulusan`
--
ALTER TABLE `profil_lulusan`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT untuk tabel `program_studi`
--
ALTER TABLE `program_studi`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT untuk tabel `semester`
--
ALTER TABLE `semester`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `slider`
--
ALTER TABLE `slider`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `struktur`
--
ALTER TABLE `struktur`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `tahun`
--
ALTER TABLE `tahun`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `tentang`
--
ALTER TABLE `tentang`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `tujuan`
--
ALTER TABLE `tujuan`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `tujuan_prodi`
--
ALTER TABLE `tujuan_prodi`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT untuk tabel `unduhan`
--
ALTER TABLE `unduhan`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT untuk tabel `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `visi_misi`
--
ALTER TABLE `visi_misi`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `visi_misi_prodi`
--
ALTER TABLE `visi_misi_prodi`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `alumni`
--
ALTER TABLE `alumni`
  ADD CONSTRAINT `alumni_id_prodi_foreign` FOREIGN KEY (`id_prodi`) REFERENCES `program_studi` (`id`);

--
-- Ketidakleluasaan untuk tabel `berita_fakultas`
--
ALTER TABLE `berita_fakultas`
  ADD CONSTRAINT `berita_fakultas_id_user_foreign` FOREIGN KEY (`id_user`) REFERENCES `users` (`id`);

--
-- Ketidakleluasaan untuk tabel `capaian_lulusan`
--
ALTER TABLE `capaian_lulusan`
  ADD CONSTRAINT `capaian_lulusan_id_prodi_foreign` FOREIGN KEY (`id_prodi`) REFERENCES `program_studi` (`id`);

--
-- Ketidakleluasaan untuk tabel `data_dosen`
--
ALTER TABLE `data_dosen`
  ADD CONSTRAINT `data_dosen_id_kategori_foreign` FOREIGN KEY (`id_kategori`) REFERENCES `kategori_dosen` (`id`),
  ADD CONSTRAINT `data_dosen_id_prodi_foreign` FOREIGN KEY (`id_prodi`) REFERENCES `program_studi` (`id`);

--
-- Ketidakleluasaan untuk tabel `dosen_prodi`
--
ALTER TABLE `dosen_prodi`
  ADD CONSTRAINT `dosen_prodi_id_prodi_foreign` FOREIGN KEY (`id_prodi`) REFERENCES `program_studi` (`id`);

--
-- Ketidakleluasaan untuk tabel `etika_prodi`
--
ALTER TABLE `etika_prodi`
  ADD CONSTRAINT `etika_prodi_id_prodi_foreign` FOREIGN KEY (`id_prodi`) REFERENCES `program_studi` (`id`);

--
-- Ketidakleluasaan untuk tabel `fasilitas`
--
ALTER TABLE `fasilitas`
  ADD CONSTRAINT `fasilitas_id_kategori_foreign` FOREIGN KEY (`id_kategori`) REFERENCES `kategori_fasilitas` (`id`);

--
-- Ketidakleluasaan untuk tabel `info_pmb`
--
ALTER TABLE `info_pmb`
  ADD CONSTRAINT `info_pmb_id_usm_foreign` FOREIGN KEY (`id_usm`) REFERENCES `kategori_usm` (`id`);

--
-- Ketidakleluasaan untuk tabel `jumlah_lulusan`
--
ALTER TABLE `jumlah_lulusan`
  ADD CONSTRAINT `jumlah_lulusan_id_prodi_foreign` FOREIGN KEY (`id_prodi`) REFERENCES `program_studi` (`id`);

--
-- Ketidakleluasaan untuk tabel `kompetensi`
--
ALTER TABLE `kompetensi`
  ADD CONSTRAINT `kompetensi_id_prodi_foreign` FOREIGN KEY (`id_prodi`) REFERENCES `program_studi` (`id`);

--
-- Ketidakleluasaan untuk tabel `kurikulum`
--
ALTER TABLE `kurikulum`
  ADD CONSTRAINT `kurikulum_id_prodi_foreign` FOREIGN KEY (`id_prodi`) REFERENCES `program_studi` (`id`),
  ADD CONSTRAINT `kurikulum_id_tahun_foreign` FOREIGN KEY (`id_tahun`) REFERENCES `tahun` (`id`);

--
-- Ketidakleluasaan untuk tabel `profil_lulusan`
--
ALTER TABLE `profil_lulusan`
  ADD CONSTRAINT `profil_lulusan_id_prodi_foreign` FOREIGN KEY (`id_prodi`) REFERENCES `program_studi` (`id`);

--
-- Ketidakleluasaan untuk tabel `tujuan_prodi`
--
ALTER TABLE `tujuan_prodi`
  ADD CONSTRAINT `tujuan_prodi_id_prodi_foreign` FOREIGN KEY (`id_prodi`) REFERENCES `program_studi` (`id`);

--
-- Ketidakleluasaan untuk tabel `unduhan`
--
ALTER TABLE `unduhan`
  ADD CONSTRAINT `unduhan_id_kategori_foreign` FOREIGN KEY (`id_kategori`) REFERENCES `kategori_unduhan` (`id`);

--
-- Ketidakleluasaan untuk tabel `visi_misi_prodi`
--
ALTER TABLE `visi_misi_prodi`
  ADD CONSTRAINT `visi_misi_prodi_id_prodi_foreign` FOREIGN KEY (`id_prodi`) REFERENCES `program_studi` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
