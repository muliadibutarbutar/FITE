<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route Dashboard Admin
Route::get('/dashboard', function () {
    return view('dashboard');
});

//Route Authentikasi
Route::get('/login', 'AuthController@index');
Route::post('/postlogin', 'AuthController@postlogin');
Route::get('/logout', 'AuthController@logout');

//Route Group Tentang
Route::group(['prefix' => 'admin-tentang'], function(){

    //Route Dekan Fakultas
    Route::get('/dekan','DekanController@index');
    Route::get('/dekan/create','DekanController@create');
    Route::post('/dekan/add','DekanController@add');
    Route::get('/dekan/edit/{id}','DekanController@edit');
    Route::post('/dekan/update/{id}','DekanController@update');
    Route::get('/dekan/hapus/{id}','DekanController@hapus');

    //Route Tentang Fakultas
    Route::get('/tentang', 'TentangController@index');
    Route::get('/tentang/create', 'TentangController@create');
    Route::post('/tentang/add', 'TentangController@add');
    Route::get('/tentang/edit/{id}', 'TentangController@edit');
    Route::post('/tentang/update/{id}', 'TentangController@update');

    //Route Visi Misi Fakultas
    Route::get('/visimisifakultas', 'VisiMisiController@index');
    Route::get('/visimisifakultas/create', 'VisiMisiController@create');
    Route::post('/visimisifakultas/add', 'VisiMisiController@add');
    Route::get('/visimisifakultas/edit/{id}', 'VisiMisiController@edit');
    Route::post('/visimisifakultas/update/{id}', 'VisiMisiController@update');

    //Route Struktur Data
    Route::get('/struktur','StrukturController@index');
    Route::get('/struktur/create','StrukturController@create');
    Route::post('/struktur/add','StrukturController@add');
    Route::get('/struktur/edit/{id}','StrukturController@edit');
    Route::post('/struktur/update/{id}','StrukturController@update');
    Route::get('/struktur/hapus/{id}','StrukturController@hapus');

    //Route Tujuan
    Route::get('/tujuan','TujuanController@index');
    Route::get('/tujuan/create','TujuanController@create');
    Route::post('/tujuan/add','TujuanController@add');
    Route::get('/tujuan/edit/{id}','TujuanController@edit');
    Route::post('/tujuan/update/{id}','TujuanController@update');
    Route::get('/tujuan/hapus/{id}','TujuanController@hapus');
});

//Route Group Program Studi
Route::group(['prefix' => 'programstudi'], function(){

    //Route Program Studi Fakultas
    Route::get('/programstudi', 'ProgramStudiController@index');
    Route::get('/programstudi/create', 'ProgramStudiController@create');
    Route::post('/programstudi/add', 'ProgramStudiController@add');
    Route::get('/programstudi/edit/{id}', 'ProgramStudiController@edit');
    Route::post('/programstudi/update/{id}', 'ProgramStudiController@update');
    Route::get('/programstudi/hapus/{id}', 'ProgramStudiController@hapus');
    Route::get('/programstudi/show/{id}', 'ProgramStudiController@show');

    //Route Visi Misi Prodi
    Route::get('/visimisiprodi/{id}/create', 'VisiMisiProdiController@create');
    Route::post('/visimisiprodi/add', 'VisiMisiProdiController@add');
    Route::get('/visimisiprodi/{id}/edit', 'VisiMisiProdiController@edit');
    Route::post('/visimisiprodi/update/{id}', 'VisiMisiProdiController@update');

    //Route Tujuan Prodi
    Route::get('/tujuanprodi/{id}/create', 'TujuanProdiController@create');
    Route::post('/tujuanprodi/add', 'TujuanProdiController@add');
    Route::get('/visimisifakultas/{id}/edit', 'TujuanProdiController@edit');
    Route::post('/tujuanprodi/update/{id}', 'TujuanProdiController@update');

    //Route Kompetensi Prodi
    Route::get('/kompetensiprodi/{id}/create', 'KompetensiProdiController@create');
    Route::post('/kompetensiprodi/add', 'KompetensiProdiController@add');
    Route::get('/kompetensiprodi/{id}/edit', 'KompetensiProdiController@edit');
    Route::post('/kompetensiprodi/update/{id}', 'KompetensiProdiController@update');

    //Route Etika Prodi
    Route::get('/etikaprodi/{id}/create', 'EtikaProdiController@create');
    Route::post('/etikaprodi/add', 'EtikaProdiController@add');
    Route::get('/etikaprodi/{id}/edit', 'EtikaProdiController@edit');
    Route::post('/etikaprodi/update/{id}', 'EtikaProdiController@update');

    //Route Profil Lulusan
    Route::get('/profillulusan/{id}/create', 'ProfilLulusanController@create');
    Route::post('/profillulusan/add', 'ProfilLulusanController@add');
    Route::get('/profillulusan/{id}/edit', 'ProfilLulusanController@edit');
    Route::post('/profillulusan/update/{id}', 'ProfilLulusanController@update');
    Route::get('/profillulusan/{id}/hapus', 'ProfilLulusanController@hapus');
    Route::get('/profillulusan/{id}/show', 'ProfilLulusanController@show');

});

//Route Group Alumni
Route::group(['prefix' => 'alumni'], function(){

    //Route Jumlah Lulusan
    Route::get('/jumlahlulusan', 'JumlahLulusanController@index');
    Route::get('/jumlahlulusan/create', 'JumlahLulusanController@create');
    Route::post('/jumlahlulusan/add', 'JumlahLulusanController@add');
    Route::get('/jumlahlulusan/edit/{id}', 'JumlahLulusanController@edit');
    Route::post('/jumlahlulusan/update/{id}', 'JumlahLulusanController@update');
    Route::get('/jumlahlulusan/hapus/{id}', 'JumlahLulusanController@hapus');

    //Route Alumni
    Route::get('/alumni', 'AlumniController@index');
    Route::get('/alumni/create', 'AlumniController@create');
    Route::post('/alumni/add', 'AlumniController@add');
    Route::get('/alumni/edit/{id}', 'AlumniController@edit');
    Route::post('/alumni/update/{id}', 'AlumniController@update');
    Route::get('/alumni/hapus/{id}', 'AlumniController@hapus');

});

//Route Group Fasilitas
Route::group(['prefix' => 'fasilitas'], function(){

    //Route Fasilitas
    Route::get('/fasilitas','FasilitasController@index');
    Route::get('/fasilitas/create','FasilitasController@create');
    Route::post('/fasilitas/add', 'FasilitasController@add');
    Route::get('/fasilitas/edit/{id}', 'FasilitasController@edit');
    Route::post('/fasilitas/update/{id}', 'FasilitasController@update');
    Route::get('/fasilitas/hapus/{id}', 'FasilitasController@hapus');

    //Route Kategori Fasilitas
    Route::get('/kategorifasilitas','KategoriFasilitasController@index');
    Route::get('/kategorifasilitas/create','KategoriFasilitasController@create');
    Route::post('/kategorifasilitas/add','KategoriFasilitasController@add');
    Route::get('/kategorifasilitas/edit/{id}','KategoriFasilitasController@edit');
    Route::post('/kategorifasilitas/update/{id}','KategoriFasilitasController@update');
    Route::get('/kategorifasilitas/hapus/{id}','KategoriFasilitasController@hapus');

});

//Route Group Unduhan
Route::group(['prefix' => 'unduhan'], function(){

    //Route Unduhan
    Route::get('/unduhan', 'UnduhanController@index');
    Route::get('/unduhan/create', 'UnduhanController@create');
    Route::post('/unduhan/add', 'UnduhanController@add');
    Route::get('/unduhan/edit/{id}', 'UnduhanController@edit');
    Route::post('/unduhan/update/{id}', 'UnduhanController@update');
    Route::get('/unduhan/hapus/{id}', 'UnduhanController@hapus');

    //Route Kategori Unduhan
    Route::get('/kategoriunduhan', 'KategoriUnduhanController@index');
    Route::get('/kategoriunduhan/create', 'KategoriUnduhanController@create');
    Route::post('/kategoriunduhan/add', 'KategoriUnduhanController@add');
    Route::get('/kategoriunduhan/edit/{id}', 'KategoriUnduhanController@edit');
    Route::post('/kategoriunduhan/update/{id}', 'KategoriUnduhanController@update');
    Route::get('/kategoriunduhan/hapus/{id}', 'KategoriUnduhanController@hapus');

});

//Route Group Unduhan
Route::group(['prefix' => 'infopmb'], function(){

    //Route Info PMB
    Route::get('/infopmb', 'InfoPmbController@index');
    Route::get('/infopmb/create', 'InfoPmbController@create');
    Route::post('/infopmb/add', 'InfoPmbController@add');
    Route::get('/infopmb/edit/{id}', 'InfoPmbController@edit');
    Route::post('/infopmb/update/{id}', 'InfoPmbController@update');
    Route::get('/infopmb/hapus/{id}', 'InfoPmbController@hapus');

    //Route Kategori USM
    Route::get('/kategoriusm', 'KategoriPendafataranController@index');
    Route::get('/kategoriusm/create', 'KategoriPendafataranController@create');
    Route::post('/kategoriusm/add', 'KategoriPendafataranController@add');
    Route::get('/kategoriusm/edit/{id}', 'KategoriPendafataranController@edit');
    Route::post('/kategoriusm/update/{id}', 'KategoriPendafataranController@update');
    Route::get('/kategoriusm/hapus/{id}', 'KategoriPendafataranController@hapus');

    //Route Lokasi USM
    Route::get('/lokasi','LokasiTesController@index');
    Route::get('/lokasi/create','LokasiTesController@create');
    Route::post('/lokasi/add','LokasiTesController@add');
    Route::get('/lokasi/edit/{id}','LokasiTesController@edit');
    Route::post('/lokasi/update/{id}','LokasiTesController@update');
    Route::get('/lokasi/hapus/{id}','LokasiTesController@hapus');

});

//Route Group Kemahasiswaan
Route::group(['prefix' => 'kemahasiswaan'], function(){

    //Route Himapro
    Route::get('himapro', 'HimaproController@index');
    Route::get('himapro/create', 'HimaproController@create');
    Route::post('himapro/add', 'HimaproController@add');
    Route::get('himapro/edit/{id}', 'HimaproController@edit');
    Route::post('himapro/update/{id}', 'HimaproController@update');
    Route::get('himapro/hapus/{id}', 'HimaproController@hapus');

    //Route Klub
    Route::get('/klub','KlubController@index');
    Route::get('/klub/create','KlubController@create');
    Route::post('/klub/add','KlubController@add');
    Route::get('/klub/edit/{id}','KlubController@edit');
    Route::post('/klub/update/{id}','KlubController@update');
    Route::get('/klub/hapus/{id}','KlubController@hapus');

});

//Route Group Akademik
Route::group(['prefix' => 'akademik'], function(){

    //Route Kalender Akademik
    Route::get('/kalender', 'KalenderAkademdemikController@index');
    Route::get('/kalender/create', 'KalenderAkademdemikController@create');
    Route::post('/kalender/add', 'KalenderAkademdemikController@add');
    Route::get('/kalender/edit/{id}', 'KalenderAkademdemikController@edit');
    Route::post('/kalender/update/{id}', 'KalenderAkademdemikController@update');
    Route::get('/kalender/hapus/{id}', 'KalenderAkademdemikController@hapus');

    //Route Kurikulum
    Route::get('/kurikulum','KurikulumController@index');
    Route::get('/kurikulum/create','KurikulumController@create');
    Route::post('/kurikulum/add','KurikulumController@add');
    Route::get('/kurikulum/edit/{id}','KurikulumController@edit');
    Route::post('/kurikulum/update/{id}','KurikulumController@update');
    Route::get('/kurikulum/hapus/{id}','KurikulumController@hapus');

    //Route Beasiswa
    Route::get('/beasiswa', 'BeasiswaController@index');
    Route::get('/beasiswa/create', 'BeasiswaController@create');
    Route::post('/beasiswa/add', 'BeasiswaController@add');
    Route::get('/beasiswa/edit/{id}', 'BeasiswaController@edit');
    Route::post('/beasiswa/update/{id}', 'BeasiswaController@update');
    Route::get('/beasiswa/hapus/{id}', 'BeasiswaController@hapus');

    //Route Kalender Kemahasiswaan
    Route::get('/kalenderkemahasiswaan', 'KalenderKemahasiswaanController@index');
    Route::get('/kalenderkemahasiswaan/create', 'KalenderKemahasiswaanController@create');
    Route::post('/kalenderkemahasiswaan/add', 'KalenderKemahasiswaanController@add');
    Route::get('/kalenderkemahasiswaan/edit/{id}', 'KalenderKemahasiswaanController@edit');
    Route::post('/kalenderkemahasiswaan/update/{id}', 'KalenderKemahasiswaanController@update');
    Route::get('/kalenderkemahasiswaan/hapus/{id}', 'KalenderKemahasiswaanController@hapus');

});

//Route Group Tri Dharma
Route::group(['prefix' => 'tridharma'], function(){

    //Route Pendidikan
    Route::get('/pendidikan','PendidikanController@index');
    Route::get('/pendidikan/create','PendidikanController@create');
    Route::post('/pendidikan/add','PendidikanController@add');
    Route::get('/pendidikan/edit/{id}','PendidikanController@edit');
    Route::post('/pendidikan/update/{id}','PendidikanController@update');
    Route::get('/pendidikan/hapus/{id}','PendidikanController@hapus');

    //Route Penelitian
    Route::get('/penelitian','PenelitianController@index');
    Route::get('/penelitian/create','PenelitianController@create');
    Route::post('/penelitian/add','PenelitianController@add');
    Route::get('/penelitian/edit/{id}','PenelitianController@edit');
    Route::post('/penelitian/update/{id}','PenelitianController@update');
    Route::get('/penelitian/hapus/{id}','PenelitianController@hapus');

    //Route Pengabdian Masyarakat
    Route::get('/pengabdian','PengabdianController@index');
    Route::get('/pengabdian/create','PengabdianController@create');
    Route::post('/pengabdian/add','PengabdianController@add');
    Route::get('/pengabdian/edit/{id}','PengabdianController@edit');
    Route::post('/pengabdian/update/{id}','PengabdianController@update');
    Route::get('/pengabdian/hapus/{id}','PengabdianController@hapus');

});

//Route Group Data Dosen
Route::group(['prefix' => 'data-dosen'], function(){

    //Route Data Dosen
    Route::get('/data-dosen','DataDosenController@index');
    Route::get('/data-dosen/create','DataDosenController@create');
    Route::post('/data-dosen/add','DataDosenController@add');
    Route::get('/data-dosen/edit/{id}','DataDosenController@edit');
    Route::post('/data-dosen/update/{id}','DataDosenController@update');
    Route::get('/data-dosen/hapus/{id}','DataDosenController@hapus');

    //Route Kategori Dosen
    Route::get('/kategoridosen','KategoriDosenController@index');
    Route::get('/kategoridosen/create','KategoriDosenController@create');
    Route::post('/kategoridosen/add','KategoriDosenController@add');
    Route::get('/kategoridosen/edit/{id}','KategoriDosenController@edit');
    Route::post('/kategoridosen/update/{id}','KategoriDosenController@update');
    Route::get('/kategoridosen/hapus/{id}','KategoriDosenController@hapus');

});

//Route User
Route::get('/user','UserController@index');
Route::get('/user/create','UserController@create');
Route::post('/user/add','UserController@add');
Route::get('/user/edit/{id}','UserController@edit');
Route::post('/user/update/{id}','UserController@update');
Route::get('/user/hapus/{id}','UserController@hapus');

//Route Berita
Route::get('/beritafakultas','BeritaFakultasController@index');
Route::get('/beritafakultas/create','BeritaFakultasController@create');
Route::post('/beritafakultas/add','BeritaFakultasController@add');
Route::get('/beritafakultas/edit/{id}','BeritaFakultasController@edit');
Route::post('/beritafakultas/update/{id}','BeritaFakultasController@update');
Route::get('/beritafakultas/hapus/{id}','BeritaFakultasController@hapus');
Route::get('/beritafakultas/show/{id}','BeritaFakultasController@show');

//Route Prestasi
Route::get('/prestasi', 'PrestasiController@index');
Route::get('/prestasi/create', 'PrestasiController@create');
Route::post('/prestasi/add', 'PrestasiController@add');
Route::get('/prestasi/edit/{id}', 'PrestasiController@edit');
Route::post('/prestasi/update/{id}', 'PrestasiController@update');
Route::get('/prestasi/hapus/{id}', 'PrestasiController@hapus');

//Route Kerjasama
Route::get('/kerjasama','KerjasamaController@index');
Route::get('/kerjasama/create','KerjasamaController@create');
Route::post('/kerjasama/add','KerjasamaController@add');
Route::get('/kerjasama/edit/{id}','KerjasamaController@edit');
Route::post('/kerjasama/update/{id}','KerjasamaController@update');
Route::get('/kerjasama/hapus/{id}','KerjasamaController@hapus');

//Route Gambar
Route::get('/gambar','GambarController@index');
Route::get('/gambar/create','GambarController@create');
Route::post('/gambar/add','GambarController@add');
Route::get('/gambar/edit/{id}','GambarController@edit');
Route::post('/gambar/update/{id}','GambarController@update');
Route::get('/gambar/hapus/{id}','GambarController@hapus');

//Route Slider
Route::get('/slider','SliderController@index');
Route::get('/slider/create','SliderController@create');
Route::post('/slider/add','SliderController@add');
Route::get('/slider/edit/{id}','SliderController@edit');
Route::post('/slider/update/{id}','SliderController@update');
Route::get('/slider/hapus/{id}','SliderController@hapus');


//Route Guest User

//Route Dashboard Guest User
Route::get('/', 'DashboardController@index');

//Route Tentang Guest User
Route::get('/tentang', 'TentangController@index2');

//Route Program Studi Guest User
Route::get('/programstudi/{id}', 'ProgramStudiController@index2');

//Route Fasilitas Guest User
Route::get('/fasilitas/{id}', 'FasilitasController@index2');

//Route Berita Guest User
Route::get('/berita', 'BeritaFakultasController@index2');
Route::get('/berita/{id}', 'BeritaFakultasController@show2');

//Route Unduhan Guest User
Route::get('/unduhanGuest', 'UnduhanController@index2');

//Route Alumni Guest User
Route::get('/alumniGuest', 'AlumniController@index2');
Route::get('/alumniProdi/{id}', 'AlumniController@show2');

//Route Info PMB Guest User
Route::get('/infopmbGuest', 'InfoPmbController@index2');

//Route Prestasi
Route::get('/prestasiGuest', 'PrestasiController@index2');

//Route Akademik
//Route Kalender Akademik
Route::get('/kalenderAkademik', 'KalenderAkademdemikController@index2');

//Route Kurikulum
Route::get('/kurikulumGuest', 'KurikulumController@index2');
Route::get('/kurikulumGuest/show/{id}', 'KurikulumController@show2');

//Route Kemahasiswaan Guest User
//Route Himapro
Route::get('/himaproGuest', 'HimaproController@index2');

//Route Klub
Route::get('/klubGuest', 'KlubController@index2');

//Route Tri Dharma Guest User
//Route Pendidikan
Route::get('/pendidikanGuest','PendidikanController@index2');

//Route Penelitian
Route::get('/penelitianGuest','PenelitianController@index2');

//Route Pengabdian
Route::get('/pengabdianGuest','PengabdianController@index2');

//Route Beasiswa User Guest
Route::get('/beasiswaGuest', 'BeasiswaController@index2');

//Route Kalender Kemahasiswaan
Route::get('/kalenderKemahasiswaan','KalenderKemahasiswaanController@index2');