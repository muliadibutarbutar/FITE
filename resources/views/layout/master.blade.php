<!DOCTYPE html>
<html>
<head>
  <title>FITE IT DEL</title>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  
  <!-- CSRF Token -->
  <meta name="_token" content="{{ csrf_token() }}">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.css">
  <link rel="stylesheet" href="https://cdn.datatables.net/1.11.0/css/dataTables.bootstrap4.min.css">
  <link rel="shortcut icon" href="{{ url('assets/images/del.png') }}">

  <!-- plugin css -->
  {!! Html::style('assets/plugins/@mdi/font/css/materialdesignicons.min.css') !!}
  {!! Html::style('assets/plugins/perfect-scrollbar/perfect-scrollbar.css') !!}
  <!-- end plugin css -->

  @stack('plugin-styles')

  <!-- common css -->
  {!! Html::style('css/app.css') !!}
  <!-- end common css -->

  @stack('style')
  <style>
* {
  box-sizing: border-box;
}

.zoom {
  transition: transform .2s;
  width: 200px;
  height: 200px;
}

.zoom:hover {
  -ms-transform: scale(1.5); /* IE 9 */
  -webkit-transform: scale(1.5); /* Safari 3-8 */
  transform: scale(1.5); 
}
</style>
</head>
<body data-base-url="{{url('/')}}">

  <div class="container-scroller" id="app">
    @include('layout.header')
    <div class="container-fluid page-body-wrapper">
      @include('layout.sidebar')
      <div class="main-panel">
        <div class="content-wrapper">
          @yield('content')
        </div>
        @include('layout.footer')
      </div>
    </div>
  </div>

  <!-- base js -->
  {!! Html::script('js/app.js') !!}
  {!! Html::script('assets/plugins/perfect-scrollbar/perfect-scrollbar.min.js') !!}
  <!-- end base js -->

  <!-- plugin js -->
  @stack('plugin-scripts')
  <!-- end plugin js -->

  <!-- common js -->
  {!! Html::script('assets/js/off-canvas.js') !!}
  {!! Html::script('assets/js/hoverable-collapse.js') !!}
  {!! Html::script('assets/js/misc.js') !!}
  {!! Html::script('assets/js/settings.js') !!}
  {!! Html::script('assets/js/todolist.js') !!}
  <!-- end common js -->

  <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
  <script src="https://cdn.datatables.net/1.11.0/js/jquery.dataTables.min.js"></script>
  <script src="https://cdn.datatables.net/1.11.0/js/dataTables.bootstrap4.min.js"></script>
  <script src="https://cdn.ckeditor.com/4.16.2/standard/ckeditor.js"></script>
  <script type="text/javascript">
                $(document).on('click', '.pilih_kategori', function (e) {
                    document.getElementById("kategori_id").value = $(this).attr('data-kategori_id');
                    document.getElementById("kategori_nama").value = $(this).attr('data-kategori_nama');
                    $('#myModal').close();
                });

                $(document).on('click', '.pilih_kategori_dosen', function (e) {
                    document.getElementById("kategori_id_dosen").value = $(this).attr('data-kategori_id_dosen');
                    document.getElementById("kategori_nama_dosen").value = $(this).attr('data-kategori_nama_dosen');
                    $('#myModal').close();
                });

                $(document).on('click', '.pilih_tahun', function (e) {
                    document.getElementById("tahun_id").value = $(this).attr('data-tahun_id');
                    document.getElementById("tahun_nama").value = $(this).attr('data-tahun_nama');
                    $('#myModal2').close();
                });

    </script>

  <script>
    
    $(document).ready(function() {
      $('#tableMaster').DataTable();
    });



    CKEDITOR.replace( 'visi' );
  </script>
  <script type="text/javascript">
    $(document).ready( function() {
  $('#test').dataTable( {
    "oSearch": {"sSearch": "2021"},
  } );
} )
  </script>
    <script>
    CKEDITOR.replace( 'tentang' );
  </script>
  <script>
    CKEDITOR.replace( 'aktifitas' );
  </script>
  <script>
    CKEDITOR.replace( 'kompetensi' );
  </script>
  <script>
    CKEDITOR.replace( 'riwayatpendidikan' );
  </script>
    <script>
    CKEDITOR.replace( 'etika_prodi' );
  </script>
    <script>
    CKEDITOR.replace( 'tujuan' );
  </script>
  <script>
    CKEDITOR.replace( 'misi' );
  </script>
  <script>
    CKEDITOR.replace( 'prestasi' );
  </script>
  <script>
    CKEDITOR.replace('deskripsi');
  </script>
  @stack('custom-scripts')
  <script type="text/javascript">
        function readURL() {
            var input = this;
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $(input).prev().attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }

        $(function () {
            $(".uploads").change(readURL)
            $("#f").submit(function(){
                // do ajax submit or just classic form submit
              //  alert("fake subminting")
                return false
            })
        })


var check = function() {
  if (document.getElementById('password').value ==
    document.getElementById('confirm_password').value) {
    document.getElementById('submit').disabled = false;
    document.getElementById('message').style.color = 'green';
    document.getElementById('message').innerHTML = 'Cocok';
  } else {
    document.getElementById('submit').disabled = true;
    document.getElementById('message').style.color = 'red';
    document.getElementById('message').innerHTML = 'Tidak Cocok';
  }
}
    </script>
</body>
</html>