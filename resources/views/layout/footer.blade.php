<footer class="footer">
  <div class="container-fluid clearfix">
    <span class="text-muted d-block text-center text-sm-left d-sm-inline-block">FAKULTAS INFORMATIKA DAN TEKNIK ELEKTRO {{ now()->year }}</span>
    <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center"><a href="//www.del.ac.id">INSTITUT TEKNOLOGI DEL</a>
    </span>
  </div>
</footer>