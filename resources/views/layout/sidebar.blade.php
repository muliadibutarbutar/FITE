<nav class="sidebar sidebar-offcanvas dynamic-active-class-disabled" id="sidebar">
  <ul class="nav">
    <li class="nav-item {{ active_class(['dashboard']) }}">
      <a class="nav-link" href="{{ url('dashboard') }}">
        <i class="menu-icon mdi mdi-television"></i>
        <span class="menu-title">Dashboard</span>
      </a>
    </li>
    <li class="nav-item {{ active_class(['admin-tentang/*']) }}">
      <a class="nav-link" data-toggle="collapse" href="#admin-tentang" aria-expanded="{{ is_active_route(['admin-tentang/*']) }}" aria-controls="test">
        <i class="menu-icon mdi mdi-notification-clear-all"></i>
        <span class="menu-title">Tentang</span>
        <i class="menu-arrow"></i>
      </a>
      <div class="collapse {{ show_class(['admin-tentang/*']) }}" id="admin-tentang">
        <ul class="nav flex-column sub-menu">
          <li class="nav-item {{ active_class(['admin-tentang/tentang']) }}">
            <a class="nav-link" href="{{ url('/admin-tentang/tentang') }}">Sejarah</a>
          </li>
          <li class="nav-item {{ active_class(['admin-tentang/visimisifakultas']) }}">
            <a class="nav-link" href="{{ url('/admin-tentang/visimisifakultas') }}">Visi Misi</a>
          </li>
          <li class="nav-item {{ active_class(['admin-tentang/tujuan']) }}">
            <a class="nav-link" href="{{ url('/admin-tentang/tujuan') }}">Tujuan</a>
          </li>
          <li class="nav-item {{ active_class(['admin-tentang/struktur']) }}">
            <a class="nav-link" href="{{ url('/admin-tentang/struktur') }}">Struktur</a>
          </li>
          <li class="nav-item {{ active_class(['admin-tentang/dekan']) }}">
            <a class="nav-link" href="{{ url('/admin-tentang/dekan') }}">Dekan</a>
          </li>
        </ul>
      </div>
    </li>
    <li class="nav-item {{ active_class(['kemahasiswaan/*']) }}">
      <a class="nav-link" data-toggle="collapse" href="#kemahasiswaan" aria-expanded="{{ is_active_route(['kemahasiswaan/*']) }}" aria-controls="kemahasiswaan">
        <i class="menu-icon mdi mdi-window-shutter"></i>
        <span class="menu-title">Kemahasiswaan</span>
        <i class="menu-arrow"></i>
      </a>
      <div class="collapse {{ show_class(['kemahasiswaan/*']) }}" id="kemahasiswaan">
        <ul class="nav flex-column sub-menu">
          <li class="nav-item {{ active_class(['kemahasiswaan/himapro']) }}">
            <a class="nav-link" href="{{ url('/kemahasiswaan/himapro') }}">Himapro</a>
          </li>
          <li class="nav-item {{ active_class(['kemahasiswaan/klub']) }}">
            <a class="nav-link" href="{{ url('/kemahasiswaan/klub') }}">Klub</a>
          </li>
        </ul>
      </div>
    </li>
    <li class="nav-item {{ active_class(['programstudi/*']) }}">
      <a class="nav-link" data-toggle="collapse" href="#programstudi" aria-expanded="{{ is_active_route(['programstudi/*']) }}" aria-controls="programstudi">
        <i class="menu-icon mdi mdi-layers-triple-outline"></i>
        <span class="menu-title">Program Studi</span>
        <i class="menu-arrow"></i>
      </a>
      <div class="collapse {{ show_class(['programstudi/*']) }}" id="programstudi">
        <ul class="nav flex-column sub-menu">
          <li class="nav-item {{ active_class(['programstudi/programstudi']) }}">
            <a class="nav-link" href="{{ url('/programstudi/programstudi') }}">Daftar Program Studi</a>
          </li>
        </ul>
      </div>
    </li>
    <li class="nav-item {{ active_class(['alumni/*']) }}">
      <a class="nav-link" data-toggle="collapse" href="#alumni" aria-expanded="{{ is_active_route(['alumni/*']) }}" aria-controls="alumni">
        <i class="menu-icon mdi mdi mdi-library"></i>
        <span class="menu-title">Alumni</span>
        <i class="menu-arrow"></i>
      </a>
      <div class="collapse {{ show_class(['alumni/*']) }}" id="alumni">
        <ul class="nav flex-column sub-menu">
          <li class="nav-item {{ active_class(['alumni/alumni']) }}">
            <a class="nav-link" href="{{ url('/alumni/alumni') }}">Daftar Alumni</a>
          </li>
          <li class="nav-item {{ active_class(['alumni/jumlahlulusan']) }}">
            <a class="nav-link" href="{{ url('/alumni/jumlahlulusan') }}">Jumlah Lulusan</a>
          </li>
        </ul>
      </div>
    </li>
    <li class="nav-item {{ active_class(['fasilitas/*']) }}">
      <a class="nav-link" data-toggle="collapse" href="#fasilitas" aria-expanded="{{ is_active_route(['fasilitas/*']) }}" aria-controls="fasilitas">
        <i class="menu-icon mdi mdi mdi-format-list-checkbox"></i>
        <span class="menu-title">Fasilitas</span>
        <i class="menu-arrow"></i>
      </a>
      <div class="collapse {{ show_class(['fasilitas/*']) }}" id="fasilitas">
        <ul class="nav flex-column sub-menu">
          <li class="nav-item {{ active_class(['fasilitas/fasilitas']) }}">
            <a class="nav-link" href="{{ url('/fasilitas/fasilitas') }}">Daftar Fasilitas</a>
          </li>
          <li class="nav-item {{ active_class(['fasilitas/kategorifasilitas']) }}">
            <a class="nav-link" href="{{ url('/fasilitas/kategorifasilitas') }}">Kategori Fasilitas</a>
          </li>
        </ul>
      </div>
    </li>
    <li class="nav-item {{ active_class(['akademik/*']) }}">
      <a class="nav-link" data-toggle="collapse" href="#akademik" aria-expanded="{{ is_active_route(['akademik/*']) }}" aria-controls="akademik">
        <i class="menu-icon mdi mdi-delta"></i>
        <span class="menu-title">Akademik</span>
        <i class="menu-arrow"></i>
      </a>
      <div class="collapse {{ show_class(['akademik/*']) }}" id="akademik">
        <ul class="nav flex-column sub-menu">
          <li class="nav-item {{ active_class(['akademik/kurikulum']) }}">
            <a class="nav-link" href="{{ url('/akademik/kurikulum') }}">Kurikulum</a>
          </li>
          <li class="nav-item {{ active_class(['akademik/beasiswa']) }}">
            <a class="nav-link" href="{{ url('/akademik/beasiswa') }}">Beasiswa</a>
          </li>
          <li class="nav-item {{ active_class(['akademik/kalender']) }}">
            <a class="nav-link" href="{{ url('/akademik/kalender') }}">Kalender Akademik</a>
          </li>
          <li class="nav-item {{ active_class(['akademik/kalenderkemahasiswaan']) }}">
            <a class="nav-link" href="{{ url('/akademik/kalenderkemahasiswaan') }}">Kalender Kemahasiswaan</a>
          </li>
        </ul>
      </div>
    </li>
    <li class="nav-item {{ active_class(['tridharma/*']) }}">
      <a class="nav-link" data-toggle="collapse" href="#tridharma" aria-expanded="{{ is_active_route(['tridharma/*']) }}" aria-controls="tridharma">
        <i class="menu-icon mdi mdi-alpha-t-box"></i>
        <span class="menu-title">Tri Dharma</span>
        <i class="menu-arrow"></i>
      </a>
      <div class="collapse {{ show_class(['tridharma/*']) }}" id="tridharma">
        <ul class="nav flex-column sub-menu">
          <li class="nav-item {{ active_class(['tridharma/pendidikan']) }}">
            <a class="nav-link" href="{{ url('/tridharma/pendidikan') }}">Pendidikan</a>
          </li>
          <li class="nav-item {{ active_class(['tridharma/penelitian']) }}">
            <a class="nav-link" href="{{ url('/tridharma/penelitian') }}">Penelitian</a>
          </li>
          <li class="nav-item {{ active_class(['tridharma/pengabdian']) }}">
            <a class="nav-link" href="{{ url('/tridharma/pengabdian') }}">Pengabdian Masyarakat</a>
          </li>
        </ul>
      </div>
    </li>
    <li class="nav-item {{ active_class(['unduhan/*']) }}">
      <a class="nav-link" data-toggle="collapse" href="#unduhan" aria-expanded="{{ is_active_route(['unduhan/*']) }}" aria-controls="unduhan">
        <i class="menu-icon mdi mdi-download"></i>
        <span class="menu-title">Unduhan</span>
        <i class="menu-arrow"></i>
      </a>
      <div class="collapse {{ show_class(['unduhan/*']) }}" id="unduhan">
        <ul class="nav flex-column sub-menu">
          <li class="nav-item {{ active_class(['unduhan/unduhan']) }}">
            <a class="nav-link" href="{{ url('/unduhan/unduhan') }}">Daftar Unduhan</a>
          </li>
          <li class="nav-item {{ active_class(['unduhan/kategoriunduhan']) }}">
            <a class="nav-link" href="{{ url('/unduhan/kategoriunduhan') }}">Kategori Unduhan</a>
          </li>
        </ul>
      </div>
    </li>
    <li class="nav-item {{ active_class(['infopmb/*']) }}">
      <a class="nav-link" data-toggle="collapse" href="#infopmb" aria-expanded="{{ is_active_route(['infopmb/*']) }}" aria-controls="infopmb">
        <i class="menu-icon mdi mdi-alpha-i-box"></i>
        <span class="menu-title">Info PMB</span>
        <i class="menu-arrow"></i>
      </a>
      <div class="collapse {{ show_class(['infopmb/*']) }}" id="infopmb">
        <ul class="nav flex-column sub-menu">
          <li class="nav-item {{ active_class(['infopmb/infopmb']) }}">
            <a class="nav-link" href="{{ url('/infopmb/infopmb') }}">Daftar PMB</a>
          </li>
          <li class="nav-item {{ active_class(['infopmb/kategoriusm']) }}">
            <a class="nav-link" href="{{ url('/infopmb/kategoriusm') }}">Kategori USM</a>
          </li>
          <li class="nav-item {{ active_class(['infopmb/lokasi']) }}">
            <a class="nav-link" href="{{ url('/infopmb/lokasi') }}">Lokasi Ujian</a>
          </li>
        </ul>
      </div>
    </li>
    <li class="nav-item {{ active_class(['data-dosen/*']) }}">
      <a class="nav-link" data-toggle="collapse" href="#data-dosen" aria-expanded="{{ is_active_route(['data-dosen/*']) }}" aria-controls="data-dosen">
        <i class="menu-icon mdi mdi-human-child"></i>
        <span class="menu-title">Data Dosen</span>
        <i class="menu-arrow"></i>
      </a>
      <div class="collapse {{ show_class(['data-dosen/*']) }}" id="data-dosen">
        <ul class="nav flex-column sub-menu">
          <li class="nav-item {{ active_class(['data-dosen/data-dosen']) }}">
            <a class="nav-link" href="{{ url('/data-dosen/data-dosen') }}">Daftar Dosen</a>
          </li>
          <li class="nav-item {{ active_class(['data-dosen/kategoridosen']) }}">
            <a class="nav-link" href="{{ url('/data-dosen/kategoridosen') }}">Kategori Dosen</a>
          </li>
        </ul>
      </div>
    </li>
    <li class="nav-item {{ active_class(['kerjasama']) }}">
      <a class="nav-link" href="{{ url('/kerjasama') }}">
        <i class="menu-icon mdi mdi-alpha-k-box"></i>
        <span class="menu-title">Kerjasama</span>
      </a>
    </li>
    <li class="nav-item {{ active_class(['prestasi']) }}">
      <a class="nav-link" href="{{ url('/prestasi') }}">
        <i class="menu-icon mdi mdi-hexagram"></i>
        <span class="menu-title">Prestasi</span>
      </a>
    </li>
    <li class="nav-item {{ active_class(['user']) }}">
      <a class="nav-link" href="{{ url('/user') }}">
        <i class="menu-icon mdi mdi-account-multiple"></i>
        <span class="menu-title">User</span>
      </a>
    </li>
    <li class="nav-item {{ active_class(['beritafakultas']) }}">
      <a class="nav-link" href="{{ url('/beritafakultas') }}">
        <i class="menu-icon mdi mdi-table-large"></i>
        <span class="menu-title">Berita</span>
      </a>
    </li>
    <li class="nav-item {{ active_class(['gambar']) }}">
      <a class="nav-link" href="{{ url('/gambar') }}">
        <i class="menu-icon mdi mdi-image-plus"></i>
        <span class="menu-title">Gambar</span>
      </a>
    </li>
    <li class="nav-item {{ active_class(['slider']) }}">
      <a class="nav-link" href="{{ url('/slider') }}">
        <i class="menu-icon mdi mdi-image-move"></i>
        <span class="menu-title">Slider</span>
      </a>
    </li>


    
    <!-- <li class="nav-item {{ active_class(['tables/basic-table']) }}">
      <a class="nav-link" href="{{ url('/tables/basic-table') }}">
        <i class="menu-icon mdi mdi-table-large"></i>
        <span class="menu-title">Tables</span>
      </a>
    </li>
    <li class="nav-item {{ active_class(['icons/material']) }}">
      <a class="nav-link" href="{{ url('/icons/material') }}">
        <i class="menu-icon mdi mdi-emoticon"></i>
        <span class="menu-title">Icons</span>
      </a>
    </li>
    <li class="nav-item {{ active_class(['user-pages/*']) }}">
      <a class="nav-link" data-toggle="collapse" href="#user-pages" aria-expanded="{{ is_active_route(['user-pages/*']) }}" aria-controls="user-pages">
        <i class="menu-icon mdi mdi-lock-outline"></i>
        <span class="menu-title">User Pages</span>
        <i class="menu-arrow"></i>
      </a>
      <div class="collapse {{ show_class(['user-pages/*']) }}" id="user-pages">
        <ul class="nav flex-column sub-menu">
          <li class="nav-item {{ active_class(['user-pages/login']) }}">
            <a class="nav-link" href="{{ url('/user-pages/login') }}">Login</a>
          </li>
          <li class="nav-item {{ active_class(['user-pages/register']) }}">
            <a class="nav-link" href="{{ url('/user-pages/register') }}">Register</a>
          </li>
          <li class="nav-item {{ active_class(['user-pages/lock-screen']) }}">
            <a class="nav-link" href="{{ url('/user-pages/lock-screen') }}">Lock Screen</a>
          </li>
        </ul>
      </div>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="https://www.bootstrapdash.com/demo/star-laravel-free/documentation/documentation.html" target="_blank">
        <i class="menu-icon mdi mdi-file-outline"></i>
        <span class="menu-title">Documentation</span>
      </a>
    </li> -->
  </ul>
</nav>