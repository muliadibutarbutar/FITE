<footer class="footer-area">
<div class="container">
<div class="row justify-content-between">
<div class="col-sm-6 col-md-4 col-xl-3">
<div class="single-footer-widget footer_1">
<a href="https://www.kemdikbud.go.id/" target="_blank"> <img src="{{ url('assets/images/kemendikbud.png') }}" alt="Logo-DEL" alt="" style="max-width: 22%;height: auto;"> </a>&emsp;
<a href="https://www.del.or.id/" target="_blank"> <img src="{{ url('assets/images/yayasan.png') }}" alt="Logo-DEL" alt="" style="max-width: 22%;height: auto;"> </a>&emsp;
<a href="http://www.del.ac.id" target="_blank"> <img src="https://i.ibb.co/B2yZfVD/Logo-DEL.png" alt="Logo-DEL" alt="" style="max-width: 21%;height: auto;"> </a>
<p>Fakultas Teknik Informatika dan Elektro (FITE) Institut Teknologi Del

</p>
</div>
</div>
<div class="col-sm-6 col-md-4 col-xl-4">
<div class="single-footer-widget footer_2">
<h4>Media Sosial</h4>
<p>Fakultas Informatika dan Teknik Elektro memiliki beberapa media sosial untuk mempermudah proses dari satu Fakultas
</p>

<div class="social_icon">
<a href="https://www.facebook.com/Fakultas-Informatika-dan-Teknik-Elektro-Institut-Teknologi-Del-110480157795104/" target="_blank"> <i class="ti-facebook"></i> </a>
<a href="https://www.instagram.com/fite.itdel/?utm_medium=copy_link" target="_blank"> <i class="ti-instagram"></i> </a>
<a href="https://www.youtube.com/channel/UCDJags7epLES5Knn43OGG6g" target="_blank"> <i class="fa fa-youtube-play" aria-hidden="true"></i> </a>
</div>
</div>
</div>
<div class="col-xl-3 col-sm-6 col-md-4">
<div class="single-footer-widget footer_2">
<h4>Kontak Kami</h4>
<div class="contact_info">
<p><span> Alamat :</span> Jl. P.I. Del, Sitoluama, Lagu Boti, Kabupaten Toba Samosir, Sumatera Utara 22381 </p>
</div>
</div>
</div>
</div>
</div>
<div class="container-fluid">
<div class="row">
<div class="col-lg-12">
<div class="copyright_part_text text-center">
<div class="row">
<div class="col-lg-12">
<p class="footer-text m-0">
Copyright &copy;<script data-cfasync="false" src="../../cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script><script>document.write(new Date().getFullYear());</script> All rights reserved by <a href="http://www.del.ac.id" target="_blank">IT Del</a>
</p>
</div>
</div>
</div>
</div>
</div>
</div>
</footer>
