<header class="main_menu home_menu">
<div class="container">
<div class="row align-items-center">
<div class="col-lg-12">
<nav class="navbar navbar-expand-lg navbar-light">
	<div class="col-sm-4">
<a class="navbar-brand" href="/"> 
	<img src="{{ url('assets/images/fite.png') }}" alt="logo" style="max-width: 50%;height: auto;margin-left: -310px;"> 
</a>
</div>
<div class="col-sm-8">
<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
<span class="navbar-toggler-icon"></span>
</button>
<div class="collapse navbar-collapse main-menu-item justify-content-end" id="navbarSupportedContent" style="margin-right: -220px;">
<ul class="navbar-nav align-items-center">
	<li class="nav-item">
<a class="nav-link" href="/">Home</a>
</li>
<li class="nav-item">
<a class="nav-link" href="/berita">Berita</a>
</li>
<li class="nav-item">
<a class="nav-link" href="/tentang">Tentang</a>
</li>

<li class="nav-item dropdown">
<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
Program Studi
</a>
	
<div class="dropdown-menu" aria-labelledby="navbarDropdown">
		<?php 
			use App\ProgramStudi;
			$ps = ProgramStudi::all();
		?>
		<?php foreach($ps as $key): ?>
			<a class="dropdown-item" href="/programstudi/{{$key->id}}"><?php echo $key->nama_prodi; ?></a>
		<?php endforeach; ?>
</div>
</li>

<li class="nav-item dropdown">
<a class="nav-link dropdown-toggle" href="blog.html" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
Akademik
</a>
<div class="dropdown-menu" aria-labelledby="navbarDropdown">
	<a class="dropdown-item" href="/beasiswaGuest">Beasiswa</a>
<a class="dropdown-item" href="/kurikulumGuest">Kurikulum</a>
<a class="dropdown-item" href="/kalenderAkademik">Kalender Akademik dan Kemahasiswaan</a>
<!-- <a class="dropdown-item" href="/kalenderKemahasiswaan">Kalender Kemahasiswaan</a> -->
</div>
</li>
<li class="nav-item dropdown">
<a class="nav-link dropdown-toggle" href="blog.html" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
Kemahasiswaan
</a>
<div class="dropdown-menu" aria-labelledby="navbarDropdown">
<a class="dropdown-item" href="/himaproGuest">Himapro</a>
<a class="dropdown-item" href="/klubGuest">Klub</a>
</div>
</li>
<li class="nav-item">
<a class="nav-link" href="/infopmbGuest">Info&nbsp;&nbsp;PMB</a>
</li>
<li class="nav-item dropdown">
<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
Tri&nbsp;Dharma
</a>
<div class="dropdown-menu" aria-labelledby="navbarDropdown">
<a class="dropdown-item" href="/pendidikanGuest">Pendidikan</a>
<a class="dropdown-item" href="/penelitianGuest">Penelitian</a>
<a class="dropdown-item" href="/pengabdianGuest">Pengabdian Masyarakat</a>
<a class="dropdown-item" href="/prestasiGuest">Prestasi</a>
</div>
</li>
<li class="nav-item">
<a class="nav-link" href="/alumniGuest">Alumni</a>
</li>
<li class="nav-item">
<a class="nav-link" href="/unduhanGuest">Unduhan</a>
</li>
<li class="nav-item dropdown">
<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
Fasilitas
</a>
	
<div class="dropdown-menu" aria-labelledby="navbarDropdown">
		<?php 
			use App\KategoriFasilitas;
			$ps = KategoriFasilitas::all();
		?>
		<?php foreach($ps as $key): ?>
			<a class="dropdown-item" href="/fasilitas/{{$key->id}}"><?php echo $key->kategori; ?></a>
		<?php endforeach; ?>
</div>
</li>
<li class="nav-item dropdown">
<a class="nav-link dropdown-toggle" href="blog.html" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
Akses Cepat
</a>
<div class="dropdown-menu" aria-labelledby="navbarDropdown">
<a class="dropdown-item" href="https://spmb.del.ac.id/users/sign_in" target="_blank">SPMB IT DEL</a>
<a class="dropdown-item" href="https://cis.del.ac.id" target="_blank">CIS IT DEL</a>
<a class="dropdown-item" href="https://ecourse.del.ac.id/login/index.php" target="_blank">Ecourse IT DEL</a>
<a class="dropdown-item" href="https://sipp.del.ac.id/" target="_blank">Online Library Information System IT DEL</a>
<a class="dropdown-item" href="https://www.instagram.com/fite.itdel/?utm_medium=copy_link" target="_blank">Instagram</a>
<a class="dropdown-item" href="https://www.youtube.com/channel/UCDJags7epLES5Knn43OGG6g" target="_blank">YouTube</a>
<a class="dropdown-item" href="https://www.facebook.com/Fakultas-Informatika-dan-Teknik-Elektro-Institut-Teknologi-Del-110480157795104/" target="_blank">Facebook</a>
</div>
</li>

</li>
<li>
<a href="https://kampusmerdeka.kemdikbud.go.id/" target="_blank"><img src="{{ url('assets/images/kampus.png') }}" style="max-width: 800%;height: auto;">&emsp;</a>
</li>
</ul>
</div>
</div>
</nav>
</div>
</div>
</div>
</header>