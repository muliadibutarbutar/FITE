<!doctype html>
<html lang="en">

<!-- Mirrored from preview.colorlib.com/theme/etrain/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 01 Sep 2021 09:04:52 GMT -->
<head>

<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<title>FITE</title>
<link rel="icon" href="{{ url('assets/images/del.png') }}">

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.css">
  <link rel="stylesheet" href="https://cdn.datatables.net/1.11.0/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="{{ url('front/css/A.bootstrap.min.css%2banimate.css%2bowl.carousel.min.css%2bthemify-icons.css%2bflaticon.css%2bmagnific-popup.css%2bslick.css%2cMcc.cumd4WexDy.css.pagespeed.cf.P9fOwQ_VlG.css')}}" />
<link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>
<script src="https://use.fontawesome.com/8465ebd807.js"></script>


<link rel="stylesheet" href="{{url('front/css/A.style.css.pagespeed.cf.ocx23Myqm7.css')}}">
</head>
<body>

<!-- Header Master -->
@include('user.layout.header')

@yield('content')

<!-- Buat footer untuk master -->
@include('user.layout.footer')
 <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
  <script src="https://cdn.datatables.net/1.11.0/js/jquery.dataTables.min.js"></script>
  <script src="https://cdn.datatables.net/1.11.0/js/dataTables.bootstrap4.min.js"></script>
<script src="{{ url('front/js/jquery-1.12.1.min.js')}}"></script>

<script src="{{ url('front/js/popper.min.js%2bbootstrap.min.js.pagespeed.jc.u-jKsEkIJu.js')}}"></script><script>eval(mod_pagespeed_53lpAu8E7e);</script>

<script>eval(mod_pagespeed_T9kFIRnY24);</script>

<script src="{{ url('front/js/jquery.magnific-popup.js')}}"></script>

<script src="{{ url('front/js/swiper.min.js')}}"></script>

<script src="{{ url('front/js/masonry.pkgd.js')}}"></script>

<script src="{{ url('front/js/owl.carousel.min.js%2bjquery.nice-select.min.js%2bslick.min.js%2bjquery.counterup.min.js%2bwaypoints.min.js.pagespeed.jc.QrZJiAiLnj.js')}}"></script><script>eval(mod_pagespeed_6EngO7M_5T);</script>
<script>eval(mod_pagespeed_3EGaY3$V6K);</script>

<script>eval(mod_pagespeed_kjcmwz9LPS);</script>
<script>eval(mod_pagespeed_tlIiTErLMn);</script>
<script>eval(mod_pagespeed_gkcoGKJYny);</script>

<script src="{{ ('front/js/custom.js') }}"></script>

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-23581568-13"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-23581568-13');
</script>
<script defer src="../../../static.cloudflareinsights.com/beacon.min.js" data-cf-beacon='{"rayId":"687d54c4381f04b4","token":"cd0b4b3a733644fc843ef0b185f98241","version":"2021.8.1","si":10}'></script>
<script type="text/javascript">
    $(document).ready( function() {
  $('#tableMaster').dataTable( {
    "oSearch": {"sSearch": "2021"},
  } );
} )
  </script>

  <script type="text/javascript">
    $(document).ready( function() {
  $('#tableMaster1').dataTable( {
  });
})
  </script>
<script type="text/javascript">
    $(document).ready( function() {
  $('#alumni').dataTable( {
    "oSearch": {"sSearch": "2020"},
    language: {
      searchPlaceholder: "Masukkan Tahun Lulusan"
    }
    });
  })
  </script>


<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.11.1/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/searchpanes/1.4.0/js/dataTables.searchPanes.min.js"></script>
<script src="https://cdn.datatables.net/select/1.3.3/js/dataTables.select.min.js"></script>

<script type="text/javascript">
  $(document).ready(function() {
    $('#example tfoot th').each(function() {
        var title = $(this).text();
        $(this).html('<input type="text" placeholder="Search ' + title + '" />');
    });
 
    var table = $('#example').DataTable({
        searchPanes: {
            viewTotal: true
        },
        dom: 'Plfrtip'
    });
 
     table.columns().every( function() {
        var that = this;
  
        $('input', this.footer()).on('keyup change', function() {
            if (that.search() !== this.value) {
                that
                    .search(this.value)
                    .draw();
            }
        });
    });
});
  function myFunction() {
  var x = document.getElementById("myDIV");
  if (x.style.display === "none") {
    x.style.display = "block";
  } else {
    x.style.display = "none";
  }
}
</script>
</body>

<!-- Mirrored from preview.colorlib.com/theme/etrain/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 01 Sep 2021 09:04:59 GMT -->
</html>