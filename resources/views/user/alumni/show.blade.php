@extends('user.layout.master')

@section('content')

<section class="breadcrumb breadcrumb_bg" style="margin-top:115px;">
<div class="container">
<div class="row">
<div class="col-lg-12 mb-4">
<div class="breadcrumb_iner text-center">
<div class="breadcrumb_iner_item">
<h2>Alumni</h2>
<p>Fakultas Informatika dan Teknik Elektro</p>
</div>
</div>
</div>
</div>
</div>
</section>

<section class="blog_area section_padding">
<div class="container">
<div class="row">
	@foreach($al as $f)
<div class="col-md-4">
<div class="blog_left_sidebar">
	
<article class="blog_item">
<div class="blog_item_img">
	<center>
		<a href="{{$f->link}}" target="_blank">
<img class="card-img rounded-10" src="{{ asset('images/'.$f->testimoni) }}" style="width:100%;max-height: auto; border-radius: 10px;" alt="">
</a>
</center>
<!-- <a href="#" class="blog_item_date">
<h3>15</h3>
<p>Jan</p>
</a> -->
</div>
<div class="blog_details">
	<b>
<h7>{{$f->nama}}</h7><br>
<h7>Tahun {{$f->tahun_lulus}}</h7><br>
<h7>{{$f->prodi->nama_prodi}}</h7><br>
</b>
<ul class="blog-info-link">
<li><a href="https://www.del.ac.id" target="_blank"><i class="far fa-user"></i> Institut Teknologi Del DEL</a></li>
</ul>
</div>
</article>

</div>
</div>
@endforeach
</div>
</div>
</section>


</div>
</div>
</div>
</section>



@endsection
