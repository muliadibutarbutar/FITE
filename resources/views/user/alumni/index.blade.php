@extends('user.layout.master')

@section('content')

<section class="breadcrumb breadcrumb_bg" style="margin-top:115px;">
<div class="container">
<div class="row">
<div class="col-lg-12 mb-4">
<div class="breadcrumb_iner text-center">
<div class="breadcrumb_iner_item">
<h2>Alumni</h2>
<p>Fakultas Informatika dan Teknik Elektro</p>
</div>
</div>
</div>
</div>
</div>
</section>

<section class="blog_area section_padding">
<div class="container">
<div class="row">
  <div class="col-lg-12 grid-margin stretch-card" style="border-radius:20px;">
    <h2>Jumlah Lulusan</h2>
    <div class="card" style="border-radius:15px;margin-bottom: 20px;">
      <div class="card-body">
        <div class="table-responsive">
          <table class="table table-hover" id="alumni">
            <thead>
              <tr>
                <th> No </th>
                <th> Program Studi </th>
                <th> Tahun Lulus </th>
                <th> Jumlah </th>
              </tr>
            </thead>
            <tbody>
              @foreach ($jl as $u)
              <tr>
                <td class="py-1">
                  {{$loop->iteration}}
                </td>
                <td> 
                  <b>{{ $u->prodi->nama_prodi }}</b>
                </td>
                <td>
                  {{$u->tahun_lulus}}
                </td>
                <td>
                  {{$u->jumlah}}
                </td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="row">

<div class="col-lg-12">
<div class="blog_left_sidebar">
  @foreach($ps as $f)
<article class="blog_item" style="background-color: rgba(232, 232, 232, 1); border-radius: 15px;">
<div class="blog_item_img">
<!-- <a href="#" class="blog_item_date">
<h3>15</h3>
<p>Jan</p>
</a> -->
</div>
<div class="blog_details">
<a class="d-inline-block" href="/alumniProdi/{{$f->id}}">
<h2>{{$f->nama_prodi}}</h2>
</a>
</div>
</article>
@endforeach
</div>
</div>
</div>
</div>
</section>


</div>
</div>
</div>
</section>



@endsection
