@extends('user.layout.master')

@section('content')
<section class="banner_part">
<div class="container">
<div class="row align-items-center">
<div class="col-lg-6 col-xl-6">
<div class="banner_text">
<div class="banner_text_iner">
<h1>FAKULTAS INFORMATIKA DAN TEKNIK ELEKTRO</h1>
<p>Fakultas Teknik Informatika dan Elektro (FITE) IT Del berdiri pada tahun 2013 seiring dengan perubahan Politeknik Informatika Del (PI Del) menjadi IT Del

</p>
</div>
</div>
</div>
</div>
</div>
</section>
<div style="margin-left: 1000px;margin-top: -800px;margin-bottom: 400px;border-radius: 50%;">
<div id="carouselExampleControls" class="carousel slide" data-ride="carousel" style="max-width: 70%;top:150px;max-height:50%;content:"";">
  <div class="carousel-inner">
    @foreach($sliderLast as $s)
    <div class="carousel-item active">
      <img src="{{ asset('images/'.$s->gambar) }}" width="650" height="400" alt="..." style="border-radius:20px;">
    </div>
    @endforeach
    @foreach($slider as $s)
    <div class="carousel-item">
      <img src="{{ asset('images/'.$s->gambar) }}" width="650" height="400" alt="..." style="border-radius:20px;">
    </div>
    @endforeach
  </div>

  <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>
</div>

<section class="learning_part">
<div class="container">
<div class="row align-items-sm-center align-items-lg-stretch">
<div class="col-md-6 col-lg-6">
<div class="learning_img">
<img  src="{{ asset('images/'.$dekan->foto) }}" alt="" style="max-width: 90%;height: auto;">
</div>
</div>
<div class="col-md-6 col-lg-6">
<div class="learning_member_text">
<h5>Dekan Fakultas Informatika dan Teknik Elektro</h5>
<h3>{{$dekan->nama}}</h3>
<p>{!! $dekan->deskripsi !!}</p>
</div>
</div>
</div>
</div>
</section>



<!-- <section class="special_cource padding_top">
<div class="container">
<div class="row justify-content-center">
<div class="col-xl-5">
<div class="section_tittle text-center">
<p>popular courses</p>
<h2>Special Courses</h2>
</div>
</div>
</div>
<div class="row">
<div class="col-sm-6 col-lg-4">
<div class="single_special_cource">
<img src="img/xspecial_cource_1.png.pagespeed.ic.XcwnmMeEmh.png" class="special_img" alt="">
<div class="special_cource_text">
<a href="course-details.html" class="btn_4">Web Development</a>
<h4>$130.00</h4>
<a href="course-details.html"><h3>Web Development</h3></a>
<p>Which whose darkness saying were life unto fish wherein all fish of together called</p>
<div class="author_info">
<div class="author_img">
<img src="img/author/xauthor_1.png.pagespeed.ic.MgwxNxwW5k.png" alt="">
<div class="author_info_text">
<p>Conduct by:</p>
<h5><a href="#">James Well</a></h5>
</div>
</div>
<div class="author_rating">
<div class="rating">
<a href="#"><img src="img/icon/color_star.svg" alt=""></a>
<a href="#"><img src="img/icon/color_star.svg" alt=""></a>
<a href="#"><img src="img/icon/color_star.svg" alt=""></a>
<a href="#"><img src="img/icon/color_star.svg" alt=""></a>
<a href="#"><img src="img/icon/star.svg" alt=""></a>
</div>
<p>3.8 Ratings</p>
</div>
</div>
</div>
</div>
</div>
<div class="col-sm-6 col-lg-4">
<div class="single_special_cource">
<img src="img/xspecial_cource_2.png.pagespeed.ic._xbWM3G7Ms.png" class="special_img" alt="">
<div class="special_cource_text">
<a href="course-details.html" class="btn_4">design</a>
<h4>$160.00</h4>
<a href="course-details.html"> <h3>Web UX/UI Design </h3></a>
<p>Which whose darkness saying were life unto fish wherein all fish of together called</p>
<div class="author_info">
<div class="author_img">
<img src="img/author/xauthor_2.png.pagespeed.ic.3kfzzAVZT9.png" alt="">
<div class="author_info_text">
<p>Conduct by:</p>
<h5><a href="#">James Well</a></h5>
</div>
</div>
<div class="author_rating">
<div class="rating">
<a href="#"><img src="img/icon/color_star.svg" alt=""></a>
<a href="#"><img src="img/icon/color_star.svg" alt=""></a>
<a href="#"><img src="img/icon/color_star.svg" alt=""></a>
<a href="#"><img src="img/icon/color_star.svg" alt=""></a>
<a href="#"><img src="img/icon/star.svg" alt=""></a>
</div>
<p>3.8 Ratings</p>
</div>
</div>
</div>
</div>
</div>
<div class="col-sm-6 col-lg-4">
<div class="single_special_cource">
<img src="img/xspecial_cource_3.png.pagespeed.ic.jtCmqLbtyi.png" class="special_img" alt="">
<div class="special_cource_text">
<a href="course-details.html" class="btn_4">Wordpress</a>
<h4>$140.00</h4>
<a href="course-details.html"> <h3>Wordpress Development</h3> </a>
<p>Which whose darkness saying were life unto fish wherein all fish of together called</p>
<div class="author_info">
<div class="author_img">
<img src="img/author/xauthor_3.png.pagespeed.ic.GJXpJYYrx7.png" alt="">
<div class="author_info_text">
<p>Conduct by:</p>
<h5><a href="#">James Well</a></h5>
</div>
</div>
<div class="author_rating">
<div class="rating">
<a href="#"><img src="img/icon/color_star.svg" alt=""></a>
<a href="#"><img src="img/icon/color_star.svg" alt=""></a>
<a href="#"><img src="img/icon/color_star.svg" alt=""></a>
<a href="#"><img src="img/icon/color_star.svg" alt=""></a>
<a href="#"><img src="img/icon/star.svg" alt=""></a>
</div>
<p>3.8 Ratings</p>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</section> -->


<!-- <section class="advance_feature learning_part">
<div class="container">
<div class="row align-items-sm-center align-items-xl-stretch">
<div class="col-md-6 col-lg-6">
<div class="learning_member_text">
<h5>Advance feature</h5>
<h2>Our Advance Educator
Learning System</h2>
<p>Fifth saying upon divide divide rule for deep their female all hath brind mid Days
and beast greater grass signs abundantly have greater also use over face earth
days years under brought moveth she star</p>
<div class="row">
<div class="col-sm-6 col-md-12 col-lg-6">
<div class="learning_member_text_iner">
<span class="ti-pencil-alt"></span>
<h4>Learn Anywhere</h4>
<p>There earth face earth behold she star so made void two given and also our</p>
</div>
</div>
<div class="col-sm-6 col-md-12 col-lg-6">
<div class="learning_member_text_iner">
<span class="ti-stamp"></span>
<h4>Expert Teacher</h4>
<p>There earth face earth behold she star so made void two given and also our</p>
</div>
</div>
</div>
</div>
</div>
<div class="col-lg-6 col-md-6">
<div class="learning_img">
<img src="img/xadvance_feature_img.png.pagespeed.ic.5V8QYgBIW7.png" alt="">
</div>
</div>
</div>
</div>
</section> -->



<section class="blog_part section_padding" style="margin-top:-200px;">
<div class="container">
<div class="row justify-content-center">
<div class="col-xl-5">
<div class="section_tittle text-center">
<h2>Berita Terkini</h2>
</div>
</div>
</div>
<div class="row">
	@foreach($berita as $b)
<div class="col-sm-6 col-lg-4 col-xl-4">
<div class="single-home-blog">
<div class="card">
<img src="{{ asset('images/'.$b->gambar) }}" class="card-img-top" alt="blog">
<div class="card-body">
<a href="/berita/{{$b->id}}">
<h5 class="card-title">{{ Str::limit($b->nama,40)}}
</h5>
</a>
<p>{!! Str::limit($b->deskripsi,120) !!}</p>
<ul>
<li> <span class="ti-comments"></span>Posted By : {{$b->user->name}}</li>
<li> <span class="ti-heart"></span>{{ $b->created_at->format('d M Y') }}</li>
</ul>
</div>
</div>
</div>
</div>
@endforeach
</div>
<center>
<a href="/berita" class="btn_1" style="margin-top: 10px;">Read More</a>
</center>
</div>
</section>

<section class="feature_part" style="margin-top:-100px;">
<div class="container">
<div class="row">
<div class="col-sm-6 col-xl-3 align-self-center">
<div class="single_feature_text ">
<h2>Fasilitas</h2>
<p>Fakultas Informatika dan Teknik Elektro menyediakan fasilitas yang baik dan ergonomis untuk mendukung suasana belajar mengajar.</p>
<a href="/fasilitas" class="btn_1">Read More</a>
</div>
</div>
@foreach($fasilitas as $f)
<div class="col-sm-6 col-xl-3">
<div class="single_feature">
<div class="single_feature_part">
<span class="single_feature_icon"><i class="ti-light-bulb"></i></span>
<h4>{{$f->nama}}</h4>
<p>{!! Str::limit($f->deskripsi,100) !!}</p>
</div>
</div>
</div>
@endforeach
<!-- <div class="col-sm-6 col-xl-3">
<div class="single_feature">
<div class="single_feature_part">
<span class="single_feature_icon"><i class="ti-light-bulb"></i></span>
<h4>Asrama</h4>
<p>Asrama merupakan tempat tinggal dari semua mahasiswa yang tinggal dari IT Del dan menerapkan sistem terbaik dalam hidup berasrama juga hidup mandiri.</p>
</div>
</div>
</div>
<div class="col-sm-6 col-xl-3">
<div class="single_feature">
<div class="single_feature_part single_feature_part_2">
<span class="single_service_icon style_icon"><i class="ti-light-bulb"></i></span>
<h4>Ruang Kolaboratif</h4>
<p>Ruang Kolaboratif yang disediakan oleh pihak fakultas merupakan tempat yang sangat aman dan cocok untuk mahasiswa dalam belajar mandiri.</p>
</div>
</div>
</div> -->
</div>
</div>
</section>


<section class="testimonial_part" style="margin-top:100px;">
<div class="container-fluid">
<div class="row justify-content-center">
<div class="col-xl-5">
<div class="section_tittle text-center">
<h2>Kerja Sama</h2>
</div>
</div>
</div>
<div class="row">
<div class="col-lg-12">
<div class="textimonial_iner owl-carousel">
<div class="testimonial_slider">
<div class="row">
  @foreach($kerjasamaFiveLatest as $k)
<div class="col-lg-2 col-xl-2 col-sm-4">
<div class="testimonial_slider_img">
<img src="{{ asset('images/'.$k->gambar) }}" alt="#">
</div>
</div>
@endforeach
</div>
</div>
<div class="testimonial_slider">
<div class="row">
  @foreach($kerjasamaRandomFiveToOne as $k)
<div class="col-xl-2 d-none d-xl-block">
<div class="testimonial_slider_img">
<img src="{{ asset('images/'.$k->gambar) }}" alt="#">
</div>
</div>
@endforeach
</div>
</div>
<div class="testimonial_slider">
<div class="row">
  @foreach($kerjasamaRandomFiveToTwo as $k)
<div class="col-lg-4 col-xl-2 col-sm-4">
<div class="testimonial_slider_img">
<img src="{{ asset('images/'.$k->gambar) }}" alt="#">
</div>
</div>
@endforeach
</div>
</div>
</div>
</div>
</div>
</div>
</section>

<section class="special_cource padding_top" style="margin-top:-100px;">
<div class="container">
<div class="row justify-content-center">
<div class="col-xl-5">
<div class="section_tittle text-center">
<h2>Testimoni</h2>
</div>
</div>
</div>
<div class="row" style="margin-top:-50px;">
	@foreach($alumni as $k)
<div class="author_info">
<div class="author_img">
	<a href="{{ asset('images/'.$k->testimoni) }}" target="_blank">
<img src="{{ asset('images/'.$k->testimoni) }}" alt="" width="300" height="300" style="border-radius: 10%;background-size: cover;display: block; margin: 40px;">
</a>
</div>
</div>
@endforeach
</div>
</div>
</section>

@endsection
