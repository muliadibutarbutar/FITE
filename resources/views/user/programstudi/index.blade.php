@extends('user.layout.master')

@section('content')

<section class="breadcrumb breadcrumb_bg" style="margin-top:115px;">
<div class="container">
<div class="row">
<div class="col-lg-12 mb-4">
<div class="breadcrumb_iner text-center">
<div class="breadcrumb_iner_item">
<h2>{{$ps->nama_prodi}}</h2>
<p>Fakultas Informatika dan Teknik Elektro</p>
</div>
</div>
</div>
</div>
</div>
</section>

<!-- Tentang -->
<section class="learning_part">
<div class="container">
<div class="row align-items-sm-center align-items-lg-stretch">
<div class="col-md-12 col-lg-12">
<!-- <p> <font size="6" color="text-black">{!! $ps->deskripsi !!}</font></p> -->

</div>
</div>
<div class="row">
  <div class="col-lg-12 grid-margin stretch-card">
  </div>
  <div class="row align-items-sm-center align-items-lg-stretch" style="margin-top:-100px;margin-bottom: 30px;">
<div class="col-md-12 col-lg-12">
	<h3>Profil</h3>
<p style="color:black;"><font size="5" color="black">{!! $ps->deskripsi !!}</font></p>
<hr>
</div>
</div>
  <div class="row align-items-sm-center align-items-lg-stretch">
<div class="col-md-6 col-lg-6">
	<h3>Visi</h3>
<p style="color:black;"> <font size="5">{!! $vs->visi !!}</font></p>

</div>
<div class="col-md-6 col-lg-6">
	<h3>Misi</h3>
<p> <font size="5">{!! $vs->misi !!}</font></p>

</div>
</div>
</div>
<hr>
<div class="row align-items-sm-center align-items-lg-stretch">
<div class="col-md-12 col-lg-12">
	<h3>Tujuan</h3>
<p> <font size="5">{!! $tj->tujuan !!}</font></p>

</div>
</div>
<hr>
<div class="row align-items-sm-center align-items-lg-stretch">
<div class="col-md-12 col-lg-12">
	<h3>Etika</h3>
<p> <font size="5">{!! $etika->etika_prodi !!}</font></p>

</div>
</div>
<hr>
<div class="row align-items-sm-center align-items-lg-stretch">
<div class="col-md-12 col-lg-12">
	<h3>Kompetensi</h3>
<p> <font size="5">{!! $kp->kompetensi !!}</font></p>

</div>

<div class="col-md-12 col-lg-12">
	<hr>
	<h3>Kurikulum</h3>
<article class="blog_item" style="background-color: rgba(232, 232, 232, 1); border-radius: 15px;">
<div class="blog_item_img">
<!-- <a href="#" class="blog_item_date">
<h3>15</h3>
<p>Jan</p>
</a> -->
</div>
<div class="blog_details">
	@foreach($kurikulum as $f)
<a class="d-inline-block" href="#">
<h2><a href="{{ asset('images/'.$f->pdf) }}" target="_blank">Kurikulum {{$f->tahun->tahun}}</a></h2>
@endforeach
</a>
<ul class="blog-info-link">
<li><a href="https://www.del.ac.id" target="_blank"><i class="far fa-user"></i> IT DEL</a></li>
</ul>
</div>
</article>

</div>
</div>
</div>

</div>

</section>

<section class="blog_part section_padding" style="margin-top:-350px;">
<div class="container">
<div class="row justify-content-center">
<div class="col-xl-5">
<div class="section_tittle text-center">
	<hr>
<h2>Dosen</h2>
</div>
</div>
</div>
<div class="row">
	@foreach($dosen as $b)
<div class="col-sm-6 col-lg-6 col-xl-4" style="margin-bottom: 10px;">
<div class="single-home-blog">
<div class="card">
<img src="{{ asset('images/'.$b->gambar) }}" class="card-img-top" alt="blog"  style="max-width:100%;height: 10%; border-radius: 10px;">
<div class="card-body">
<a href="{{$b->link}}" target="_blank">
<b><h7 class="card-title">{{ Str::limit($b->nama,70)}}
</h7></b>
</a>
<b><h4>{!! Str::limit($b->jabatan->nama,10) !!}</h4></b>
<p>{!! Str::limit($b->riwayatpendidikan,80) !!}</p>
<ul>
	<!--
<li> <span class="ti-heart"></span>{{ $b->created_at->format('d M Y') }}</li> -->
</ul>
</div>
</div>
</div>
</div>
@endforeach
</div>
</div>
</section>



@endsection
