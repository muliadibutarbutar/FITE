@extends('user.layout.master')

@section('content')

<section class="breadcrumb breadcrumb_bg" style="margin-top:115px;">
<div class="container">
<div class="row">
<div class="col-lg-12 mb-4">
<div class="breadcrumb_iner text-center">
<div class="breadcrumb_iner_item">
<h2>Berita</h2>
<p>Fakultas Informatika dan Teknik Elektro</p>
</div>
</div>
</div>
</div>
</div>
</section>

<section class="blog_area section_padding">
<div class="container">
<div class="row">
<div class="col-lg-12">
<div class="blog_left_sidebar">
<article class="blog_item">
<div class="blog_item_img">
	<center>
<img class="card-img rounded-10" src="{{ asset('images/'.$f->gambar) }}" style="width:50%;max-height: auto;" alt="">
</center>
<a href="#" class="blog_item_date">
<h3>{{ $f->created_at->format('d') }}</h3>
<p>{{ $f->created_at->format('M') }}</p>
</a>
</div>
<div class="blog_details">
<a class="d-inline-block" href="#">
<h2>{{$f->nama}}</h2>
</a>
<p>{!! $f->deskripsi !!}</p>
<ul class="blog-info-link">
<li><a href="#"><i class="far fa-user"></i> Posted By</a> &nbsp;{{$f->user->name}}
</li>
</ul>
</div>
</article>
</div>
</div>
</div>
</div>
</section>


</div>
</div>
</div>
</section>



@endsection
