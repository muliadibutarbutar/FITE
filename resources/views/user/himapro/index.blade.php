@extends('user.layout.master')

@section('content')

<section class="breadcrumb breadcrumb_bg" style="margin-top:115px;">
<div class="container">
<div class="row">
<div class="col-lg-12 mb-4">
<div class="breadcrumb_iner text-center">
<div class="breadcrumb_iner_item">
<h2>Himpunan Mahasiswa Prodi</h2>
<p>Fakultas Informatika dan Teknik Elektro</p>
</div>
</div>
</div>
</div>
</div>
</section>

<section class="blog_area section_padding">
<div class="container">
<div class="row">
<div class="col-lg-12">
<div class="blog_left_sidebar">
	@foreach($himapro as $f)
<article class="blog_item">
<div class="blog_item_img">
<img class="card-img rounded-10" src="{{ asset('images/'.$f->gambar) }}" style="width:50%;max-height: auto;" alt="">
<!-- <a href="#" class="blog_item_date">
<h3>15</h3>
<p>Jan</p>
</a> -->
</div>
<div class="blog_details">
<a class="d-inline-block" href="single-blog.html">
<h1>{{$f->nama}}</h1>
</a>
<h3>Aktifitas</h3>
<p>{!! $f->aktifitas !!}</p>
<h3>Deskripsi</h3>
<p>{!! $f->deskripsi !!}</p>
<ul class="blog-info-link">
<li><a href="https://www.del.ac.id" target="_blank"><i class="far fa-user"></i> Institut Teknologi DEL</a></li>
</ul>
</div>
</article>
@endforeach
</div>
</div>
</div>
</div>
</section>


</div>
</div>
</div>
</section>



@endsection
