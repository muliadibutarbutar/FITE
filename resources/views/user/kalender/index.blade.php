@extends('user.layout.master')

@section('content')

<section class="breadcrumb breadcrumb_bg" style="margin-top:115px;">
<div class="container">
<div class="row">
<div class="col-lg-12 mb-4">
<div class="breadcrumb_iner text-center">
<div class="breadcrumb_iner_item">
<h2>Kalender Akademik dan Kemahasiswaan</h2>
<p>Fakultas Informatika dan Teknik Elektro</p>
</div>
</div>
</div>
</div>
</div>
</section>

<!-- Tentang -->
<section class="learning_part">
<div class="container">
<div class="row align-items-sm-center align-items-lg-stretch">
<div class="col-md-12 col-lg-12">
	@foreach($ka as $k)
<p> <font size="5"><a href="{{ asset('images/'.$k->kalender) }}" target="_blank">{{ $k->nama }}</a></font></p>
@endforeach
@foreach($ut as $k)
<p> <font size="5"><a href="{{ asset('images/'.$k->pdf) }}" target="_blank">{{ $k->nama }}</a></font></p>
@endforeach
</div>
</div>
</div>
</section>



@endsection
