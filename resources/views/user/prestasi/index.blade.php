@extends('user.layout.master')

@section('content')

<section class="breadcrumb breadcrumb_bg" style="margin-top:115px;">
<div class="container">
<div class="row">
<div class="col-lg-12 mb-4">
<div class="breadcrumb_iner text-center">
<div class="breadcrumb_iner_item">
<h2>Prestasi</h2>
<p>Fakultas Informatika dan Teknik Elektro</p>
</div>
</div>
</div>
</div>
</div>
</section>

<section class="blog_area section_padding">
<div class="container">
<div class="row">
<div class="col-lg-12">
<div class="blog_left_sidebar">
	
<article class="blog_item" style="border-radius:10px;">
<div class="blog_item_img">
<table class="table table-hover" cellspacing="0" cellpadding="0" id="tableMaster1">
  <thead>
    <tr>
      <th scope="col">No</th>
      <th scope="col">Nama</th>
      <th scope="col">Waktu</th>
      <th scope="col">Tingkatan</th>
      <th scope="col">Pencapaian</th>
      <th scope="col">Deskripsi</th>
    </tr>
  </thead>
  <tbody>
    @foreach($prestasi as $f)
    <tr>
      <th scope="row">{{$loop->iteration}}</th>
      <td>{{$f->nama}}</td>
      <td>{{$f->waktu}}</td>
      <td>{{$f->tingkatan}}</td>
      <td>{{$f->dicapai}}</td>
      <td><font color="black"> {!! $f->deskripsi !!}</font></td>
    </tr>
    @endforeach
  </tbody>
</table>
<!-- <a href="#" class="blog_item_date">
<h3>15</h3>
<p>Jan</p>
</a> -->
</div>
<div class="blog_details">
<a class="d-inline-block" href="single-blog.html">

</a>

<ul class="blog-info-link">
<li><a href="https://www.del.ac.id" target="_blank"><i class="far fa-user"></i> Institu Teknologi DEL</a></li>
</ul>
</div>
</article>

</div>
</div>
</div>
</div>
</section>


</div>
</div>
</div>
</section>



@endsection
