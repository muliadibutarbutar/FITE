@extends('user.layout.master')

@section('content')

<section class="breadcrumb breadcrumb_bg" style="margin-top:115px;">
<div class="container">
<div class="row">
<div class="col-lg-12 mb-4">
<div class="breadcrumb_iner text-center">
<div class="breadcrumb_iner_item">
<h2>Beasiswa</h2>
<p>Fakultas Informatika dan Teknik Elektro</p>
</div>
</div>
</div>
</div>
</div>
</section>

<section class="blog_area section_padding">
<div class="container">
<div class="row">
<div class="col-lg-12">
<div class="blog_left_sidebar">
	@foreach($beasiswa as $f)
<article class="blog_item">
<div class="blog_item_img">
	<center>

</center>
<!-- <a href="#" class="blog_item_date">
<h3>15</h3>
<p>Jan</p>
</a> -->
</div>
<div class="blog_details" style="background-color:rgba(232, 232, 232, 1); border-radius: 15px;">
<a class="d-inline-block" href="{{ asset('images/'.$f->logo) }}" target="_blank">
	<img class="card-img rounded-10" src="{{ asset('images/'.$f->logo) }}" style="width:25%;max-height: auto;margin-bottom: 20px; border-radius: 20px;" alt="">
<h2>{{$loop->iteration}}.&nbsp;{{$f->nama}}</h2>
</a>
<p>{!! $f->deskripsi !!}</p>
<ul class="blog-info-link">
<li><a href="https://www.del.ac.id" target="_blank"><i class="far fa-user"></i> Institut Teknologi DEL</a></li>
</ul>
</div>
</article>
@endforeach
</div>
</div>
</div>
</div>
</section>


</div>
</div>
</div>
</section>



@endsection
