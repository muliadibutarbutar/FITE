@extends('user.layout.master')

@section('content')

<section class="breadcrumb breadcrumb_bg" style="margin-top:115px;">
<div class="container">
<div class="row">
<div class="col-lg-12 mb-4">
<div class="breadcrumb_iner text-center">
<div class="breadcrumb_iner_item">
<h2>Tentang</h2>
<p>Fakultas Informatika dan Teknik Elektro</p>
</div>
</div>
</div>
</div>
</div>
</section>

<!-- Tentang -->
<section class="learning_part">
<div class="container">
	<div class="row">
  <div class="col-lg-12 grid-margin stretch-card">
  	<center>
  <h2>Visi Misi Fakultas Informatika dan Teknik Elektro</h2>
  </center>
  </div>
  
  <div class="row align-items-sm-center align-items-lg-stretch">
<div class="col-md-6 col-lg-6">
	<h3>Visi</h3>
<p style="color:black;"> <font size="5">{!! $vs->visi !!}</font></p>

</div>
<div class="col-md-6 col-lg-6">
	<h3>Misi</h3>
<p> <font size="5">{!! $vs->misi !!}</font></p>

</div>
</div>
</div>
<div class="row align-items-sm-center align-items-lg-stretch">
<div class="col-md-12 col-lg-12">
<center>
  <h2>Tujuan</h2>
  </center>
<font color="black" size="5">{!! $tj->tujuan !!}</font>
 
</div>
</div>
<div class="row align-items-sm-center align-items-lg-stretch">
<div class="col-md-12 col-lg-12">
	<center>
		<h2>Struktur</h2>
<img class="card-img rounded-10" src="{{ asset('images/'.$st->gambar) }}" style="width:100%;max-height: auto;margin-bottom: 50px;" alt="">
</center>
<center>
  <h2>Sejarah</h2>
  </center>
<p><font color="black">{!! $t->tentang !!}</font></p>
 
</div>
</div>

</div>
</section>



@endsection
