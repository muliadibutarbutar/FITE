@extends('user.layout.master')

@section('content')

<section class="breadcrumb breadcrumb_bg" style="margin-top:115px;">
<div class="container">
<div class="row">
<div class="col-lg-12 mb-4">
<div class="breadcrumb_iner text-center">
<div class="breadcrumb_iner_item">
<h2>Info PMB</h2>
<p>Fakultas Informatika dan Teknik Elektro</p>
</div>
</div>
</div>
</div>
</div>
</section>


<section class="blog_area section_padding">
<div class="container">
<div class="row">
<div class="col-lg-12">
<div class="blog_left_sidebar">
<article class="blog_item" style="background-color: rgb(240, 240, 240);border-radius: 20px;">
<div class="blog_item_img">
<!-- <a href="#" class="blog_item_date">
<h3>15</h3>
<p>Jan</p>
</a> -->
</div>
<div class="blog_details">
<h2>Jadwal Pendaftaran</h2>
@foreach($ip as $f)
<h3><b><a href="{{$f->kategori->link}}" target="_blank">{{$f->kategori->nama}}</a></b>&emsp;{{date('d-m-Y', strtotime($f->jadwal1))}} - {{date('d-m-Y', strtotime($f->jadwal2))}}</h3>
@endforeach
<ul class="blog-info-link">
<li><a href="https://www.del.ac.id" target="_blank"><i class="far fa-user"></i> Institut Teknologi DEL</a></li>
</ul>
</div>
</article>
</div>
</div>
</div>
</div>
</section>

<section class="blog_area section_padding" style="margin-top:-250px">
<div class="container">
<div class="row">
<div class="col-lg-12">
<div class="blog_left_sidebar">
<article class="blog_item" style="background-color: rgb(240, 240, 240);border-radius: 20px;">
<div class="blog_item_img">
<!-- <a href="#" class="blog_item_date">
<h3>15</h3>
<p>Jan</p>
</a> -->
</div>
<div class="blog_details">
<h2>Jenis Tes</h2>
@foreach($ip as $f)
<h3>{{$loop->iteration}} &emsp; {{$f->jenis_tes}}</h3>
@endforeach
<ul class="blog-info-link">
<li><a href="https://www.del.ac.id" target="_blank"><i class="far fa-user"></i> Institut Teknologi DEL</a></li>
</ul>
</div>
</article>
</div>
</div>
</div>
</div>
</section>

<section class="blog_area section_padding" style="margin-top:-250px">
<div class="container">
<div class="row">
<div class="col-lg-12">
<div class="blog_left_sidebar">
<article class="blog_item" style="background-color: rgb(240, 240, 240);border-radius: 20px;">
<div class="blog_item_img">
<!-- <a href="#" class="blog_item_date">
<h3>15</h3>
<p>Jan</p>
</a> -->
</div>
<div class="blog_details">
<h2>Lokasi Tes</h2>
@foreach($lokasi as $l)
<h3>{{$l->nama}}</h3>
<p>{{$l->lokasi}}</p>
@endforeach

<ul class="blog-info-link">
<li><a href="https://www.del.ac.id" target="_blank"><i class="far fa-user"></i> Institut Teknologi DEL</a></li>
</ul>
</div>
</article>
</div>
</div>
</div>
</div>
</section>




</div>
</article>
</div>
</div>
</div>
</div>
</section>


</div>
</div>
</div>
</section>



@endsection
