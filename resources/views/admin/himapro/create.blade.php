@section('js')
    <script type="text/javascript">
        function readURL() {
            var input = this;
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $(input).prev().attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }

        $(function () {
            $(".uploads").change(readURL)
            $("#f").submit(function(){
                // do ajax submit or just classic form submit
              //  alert("fake subminting")
                return false
            })
        })
    </script>
@stop

@extends('layout.master')

@push('plugin-styles')
@endpush


@section('content')
<form action="/kemahasiswaan/himapro/add" method="post" enctype="multipart/form-data">
    {{ csrf_field() }}
<div class="row">
            <div class="col-md-12 d-flex align-items-stretch grid-margin">
              <div class="row flex-grow">
                <div class="col-12">
                  <div class="card">
                    <div class="card-body">
                      <h4 class="card-title">Tambah Himapro</h4>
                        <div class="form-group">
                            <label for="judul" class="col-md-4 control-label">Nama Himapro</label>
                            <div class="col-md-6">
                                <input id="judul" type="text" class="form-control" name="nama" placeholder="Masukkan Nama Himapro">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="email" class="col-md-4 control-label">Gambar</label>
                            <div class="col-md-6">
                                <img class="product" width="200" height="200" />
                                <input type="file" class="uploads form-control" style="margin-top: 20px;" name="gambar">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="deskripsi" class="col-md-4 control-label">Aktifitas</label>
                            <div class="col-md-12">
                                <textarea type="text" class="form-control" name="aktifitas" rows="5" placeholder="Aktifitas dari Himapro"></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="deskripsi" class="col-md-4 control-label">Deskripsi</label>
                            <div class="col-md-12">
                                <textarea type="text" class="form-control" name="deskripsi" rows="5" placeholder="Deskripsi dari Himapro"></textarea>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary" id="submit">
                                    Submit
                        </button>
                        <a href="/kemahasiswaan/himapro" class="btn btn-light pull-right" style="float:right;">Kembali</a>
                    </div>
                  </div>
                </div>
              </div>
            </div>

</div>
</form>


@endsection


@push('plugin-scripts')
@endpush

@push('custom-scripts')
@endpush

