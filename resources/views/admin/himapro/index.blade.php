@extends('layout.master')

@push('plugin-styles')
@endpush

@section('content')
<div class="row">
  <div class="col-lg-12 grid-margin stretch-card">
    <div class="card">
      <div class="card-body">
        <h4 class="card-title">Himapro</h4>
        <a href="/kemahasiswaan/himapro/create" class="btn btn-primary">Tambah Himapro</a>
        <hr>
        <div class="table-responsive">
          <table class="table table-hover" id="tableMaster">
            <thead>
              <tr>
                <th> No </th>
                <th> Nama Himapro </th>
                <th> Aktifitas </th>
                <th> Deskripsi</th>
                <th> Aksi </th>
              </tr>
            </thead>
            <tbody>
              @foreach ($bf as $u)
              <tr>
                <td class="py-1">
                  {{$loop->iteration}}
                </td>
                <td>
                  {{$u->nama}}
                </td>
                <td> 
                  {!! Str::limit($u->aktifitas, 50) !!}
                </td>
                <td>
                  {!! Str::limit($u->deskripsi,50)!!}
                </td>
                <td>
                  <a href="/kemahasiswaan/himapro/edit/{{$u->id}}" class="btn btn-warning">Edit</a>
                  <a href="/kemahasiswaan/himapro/hapus/{{$u->id}}" class="btn btn-danger">Hapus</a>
                </td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection

@push('plugin-scripts')
@endpush

@push('custom-scripts')
@endpush