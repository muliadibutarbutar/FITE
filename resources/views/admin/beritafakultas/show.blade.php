@extends('layout.master')

@push('plugin-styles')
@endpush


@section('content')
<div class="row">
  <div class="col-lg-12 grid-margin stretch-card">
    <div class="card">
      <div class="card-body">
        <h1 class="card-title">Detail Berita <h1><b>{{$bf->nama}}</b></h1></h1>
        <p class="card-description">
          <div class="zoom">
            <img width="200" height="200" src="{{ asset('images/'.$bf->gambar) }}"/>
          </div>
          {!! $bf->deskripsi !!}
          <br>
        <small>Posted By</small> 
          {{$bf->user->name}}
        </p>
        <a href="/master-data/beritafakultas" class="btn btn-light pull-right" style="float:right;">Kembali</a>
      </div>
    </div>
  </div>
</div>
@endsection

@push('plugin-scripts')
@endpush

@push('custom-scripts')
@endpush