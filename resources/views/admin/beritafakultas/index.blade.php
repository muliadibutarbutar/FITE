@extends('layout.master')

@push('plugin-styles')
@endpush

@section('content')
<div class="row">
  <div class="col-lg-12 grid-margin stretch-card">
    <div class="card">
      <div class="card-body">
        <h4 class="card-title">Berita Fakultas FITE</h4>
        <a href="/beritafakultas/create" class="btn btn-primary">Tambah Berita</a>
        <hr>
        <div class="table-responsive">
          <table class="table table-hover" id="tableMaster">
            <thead>
              <tr>
                <th> No </th>
                <th> Judul Berita </th>
                <th> Deskripsi </th>
                <th> Posted By</th>
                <th> Aksi </th>
              </tr>
            </thead>
            <tbody>
              @foreach ($bf as $u)
              <tr>
                <td class="py-1">
                  {{$loop->iteration}}
                </td>
                <td> 
                  <b>{{ Str::limit($u->nama, 50)}}</b>
                </td>
                <td>
                  {!! Str::limit($u->deskripsi,50)!!}
                </td>
                <td>
                  {{$u->user->name}}
                </td>
                <td>
                  <a href="/beritafakultas/show/{{$u->id}}" class="btn btn-secondary">Lihat</a>
                  <a href="/beritafakultas/edit/{{$u->id}}" class="btn btn-warning">Edit</a>
                  <a href="/beritafakultas/hapus/{{$u->id}}" class="btn btn-danger">Hapus</a>
                </td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection

@push('plugin-scripts')
@endpush

@push('custom-scripts')
@endpush