@extends('layout.master')

@push('plugin-styles')
@endpush


@section('content')
<div class="row">
  <div class="col-lg-12 grid-margin stretch-card">
    <h2>Tentang Fakultas Informatika dan Teknik Elektro</h2>
  </div>
  <div class="col-lg-12 grid-margin stretch-card">
    <a href="/admin-tentang/tentang/create" class="btn btn-primary">Tambah Tentang</a> &nbsp;&nbsp;
    <a href="/admin-tentang/tentang/edit/1" class="btn btn-warning">Edit Tentang</a>
  </div>
  <div class="col-lg-12 grid-margin stretch-card">
    <div class="card">
      <div class="card-body">
        <h4 class="card-title">Tentang</h4>
        <p class="card-description"> 
          {!! $t-> tentang !!}
        </p>
      </div>
    </div>
  </div>
</div>
@endsection

@push('plugin-scripts')
@endpush

@push('custom-scripts')
@endpush