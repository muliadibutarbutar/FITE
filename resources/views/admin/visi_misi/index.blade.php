@extends('layout.master')

@push('plugin-styles')
@endpush


@section('content')
<div class="row">
  <div class="col-lg-12 grid-margin stretch-card">
    <h2>Visi Misi Fakultas Informatika dan Teknik Elektro</h2>
  </div>
  <div class="col-lg-12 grid-margin stretch-card">
    <a href="/admin-tentang/visimisifakultas/create" class="btn btn-primary">Tambah Visi Misi</a> &nbsp;&nbsp;
    <a href="/admin-tentang/visimisifakultas/edit/1" class="btn btn-warning">Edit Visi Misi</a>
  </div>
  <div class="col-lg-6 grid-margin stretch-card">
    <div class="card">
      <div class="card-body">
        <h4 class="card-title">Visi</h4>
        <p class="card-description"> 
          {!! $vs-> visi!!}
        </p>
      </div>
    </div>
  </div>
  <div class="col-lg-6 grid-margin stretch-card">
    <div class="card">
      <div class="card-body">
        <h4 class="card-title">Misi</h4>
        <p class="card-description"> 
          {!! $vs-> misi !!}
        </p>
      </div>
    </div>
  </div>
</div>
@endsection

@push('plugin-scripts')
@endpush

@push('custom-scripts')
@endpush