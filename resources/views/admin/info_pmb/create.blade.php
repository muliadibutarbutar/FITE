@extends('layout.master')

@push('plugin-styles')
@endpush


@section('content')
<form action="/infopmb/infopmb/add" method="post" enctype="multipart/form-data">
    {{ csrf_field() }}
<div class="row">
            <div class="col-md-12 d-flex align-items-stretch grid-margin">
              <div class="row flex-grow">
                <div class="col-12">
                  <div class="card">
                    <div class="card-body">
                      <h4 class="card-title">Tambah Info PMB</h4>
                        <div class="form-group">
                            <label for="judul" class="col-md-4 control-label">Jadwal Daftar</label>
                            <div class="col-md-6">
                                <input id="judul" type="date" class="form-control" name="jadwal1" placeholder="Masukkan Tanggal">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="judul" class="col-md-4 control-label">Sampai</label>
                            <div class="col-md-6">
                                <input id="judul" type="date" class="form-control" name="jadwal2" placeholder="Masukkan Tanggal">
                            </div>
                        </div>
                         <div class="form-group">
                            <label for="kategori_id" class="col-md-4 control-label">Kategori</label>
                            <div class="col-md-6">
                                <div class="input-group">
                                <input id="kategori_nama" type="text" class="form-control" readonly="" >
                                <input id="kategori_id" type="hidden" name="id_usm" value="{{ old('kategori_id') }}" readonly="" required>
                                <span class="input-group-btn">
                                    <button type="button" class="btn btn-warning btn-secondary" data-toggle="modal" data-target="#myModal"><b>Cari Kategori</b> <span class="enu-icon mdi mdi-search"></span></button>
                                </span>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="judul" class="col-md-4 control-label">Jenis Tes</label>
                            <div class="col-md-6">
                                <input id="judul" type="text" class="form-control" name="jenis_tes" placeholder="Masukkan Tanggal">
                            </div>
                        </div>

                        <button type="submit" class="btn btn-primary" id="submit">
                                    Submit
                        </button>
                        <a href="/infopmb/infopmb" class="btn btn-light pull-right" style="float:right;">Kembali</a>
                    </div>
                  </div>
                </div>
              </div>
            </div>

</div>
</form>

 <!-- Modal -->
<div class="modal" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" >
  <div class="modal-dialog modal-lg modal-header" role="document" >
    <div class="modal-content" style="background: #fff;">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Cari Kategori</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
                        <table id="tableMaster" class="table table-bordered table-hover table-striped">
                            <thead>
                        <tr>
                            <th>
                            No
                          </th>
                          <th>
                            Nama
                          </th>
                        </tr>
                      </thead>
                            <tbody>
                              @foreach($ka as $k)
                                <tr class="pilih_kategori" data-kategori_id="<?php echo $k->id; ?>" data-kategori_nama="<?php echo $k->nama; ?>" >
                                    <td>{{$loop->iteration}}</td>
                                    <td class="py-1">
                            {{$k->nama}}
                          </td>
                        </tr>
                        @endforeach
                            </tbody>
                        </table>  
                    </div>
                </div>
            </div>
        </div>

@endsection


@push('plugin-scripts')
@endpush

@push('custom-scripts')
@endpush

