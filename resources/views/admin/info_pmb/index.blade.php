@extends('layout.master')

@push('plugin-styles')
@endpush

@section('content')
<div class="row">
  <div class="col-lg-12 grid-margin stretch-card">
    <div class="card">
      <div class="card-body">
        <h4 class="card-title">Daftar Info USM</h4>
        <a href="/infopmb/infopmb/create" class="btn btn-primary">Tambah Info PMB</a>
        <hr>
        <div class="table-responsive">
          <table class="table table-hover" id="tableMaster">
            <thead>
              <tr>
                <th> No </th>
                <th> Kategori </th>
                <th> Jadwal Pendaftaran </th>
                <th> Jenis Tes </th>
                <th> Aksi </th>
              </tr>
            </thead>
            <tbody>
              @foreach ($un as $u)
              <tr>
                <td class="py-1">
                  {{$loop->iteration}}
                </td>
                <td> 
                  <a href="{{$u->kategori->link}}" target="_blank">{{$u->kategori->nama}}</a>
                </td>
                <td> 
                  {{$u->jadwal1}} - {{$u->jadwal2}}
                </td>
                <td>
                  {{$u->jenis_tes}} 
                </td>
                <td>
                  <a href="/infopmb/infopmb/edit/{{$u->id}}" class="btn btn-warning">Edit</a>
                  <a href="/infopmb/infopmb/hapus/{{$u->id}}" class="btn btn-danger">Hapus</a>
                </td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection

@push('plugin-scripts')
@endpush

@push('custom-scripts')
@endpush