@extends('layout.master')

@push('plugin-styles')
@endpush

@section('content')
<form action="/user/add" method="post" enctype="multipart/form-data">
    {{ csrf_field() }}
<div class="row">
            <div class="col-md-12 d-flex align-items-stretch grid-margin">
              <div class="row flex-grow">
                <div class="col-12">
                  <div class="card">
                    <div class="card-body">
                      <h4 class="card-title">Tambah User Admin</h4>
                        <div class="form-group">
                            <label for="judul" class="col-md-4 control-label">Nama User</label>
                            <div class="col-md-6">
                                <input id="judul" type="text" class="form-control" name="name" placeholder="Nama Program Studi">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="judul" class="col-md-4 control-label">Email</label>
                            <div class="col-md-6">
                                <input id="judul" type="text" class="form-control" name="email" placeholder="Kode Program Studi">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="password" class="col-md-4 control-label">Password</label>
                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" onkeyup='check();' name="password" placeholder="Password">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="password-confirm" class="col-md-4 control-label">Confirm Password</label>
                            <div class="col-md-6">
                                <input id="confirm_password" type="password" onkeyup="check()" class="form-control" name="password_confirmation" placeholder="Confirmation Password">
                                 <span id='message'></span>
                            </div>
                        </div>

                        <button type="submit" class="btn btn-primary" id="submit">
                                    Submit
                        </button>
                        <a href="/user" class="btn btn-light pull-right" style="float:right;">Kembali</a>
                    </div>
                  </div>
                </div>
              </div>
            </div>

</div>
</form>
@endsection


@push('plugin-scripts')
@endpush

@push('custom-scripts')
@endpush