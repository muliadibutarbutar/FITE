@extends('layout.master')

@push('plugin-styles')
@endpush


@section('content')
<form action="/user/update/{{$user->id}}" method="post" enctype="multipart/form-data">
    {{ csrf_field() }}
<div class="row">
            <div class="col-md-12 d-flex align-items-stretch grid-margin">
              <div class="row flex-grow">
                <div class="col-12">
                  <div class="card">
                    <div class="card-body">
                      <h4 class="card-title">Edit User {{$user->name}}</h4>
                        <div class="form-group">
                            <label for="judul" class="col-md-4 control-label">Nama User</label>
                            <div class="col-md-6">
                                <input id="judul" type="text" class="form-control" value="{{$user->name}}" name="name" placeholder="Nama User">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="judul" class="col-md-4 control-label">Email</label>
                            <div class="col-md-6">
                                <input id="judul" type="text" class="form-control" value="{{$user->email}}" name="email" placeholder="Email User">
                            </div>
                        </div>

                        <button type="submit" class="btn btn-primary" id="submit">
                                    Submit
                        </button>
                        <a href="/user" class="btn btn-light pull-right" style="float:right;">Kembali</a>
                    </div>
                  </div>
                </div>
              </div>
            </div>

</div>
</form>
@endsection


@push('plugin-scripts')
@endpush

@push('custom-scripts')
@endpush