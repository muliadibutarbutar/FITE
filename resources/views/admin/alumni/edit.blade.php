@section('js')
    <script type="text/javascript">
        function readURL() {
            var input = this;
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $(input).prev().attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }

        $(function () {
            $(".uploads").change(readURL)
            $("#f").submit(function(){
                // do ajax submit or just classic form submit
              //  alert("fake subminting")
                return false
            })
        })


var check = function() {
  if (document.getElementById('password').value ==
    document.getElementById('confirm_password').value) {
    document.getElementById('submit').disabled = false;
    document.getElementById('message').style.color = 'green';
    document.getElementById('message').innerHTML = 'matching';
  } else {
    document.getElementById('submit').disabled = true;
    document.getElementById('message').style.color = 'red';
    document.getElementById('message').innerHTML = 'not matching';
  }
}
    </script>
@stop

@extends('layout.master')

@push('plugin-styles')
@endpush


@section('content')
<form action="/alumni/alumni/update/{{$bf->id}}" method="post" enctype="multipart/form-data">
    {{ csrf_field() }}
<div class="row">
            <div class="col-md-12 d-flex align-items-stretch grid-margin">
              <div class="row flex-grow">
                <div class="col-12">
                  <div class="card">
                    <div class="card-body">
                      <h4 class="card-title">Edit Data Alumni {{$bf->nama}}</h4>
                        <div class="form-group">
                            <label for="judul" class="col-md-4 control-label">Nama Alumni</label>
                            <div class="col-md-6">
                                <input id="judul" type="text" class="form-control" name="nama" placeholder="Masukkan Nama Alumni" value="{{$bf->nama}}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="email" class="col-md-4 control-label">Testimoni</label>
                            <div class="col-md-6">
                                <img width="200" height="200" @if($bf->testimoni) src="{{ asset('images/'.$bf->testimoni) }}" @endif/>
                                <input type="file" class="uploads form-control" style="margin-top: 20px;" name="testimoni" value="{{$bf->testimoni}}"/>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="judul" class="col-md-4 control-label">Tahun Lulus</label>
                            <div class="col-md-6">
                                <input id="judul" type="text" class="form-control" name="tahun_lulus" placeholder="Masukkan Tahun Lulus" value="{{$bf->tahun_lulus}}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="judul" class="col-md-4 control-label">Link Instagram</label>
                            <div class="col-md-6">
                                <input id="judul" type="text" class="form-control" name="link" placeholder="Masukkan Link Instagram" value="{{$bf->link}}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="kategori_id" class="col-md-4 control-label">Program Studi</label>
                            <div class="col-md-6">
                                <div class="input-group">
                                <input id="kategori_nama" type="text" class="form-control" readonly="" value="{{$bf->prodi->nama_prodi}}">
                                <input id="kategori_id" type="hidden" name="id_prodi" value="{{ $bf->id_prodi}}" readonly="" required>
                                <span class="input-group-btn">
                                    <button type="button" class="btn btn-warning btn-secondary" data-toggle="modal" data-target="#myModal"><b>Cari Program Studi</b> <span class="enu-icon mdi mdi-search"></span></button>
                                </span>
                                </div>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary" id="submit">
                                    Submit
                        </button>
                        <a href="/alumni/alumni" class="btn btn-light pull-right" style="float:right;">Kembali</a>
                    </div>
                  </div>
                </div>
              </div>
            </div>

</div>
</form>

<!-- Modal -->
<div class="modal" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" >
  <div class="modal-dialog modal-lg modal-header" role="document" >
    <div class="modal-content" style="background: #fff;">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Cari Kategori</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
                        <table id="tableMaster" class="table table-bordered table-hover table-striped">
                            <thead>
                        <tr>
                            <th>
                            No
                          </th>
                          <th>
                            Nama
                          </th>
                        </tr>
                      </thead>
                            <tbody>
                              @foreach($ka as $k)
                                <tr class="pilih_kategori" data-kategori_id="<?php echo $k->id; ?>" data-kategori_nama="<?php echo $k->nama_prodi; ?>" >
                                    <td>{{$loop->iteration}}</td>
                                    <td class="py-1">
                            {{$k->nama_prodi}}
                          </td>
                        </tr>
                        @endforeach
                            </tbody>
                        </table>  
                    </div>
                </div>
            </div>
        </div>

@endsection


@push('plugin-scripts')
@endpush

@push('custom-scripts')
@endpush