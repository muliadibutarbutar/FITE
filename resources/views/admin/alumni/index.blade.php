@extends('layout.master')

@push('plugin-styles')
@endpush

@section('content')
<div class="row">
  <div class="col-lg-12 grid-margin stretch-card">
    <div class="card">
      <div class="card-body">
        <h4 class="card-title">Alumni FITE</h4>
        <a href="/alumni/alumni/create" class="btn btn-primary">Tambah Alumni</a>
        <hr>
        <div class="table-responsive">
          <table class="table table-hover" id="tableMaster">
            <thead>
              <tr>
                <th> No </th>
                <th> Nama Alumni </th>
                <th> Tahun Lulus </th>
                <th> Program Studi </th>
                <th> Aksi </th>
              </tr>
            </thead>
            <tbody>
              @foreach ($bf as $u)
              <tr>
                <td class="py-1">
                  {{$loop->iteration}}
                </td>
                <td> 
                  <b>{{ $u->nama }}</b>
                </td>
                <td>
                  {{$u->tahun_lulus}}
                </td>
                <td>
                  {{$u->prodi->nama_prodi}}
                </td>
                <td>
                  <a href="/alumni/alumni/edit/{{$u->id}}" class="btn btn-warning">Edit</a>
                  <a href="/alumni/alumni/hapus/{{$u->id}}" class="btn btn-danger">Hapus</a>
                </td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection

@push('plugin-scripts')
@endpush

@push('custom-scripts')
@endpush