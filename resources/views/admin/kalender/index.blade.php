@extends('layout.master')

@push('plugin-styles')
@endpush

@section('content')
<div class="row">
  <div class="col-lg-12 grid-margin stretch-card">
    <div class="card">
      <div class="card-body">
        <h4 class="card-title">Daftar Kalender</h4>
        <a href="/akademik/kalender/create" class="btn btn-primary">Tambah Kalender</a>
        <hr>
        <div class="table-responsive">
          <table class="table table-hover" id="tableMaster">
            <thead>
              <tr>
                <th> No </th>
                <th> Nama File (Dokumen) </th>
                <th> Dokumen </th>
                <th> Aksi </th>
              </tr>
            </thead>
            <tbody>
              @foreach ($un as $u)
              <tr>
                <td class="py-1">
                  {{$loop->iteration}}
                </td>
                <td> 
                  {{$u->nama}}
                </td>
                <td> 
                  <a href="{{ asset('images/'.$u->kalender) }}" target="_blank">{{$u->kalender}}</a>
                </td>
                <td>
                  <a href="/akademik/kalender/edit/{{$u->id}}" class="btn btn-warning">Edit</a>
                  <a href="/akademik/kalender/hapus/{{$u->id}}" class="btn btn-danger">Hapus</a>
                </td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection

@push('plugin-scripts')
@endpush

@push('custom-scripts')
@endpush