@extends('layout.master')

@push('plugin-styles')
@endpush


@section('content')
<form action="/akademik/kalender/add" method="post" enctype="multipart/form-data">
    {{ csrf_field() }}
<div class="row">
            <div class="col-md-12 d-flex align-items-stretch grid-margin">
              <div class="row flex-grow">
                <div class="col-12">
                  <div class="card">
                    <div class="card-body">
                      <h4 class="card-title">Tambah Kalender</h4>
                        <div class="form-group">
                            <label for="judul" class="col-md-4 control-label">Nama Kalender</label>
                            <div class="col-md-6">
                                <input id="judul" type="text" class="form-control" name="nama" placeholder="Masukkan Nama Dokumen">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="email" class="col-md-4 control-label">Dokumen Kalender</label>
                            <div class="col-md-6">
                                <input type="file" class="form-control" name="kalender">
                            </div>
                        </div>

                        <button type="submit" class="btn btn-primary" id="submit">
                                    Submit
                        </button>
                        <a href="/akademik/kalender" class="btn btn-light pull-right" style="float:right;">Kembali</a>
                    </div>
                  </div>
                </div>
              </div>
            </div>

</div>
</form>

 
@endsection


@push('plugin-scripts')
@endpush

@push('custom-scripts')
@endpush

