@section('js')
    <script type="text/javascript">
        function readURL() {
            var input = this;
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $(input).prev().attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }

        $(function () {
            $(".uploads").change(readURL)
            $("#f").submit(function(){
                // do ajax submit or just classic form submit
              //  alert("fake subminting")
                return false
            })
        })
    </script>
@stop

@extends('layout.master')

@push('plugin-styles')
@endpush


@section('content')
<form action="/akademik/kurikulum/add" method="post" enctype="multipart/form-data">
    {{ csrf_field() }}
<div class="row">
            <div class="col-md-12 d-flex align-items-stretch grid-margin">
              <div class="row flex-grow">
                <div class="col-12">
                  <div class="card">
                    <div class="card-body">
                      <h4 class="card-title">Tambah Kurikulum</h4>
                         <div class="form-group">
                            <label for="kategori_id" class="col-md-4 control-label">Program Studi</label>
                            <div class="col-md-6">
                                <div class="input-group">
                                <input id="kategori_nama" type="text" class="form-control" readonly="" >
                                <input id="kategori_id" type="hidden" name="id_prodi" value="{{ old('kategori_id') }}" readonly="" required>
                                <span class="input-group-btn">
                                    <button type="button" class="btn btn-warning btn-secondary" data-toggle="modal" data-target="#myModal"><b>Cari Program Studi</b> <span class="enu-icon mdi mdi-search"></span></button>
                                </span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="tahun_id" class="col-md-4 control-label">Tahun</label>
                            <div class="col-md-6">
                                <div class="input-group">
                                <input id="tahun_nama" type="text" class="form-control" readonly="" >
                                <input id="tahun_id" type="hidden" name="id_tahun" value="{{ old('tahun_id') }}" readonly="" required>
                                <span class="input-group-btn">
                                    <button type="button" class="btn btn-warning btn-secondary" data-toggle="modal" data-target="#myModal2"><b>Cari Tahun</b> <span class="enu-icon mdi mdi-search"></span></button>
                                </span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="email" class="col-md-4 control-label">Dokumen (PDF)</label>
                            <div class="col-md-6">
                                <input type="file" class="uploads form-control" style="margin-top: 20px;" name="pdf">
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary" id="submit">
                                    Submit
                        </button>
                        <a href="/akademik/kurikulum" class="btn btn-light pull-right" style="float:right;">Kembali</a>
                    </div>
                  </div>
                </div>
              </div>
            </div>

</div>
</form>

 <!-- Modal -->
<div class="modal" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" >
  <div class="modal-dialog modal-lg modal-header" role="document" >
    <div class="modal-content" style="background: #fff;">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Cari Program Studi</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
                        <table id="tableMaster" class="table table-bordered table-hover table-striped">
                            <thead>
                        <tr>
                            <th>
                            No
                          </th>
                          <th>
                            Nama
                          </th>
                        </tr>
                      </thead>
                            <tbody>
                              @foreach($ka as $k)
                                <tr class="pilih_kategori" data-kategori_id="<?php echo $k->id; ?>" data-kategori_nama="<?php echo $k->nama_prodi; ?>" >
                                    <td>{{$loop->iteration}}</td>
                                    <td class="py-1">
                            {{$k->nama_prodi}}
                          </td>
                        </tr>
                        @endforeach
                            </tbody>
                        </table>  
                    </div>
                </div>
            </div>
        </div>


 <!-- Modal -->
<div class="modal" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" >
  <div class="modal-dialog modal-lg modal-header" role="document" >
    <div class="modal-content" style="background: #fff;">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Cari Program Studi</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
                        <table id="tableMaster" class="table table-bordered table-hover table-striped">
                            <thead>
                        <tr>
                            <th>
                            No
                          </th>
                          <th>
                            Nama
                          </th>
                        </tr>
                      </thead>
                            <tbody>
                              @foreach($ta as $k)
                                <tr class="pilih_tahun" data-tahun_id="<?php echo $k->id; ?>" data-tahun_nama="<?php echo $k->tahun; ?>" >
                                    <td>{{$loop->iteration}}</td>
                                    <td class="py-1">
                            {{$k->tahun}}
                          </td>
                        </tr>
                        @endforeach
                            </tbody>
                        </table>  
                    </div>
                </div>
            </div>
        </div>
@endsection


@push('plugin-scripts')
@endpush

@push('custom-scripts')
@endpush

