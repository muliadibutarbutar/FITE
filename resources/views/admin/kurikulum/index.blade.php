@extends('layout.master')

@push('plugin-styles')
@endpush

@section('content')
<div class="row">
  <div class="col-lg-12 grid-margin stretch-card">
    <div class="card">
      <div class="card-body">
        <h4 class="card-title">Daftar Kurikulum</h4>
        <a href="/akademik/kurikulum/create" class="btn btn-primary">Tambah Daftar Kurikulum</a>
        <hr>
        <div class="table-responsive">
          <table class="table table-hover" id="tableMaster">
            <thead>
              <tr>
                <th> No </th>
                <th> Program Studi </th>
                <th> Tahun </th>
                <th> Dokumen </th>
                <th> Aksi </th>
              </tr>
            </thead>
            <tbody>
              @foreach ($k as $u)
              <tr>
                <td class="py-1">
                  {{$loop->iteration}}
                </td>
                <td> 
                  {{$u->prodi->nama_prodi}}
                </td>
                <td> 
                  {{$u->tahun->tahun}}
                </td>
                <td>
                  <a href="{{ asset('images/'.$u->pdf) }}" target="_blank">{{$u->pdf}}</a>
                </td>
                <td>
                  <a href="/akademik/kurikulum/edit/{{$u->id}}" class="btn btn-warning">Edit</a>
                  <a href="/akademik/kurikulum/hapus/{{$u->id}}" class="btn btn-danger">Hapus</a>
                </td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection

@push('plugin-scripts')
@endpush

@push('custom-scripts')
@endpush