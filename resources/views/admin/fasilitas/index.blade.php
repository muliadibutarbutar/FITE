@extends('layout.master')

@push('plugin-styles')
@endpush

@section('content')
<div class="row">
  <div class="col-lg-12 grid-margin stretch-card">
    <div class="card">
      <div class="card-body">
        <h4 class="card-title">Daftar Fasilitas</h4>
        <a href="/fasilitas/fasilitas/create" class="btn btn-primary">Tambah Fasilitas</a>
        <hr>
        <div class="table-responsive">
          <table class="table table-hover" id="tableMaster">
            <thead>
              <tr>
                <th> No </th>
                <th> Nama Fasilitas </th>
                <th> Kategori </th>
                <th> Deskripsi </th>
                <th> Aksi </th>
              </tr>
            </thead>
            <tbody>
              @foreach ($fs as $u)
              <tr>
                <td class="py-1">
                  {{$loop->iteration}}
                </td>
                <td> 
                  {{$u->nama}}
                </td>
                <td> 
                  {{$u->kategori->kategori}}
                </td>
                <td>
                  {!!  Str::limit($u->deskripsi,50) !!}
                </td>
                <td>
                  <a href="/fasilitas/fasilitas/edit/{{$u->id}}" class="btn btn-warning">Edit</a>
                  <a href="/fasilitas/fasilitas/hapus/{{$u->id}}" class="btn btn-danger">Hapus</a>
                </td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection

@push('plugin-scripts')
@endpush

@push('custom-scripts')
@endpush