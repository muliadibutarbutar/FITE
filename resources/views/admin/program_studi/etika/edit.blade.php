@extends('layout.master')

@push('plugin-styles')
@endpush


@section('content')
<form action="/programstudi/etikaprodi/update/{{$ps->id}}" method="post" enctype="multipart/form-data">
    {{ csrf_field() }}
<div class="row">
            <div class="col-md-12 d-flex align-items-stretch grid-margin">
              <div class="row flex-grow">
                <div class="col-12">
                  <div class="card">
                    <div class="card-body">
                      <h4>Edit</h4>
                        <div class="form-group">
                            <label for="deskripsi" class="col-md-4 control-label">Etika</label>
                            <div class="col-md-12">
                                <textarea type="text" class="form-control" name="etika_prodi" rows="5" placehol_prodider="Deskripsi dari Etika" value="{{$ps->etika}}">{{$ps->etika_prodi }}</textarea>
                            </div>
                        </div>
                        <input type="hidden" name="id_prodi" value="{{$ps->id_prodi}}">
                        <button type="submit" class="btn btn-primary" id="submit">
                                    Submit
                        </button>
                        <a href="/programstudi/programstudi" class="btn btn-light pull-right" style="float:right;">Kembali</a>
                    </div>
                  </div>
                </div>
              </div>
            </div>

</div>
</form>
@endsection


@push('plugin-scripts')
@endpush

@push('custom-scripts')
@endpush