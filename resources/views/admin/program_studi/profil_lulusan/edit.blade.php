@extends('layout.master')

@push('plugin-styles')
@endpush


@section('content')
<form action="/programstudi/profillulusan/update/{{$p->id}}" method="post" enctype="multipart/form-data">
    {{ csrf_field() }}
<div class="row">
            <div class="col-md-12 d-flex align-items-stretch grid-margin">
              <div class="row flex-grow">
                <div class="col-12">
                  <div class="card">
                    <div class="card-body">
                      <h4 class="card-title">Edit Profil Lulusan {{$p->profil_lulusan}}</h4>
                        <div class="form-group">
                            <label for="judul" class="col-md-4 control-label">Profil Lulusan</label>
                            <div class="col-md-6">
                                <input id="judul" type="text" class="form-control" value="{{$p->profil_lulusan}}" name="profil_lulusan" placeholder="Profil Lulusan">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="judul" class="col-md-4 control-label">Jenis Pekerjaan</label>
                            <div class="col-md-6">
                                <input id="judul" type="text" class="form-control" value="{{$p->jenis_pekerjaan}}" name="jenis_pekerjaan" placeholder="Jenis Pekerjaan">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="deskripsi" class="col-md-4 control-label">Deskripsi</label>
                            <div class="col-md-12">
                                <textarea type="text" class="form-control" name="deskripsi" rows="5" placeholder="Deskripsi dari Profil Lulusan" value="{{$p->deskripsi}}">{{$p->deskripsi}}</textarea>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary" id="submit">
                                    Submit
                        </button>
                        <a href="/programstudi/programstudi" class="btn btn-light pull-right" style="float:right;">Kembali</a>
                    </div>
                  </div>
                </div>
              </div>
            </div>

</div>
</form>
@endsection


@push('plugin-scripts')
@endpush

@push('custom-scripts')
@endpush