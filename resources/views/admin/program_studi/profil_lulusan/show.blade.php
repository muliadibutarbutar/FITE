@extends('layout.master')

@push('plugin-styles')
@endpush


@section('content')
<div class="row">
  <div class="col-lg-12 grid-margin stretch-card">
    <div class="card">
      <div class="card-body">
        <h1 class="card-title">Detail Profil Lulusan <h1><b>{{$p->profil_lulusan}}</b></h1></h1>
        <p class="card-description">
          <div class="row">
          <table class="table table-responsive table-hover">
            <tr>
              <td>Profil Lulusan</td>
              <td>:</td>
              <td>{{$p->profil_lulusan}}</td>
            </tr>
            <td>Jenis Pekerjaan</td>
              <td>:</td>
              <td>{{$p->jenis_pekerjaan}}</td>
            </tr>
            <td>Deskripsi</td>
              <td>:</td>
              <td>{!! $p->deskripsi !!}</td>
            </tr>
          </table>
          </div>
        </p>
        <a href="/programstudi/programstudi" class="btn btn-warning pull-right" style="float:right;">Kembali</a>
      </div>
    </div>
  </div>
</div>
@endsection

@push('plugin-scripts')
@endpush

@push('custom-scripts')
@endpush