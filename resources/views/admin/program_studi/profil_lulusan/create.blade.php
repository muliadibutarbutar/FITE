@extends('layout.master')

@push('plugin-styles')
@endpush


@section('content')
<form action="/programstudi/profillulusan/add" method="post" enctype="multipart/form-data">
    {{ csrf_field() }}
<div class="row">
            <div class="col-md-12 d-flex align-items-stretch grid-margin">
              <div class="row flex-grow">
                <div class="col-12">
                  <div class="card">
                    <div class="card-body">
                      <h2>Tambah Profil Lulusan</h2>
                      <form>
                        <div class="form-group">
                          <label for="deskripsi" class="col-md-4 control-label">Profil Lulusan</label>
                          <input type="text" class="form-control" name="profil_lulusan">
                        </div>
                        <div class="form-group">
                          <label for="deskripsi" class="col-md-4 control-label">Jenis Pekerjaan</label>
                          <input type="text" class="form-control" name="jenis_pekerjaan">
                        </div>
                        <div class="form-group">
                            <label for="deskripsi" class="col-md-4 control-label">Deskripsi</label>
                            <div class="col-md-12">
                                <textarea type="text" class="form-control" name="deskripsi" rows="5" placeholder="Deskripsi dari Profil Lulusan"></textarea>
                            </div>
                        </div>
                          <input type="hidden" name="id_prodi" value="{{$ps->id}}">
                        <button type="submit" class="btn btn-primary">Submit</button>
                      </form>
                        
                        <a href="/programstudi/programstudi" class="btn btn-warning pull-right" style="float:right;">Kembali</a>
                    </div>
                  </div>
                </div>
              </div>
            </div>

</div>
</form>
@endsection


@push('plugin-scripts')
@endpush

@push('custom-scripts')
@endpush