@extends('layout.master')

@push('plugin-styles')
@endpush


@section('content')
<form action="/programstudi/tujuanprodi/add" method="post" enctype="multipart/form-data">
    {{ csrf_field() }}
<div class="row">
            <div class="col-md-12 d-flex align-items-stretch grid-margin">
              <div class="row flex-grow">
                <div class="col-12">
                  <div class="card">
                    <div class="card-body">
                      <h4>Tambah</h4>
                        <div class="form-group">
                            <label for="deskripsi" class="col-md-4 control-label">Visi</label>
                            <div class="col-md-12">
                                <textarea type="text" class="form-control" name="tujuan" rows="5" placeholder="Deskripsi dari Tujuan"></textarea>
                            </div>
                        </div>
                        <input type="hidden" name="id_prodi" value="{{$ps->id}}">
                        <button type="submit" class="btn btn-primary" id="submit">
                                    Submit
                        </button>
                        <a href="/programstudi/programstudi" class="btn btn-light pull-right" style="float:right;">Kembali</a>
                    </div>
                  </div>
                </div>
              </div>
            </div>

</div>
</form>
@endsection


@push('plugin-scripts')
@endpush

@push('custom-scripts')
@endpush