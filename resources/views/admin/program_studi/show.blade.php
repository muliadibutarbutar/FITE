@extends('layout.master')

@push('plugin-styles')
@endpush


@section('content')
<div class="row">
  <div class="col-lg-12 grid-margin stretch-card">
    <h2>{{$ps->nama_prodi}}</h2>
  </div>
  <div class="col-lg-12 grid-margin stretch-card">
    <h3>Visi Misi</h3>
  </div>
  <div class="col-lg-12 grid-margin stretch-card">
    <a href="/programstudi/visimisiprodi/{{$ps->id}}/create" class="btn btn-primary">Tambah Visi Misi</a> &nbsp;&nbsp;
    <a href="/programstudi/visimisiprodi/{{$ps->id}}/edit" class="btn btn-warning">Edit Visi Misi</a>
  </div>
  <div class="col-lg-6 grid-margin stretch-card">
    <div class="card">
      <div class="card-body">
        <h4 class="card-title">Visi</h4>
        <p class="card-description"> 
          @foreach($visimisiprodi as $vs)
          {!! $vs->visi !!}
          @endforeach
        </p>
      </div>
    </div>
  </div>
  <div class="col-lg-6 grid-margin stretch-card">
    <div class="card">
      <div class="card-body">
        <h4 class="card-title">Misi</h4>
        <p class="card-description"> 
          @foreach($visimisiprodi as $vs)
          {!! $vs->misi !!}
          @endforeach
        </p>
      </div>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-lg-12 grid-margin stretch-card">
    <h3>Tujuan</h3>
  </div>
  <div class="col-lg-12 grid-margin stretch-card">
    <a href="/programstudi/tujuanprodi/{{$ps->id}}/create" class="btn btn-primary">Tambah Tujuan</a> &nbsp;&nbsp;
    <a href="/programstudi/visimisifakultas/{{$ps->id}}/edit" class="btn btn-warning">Edit Tujuan</a>
  </div>
  <div class="col-lg-12 grid-margin stretch-card">
    <div class="card">
      <div class="card-body">
        <h4 class="card-title">Tujuan</h4>
        <p class="card-description">
          @foreach($tujuanprodi as $tj) 
          {!! $tj->tujuan !!}
          @endforeach
        </p>
      </div>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-lg-12 grid-margin stretch-card">
    <h3>Kompetensi</h3>
  </div>
  <div class="col-lg-12 grid-margin stretch-card">
    <a href="/programstudi/kompetensiprodi/{{$ps->id}}/create" class="btn btn-primary">Tambah Kompetensi</a> &nbsp;&nbsp;
    <a href="/programstudi/kompetensiprodi/{{$ps->id}}/edit" class="btn btn-warning">Edit Kompetensi</a>
  </div>
  <div class="col-lg-12 grid-margin stretch-card">
    <div class="card">
      <div class="card-body">
        <h4 class="card-title">Kompetensi</h4>
        <p class="card-description">
            @foreach($kompetensiprodi as $kp) 
            {!! $kp->kompetensi !!}
            @endforeach
        </p>
      </div>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-lg-12 grid-margin stretch-card">
    <h3>Etika</h3>
  </div>
  <div class="col-lg-12 grid-margin stretch-card">
    <a href="/programstudi/etikaprodi/{{$ps->id}}/create" class="btn btn-primary">Tambah Etika</a> &nbsp;&nbsp;
    <a href="/programstudi/etikaprodi/{{$ps->id}}/edit" class="btn btn-warning">Edit Etika</a>
  </div>
  <div class="col-lg-12 grid-margin stretch-card">
    <div class="card">
      <div class="card-body">
        <h4 class="card-title">Etika</h4>
        <p class="card-description">
        @foreach($etikaprodi as $ep) 
          {!! $ep->etika_prodi !!}
          @endforeach
        </p>
      </div>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-lg-12 grid-margin stretch-card">
    <h3>Dosen Prodi</h3>
  </div>
  <div class="col-lg-12 grid-margin stretch-card">
    <a href="/programstudi/visimisifakultas/create" class="btn btn-primary">Tambah Dosen Prodi</a> &nbsp;&nbsp;
    <a href="/programstudi/visimisifakultas/edit/1" class="btn btn-warning">Edit Dosen Prodi</a>
  </div>
  <div class="col-lg-12 grid-margin stretch-card">
    <div class="card">
      <div class="card-body">
        <h4 class="card-title">Dosen Prodi</h4>
        <p class="card-description"> 
          
        </p>
      </div>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-lg-12 grid-margin stretch-card">
    <h3>Profil Lulusan</h3>
  </div>
  <div class="col-lg-12 grid-margin stretch-card">
    <div class="card">
      <div class="card-body">
        <h4 class="card-title">Berikut ada data profil lulusan dari dari {{$ps->nama_prodi}}</h4>
        <a href="/programstudi/profillulusan/{{$ps->id}}/create" class="btn btn-primary">Tambah Profil Lulusan</a>
        <p class="card-description"> 
          <div class="table-responsive">
          <table class="table table-hover">
            <thead>
              <tr>
                <th> No </th>
                <th> Profil Lulusan </th>
                <th> Jenis Pekerjaan </th>
                <th> Deskripsi </th>
                <th> Aksi </th>
              </tr>
            </thead>
            <tbody>
              @foreach ($profillulusan as $p)
              <tr>
                <td class="py-1">
                  {{$loop->iteration}}  
                </td>
                <td> 
                  {{$p->profil_lulusan}} 
                </td>
                <td>
                  {{ Str::limit($p->jenis_pekerjaan,50)}}
                </td>
                <td> {!! Str::limit($p->deskripsi,50)!!} </td>
                <td> 
                  <a href="/programstudi/profillulusan/{{$p->id}}/show" class="btn btn-success">Lihat</a>
                  <a href="/programstudi/profillulusan/{{$p->id}}/edit" class="btn btn-warning">Edit</a>
                  <a href="/programstudi/profillulusan/{{$p->id}}/hapus" class="btn btn-danger">Hapus</a> 
                </td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
        </p>
      </div>
    </div>
  </div>
</div>
@endsection

@push('plugin-scripts')
@endpush

@push('custom-scripts')
@endpush