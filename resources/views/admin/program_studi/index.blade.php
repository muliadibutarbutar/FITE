@extends('layout.master')

@push('plugin-styles')
@endpush

@section('content')
<div class="row">
  <div class="col-lg-12 grid-margin stretch-card">
    <div class="card">
      <div class="card-body">
        <h4 class="card-title">Program Studi FITE</h4>
        <a href="/programstudi/programstudi/create" class="btn btn-primary">Tambah Program Studi</a><hr>
        <div class="table-responsive">
          <table class="table table-hover" id="tableMaster">
            <thead>
              <tr>
                <th> No </th>
                <th> Kode Program Studi </th>
                <th> Program Studi </th>
                <th> Aksi </th>
              </tr>
            </thead>
            <tbody>
              @foreach($ps as $p)
              <tr>
                <td class="py-1">
                  {{ $loop->iteration}}
                </td>
                <td> {{ $p->kode_prodi }} </td>
                <td>
                  {{ $p->nama_prodi }}
                </td>
                <td>
                  <a href="/programstudi/programstudi/edit/{{$p->id}}" class="btn btn-warning">Edit</a>
                  <a href="/programstudi/programstudi/hapus/{{$p->id}}" class="btn btn-danger">Hapus</a>
                  <a href="/programstudi/programstudi/show/{{$p->id}}" class="btn btn-success">Informasi</a>
                </td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection

@push('plugin-scripts')
@endpush

@push('custom-scripts')
@endpush