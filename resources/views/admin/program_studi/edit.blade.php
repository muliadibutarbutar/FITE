@extends('layout.master')

@push('plugin-styles')
@endpush


@section('content')
<form action="/programstudi/programstudi/update/{{$ps->id}}" method="post" enctype="multipart/form-data">
    {{ csrf_field() }}
<div class="row">
            <div class="col-md-12 d-flex align-items-stretch grid-margin">
              <div class="row flex-grow">
                <div class="col-12">
                  <div class="card">
                    <div class="card-body">
                      <h4 class="card-title">Edit Program Studi {{$ps->nama_prodi}}</h4>
                        <div class="form-group">
                            <label for="judul" class="col-md-4 control-label">Kode Program Studi</label>
                            <div class="col-md-6">
                                <input id="judul" type="text" class="form-control" value="{{$ps->kode_prodi}}" name="kode_prodi" placeholder="Nama Program Studi">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="judul" class="col-md-4 control-label">Program Studi</label>
                            <div class="col-md-6">
                                <input id="judul" type="text" class="form-control" value="{{$ps->nama_prodi}}" name="nama_prodi" placeholder="Kode Program Studi">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="deskripsi" class="col-md-4 control-label">Deskripsi</label>
                            <div class="col-md-12">
                                <textarea type="text" class="form-control" name="deskripsi" rows="5" placeholder="Deskripsi dari Program Studi">{{ $ps->deskripsi }}</textarea>
                            </div>
                        </div>

                        <button type="submit" class="btn btn-primary" id="submit">
                                    Submit
                        </button>
                        <a href="/programstudi/programstudi" class="btn btn-light pull-right" style="float:right;">Kembali</a>
                    </div>
                  </div>
                </div>
              </div>
            </div>

</div>
</form>
@endsection


@push('plugin-scripts')
@endpush

@push('custom-scripts')
@endpush