@extends('layout.master')

@push('plugin-styles')
@endpush

@section('content')
<div class="row">
  <div class="col-lg-12 grid-margin stretch-card">
    <div class="card">
      <div class="card-body">
        <h4 class="card-title">Daftar Fasilitas</h4>
        <a href="/unduhan/kategoriunduhan/create" class="btn btn-primary">Tambah Kategori Unduhan</a>
        <hr>
        <div class="table-responsive">
          <table class="table table-hover" id="tableMaster">
            <thead>
              <tr>
                <th> No </th>
                <th> Nama Kategori </th>
                <th> Aksi </th>
              </tr>
            </thead>
            <tbody>
              @foreach ($k as $u)
              <tr>
                <td class="py-1">
                  {{$loop->iteration}}
                </td>
                <td> 
                  {{$u->nama}}
                </td>
                <td>
                  <a href="/unduhan/kategoriunduhan/edit/{{$u->id}}" class="btn btn-warning">Edit</a>
                  <a href="/unduhan/kategoriunduhan/hapus/{{$u->id}}" class="btn btn-danger">Hapus</a>
                </td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection

@push('plugin-scripts')
@endpush

@push('custom-scripts')
@endpush