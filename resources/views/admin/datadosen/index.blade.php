@extends('layout.master')

@push('plugin-styles')
@endpush

@section('content')
<div class="row">
  <div class="col-lg-12 grid-margin stretch-card">
    <div class="card">
      <div class="card-body">
        <h4 class="card-title">Daftar Dosen FITE</h4>
        <a href="/data-dosen/data-dosen/create" class="btn btn-primary">Tambah Dosen</a>
        <hr>
        <div class="table-responsive">
          <table class="table table-hover" id="tableMaster">
            <thead>
              <tr>
                <th> No </th>
                <th> Nama Dosen </th>
                <th> Program Studi </th>
                <th> Riwayat Pendidikan </th>
                <th> Jabatan </th>
                <th> Aksi </th>
              </tr>
            </thead>
            <tbody>
              @foreach ($bf as $u)
              <tr>
                <td class="py-1">
                  {{$loop->iteration}}
                </td>
                <td> 
                  <b>{{ Str::limit($u->nama, 50)}}</b>
                </td>
                <td> 
                  <b>{{ $u->prodi->nama_prodi}}</b>
                </td>
                <td>
                  {!! Str::limit($u->riwayatpendidikan,50)!!}
                </td>
                <td> 
                  <b>{{ $u->jabatan->nama}}</b>
                </td>
                <td>
                  <a href="/data-dosen/data-dosen/edit/{{$u->id}}" class="btn btn-warning">Edit</a>
                  <a href="/data-dosen/data-dosen/hapus/{{$u->id}}" class="btn btn-danger">Hapus</a>
                </td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection

@push('plugin-scripts')
@endpush

@push('custom-scripts')
@endpush