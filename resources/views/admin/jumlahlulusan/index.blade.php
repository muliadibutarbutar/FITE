@extends('layout.master')

@push('plugin-styles')
@endpush

@section('content')
<div class="row">
  <div class="col-lg-12 grid-margin stretch-card">
    <div class="card">
      <div class="card-body">
        <h4 class="card-title">Jumlah Alumni FITE</h4>
        <a href="/alumni/jumlahlulusan/create" class="btn btn-primary">Tambah Jumlah Alumni</a>
        <hr>
        <div class="table-responsive">
          <table class="table table-hover" id="test">
            <thead>
              <tr>
                <th> No </th>
                <th> Program Studi </th>
                <th> Tahun Lulus </th>
                <th> Jumlah </th>
                <th> Aksi </th>
              </tr>
            </thead>
            <tbody>
              @foreach ($jl as $u)
              <tr>
                <td class="py-1">
                  {{$loop->iteration}}
                </td>
                <td> 
                  <b>{{ $u->prodi->nama_prodi }}</b>
                </td>
                <td>
                  {{$u->tahun_lulus}}
                </td>
                <td>
                  {{$u->jumlah}}
                </td>
                <td>
                  <a href="/alumni/jumlahlulusan/edit/{{$u->id}}" class="btn btn-warning">Edit</a>
                  <a href="/alumni/jumlahlulusan/hapus/{{$u->id}}" class="btn btn-danger">Hapus</a>
                </td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection

@push('plugin-scripts')
@endpush

@push('custom-scripts')
@endpush