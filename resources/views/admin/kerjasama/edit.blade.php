@section('js')
    <script type="text/javascript">
        function readURL() {
            var input = this;
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $(input).prev().attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }

        $(function () {
            $(".uploads").change(readURL)
            $("#f").submit(function(){
                // do ajax submit or just classic form submit
              //  alert("fake subminting")
                return false
            })
        })


var check = function() {
  if (document.getElementById('password').value ==
    document.getElementById('confirm_password').value) {
    document.getElementById('submit').disabled = false;
    document.getElementById('message').style.color = 'green';
    document.getElementById('message').innerHTML = 'matching';
  } else {
    document.getElementById('submit').disabled = true;
    document.getElementById('message').style.color = 'red';
    document.getElementById('message').innerHTML = 'not matching';
  }
}
    </script>
@stop

@extends('layout.master')

@push('plugin-styles')
@endpush


@section('content')
<form action="/kerjasama/update/{{$fs->id}}" method="post" enctype="multipart/form-data">
    {{ csrf_field() }}
<div class="row">
            <div class="col-md-12 d-flex align-items-stretch grid-margin">
              <div class="row flex-grow">
                <div class="col-12">
                  <div class="card">
                    <div class="card-body">
                      <h4 class="card-title">Edit Kerjasama {{$fs->nama}}</h4>
                        <div class="form-group">
                            <label for="judul" class="col-md-4 control-label">Nama Kerjasamma</label>
                            <div class="col-md-6">
                                <input id="judul" type="text" class="form-control" name="nama" placeholder="Masukkan Judul Berita" value="{{$fs->nama}}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="email" class="col-md-4 control-label">Gambar</label>
                            <div class="col-md-6">
                                <img width="200" height="200" @if($fs->gambar) src="{{ asset('images/'.$fs->gambar) }}" @endif/>
                                <input type="file" class="uploads form-control" style="margin-top: 20px;" name="gambar" value="{{$fs->gambar}}"/>
                            </div>
                        </div>

                        <button type="submit" class="btn btn-primary" id="submit">
                                    Submit
                        </button>
                        <a href="/kerjasama" class="btn btn-light pull-right" style="float:right;">Kembali</a>
                    </div>
                  </div>
                </div>
              </div>
            </div>

</div>
</form>
@endsection


@push('plugin-scripts')
@endpush

@push('custom-scripts')
@endpush