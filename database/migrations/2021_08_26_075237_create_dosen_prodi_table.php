<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDosenProdiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dosen_prodi', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nama');
            $table->longText('deskripsi');
            $table->string('jabatan');
            $table->unsignedInteger('id_prodi');
            $table->foreign('id_prodi')->references('id')->on('program_studi');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dosen_prodi');
    }
}
