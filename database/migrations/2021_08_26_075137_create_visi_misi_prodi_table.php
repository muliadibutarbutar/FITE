<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVisiMisiProdiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('visi_misi_prodi', function (Blueprint $table) {
            $table->increments('id');
            $table->longText('visi');
            $table->longText('misi');
            $table->unsignedInteger('id_prodi');
            $table->foreign('id_prodi')->references('id')->on('program_studi');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('visi_misi_prodi');
    }
}
