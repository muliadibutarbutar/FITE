<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInfoPmbTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('info_pmb', function (Blueprint $table) {
            $table->increments('id');
            $table->date('jadwal1');
            $table->date('jadwal2');
            $table->string('jenis_tes');
            $table->unsignedInteger('id_usm');
            $table->foreign('id_usm')->references('id')->on('kategori_usm');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('info_pmb');
    }
}
